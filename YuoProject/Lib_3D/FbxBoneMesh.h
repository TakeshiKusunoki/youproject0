#pragma once
//#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
//#include <windows.h>
#include <d3d11.h>
//#include <wrl.h>
#include <DirectXMath.h>
//#include <fbxsdk.h>
//#include <vector>
//#include "LoadFbx.h"


namespace Lib_3D {
	class FbxBoneMeshInit;

	class FbxBoneMesh
	{
	private:

		static constexpr size_t MAX_BONES = 32;

		//コンスタントバッファ(定数バッファ)
		struct CONSTANT_BUFFER
		{
			DirectX::XMFLOAT4X4 wvp;  //ワールド・ビュー・プロジェクション合成行列
			DirectX::XMFLOAT4X4 world;      //ワールド変換行列
			DirectX::XMFLOAT4 material_color;    //材質色
			DirectX::XMFLOAT4 light_direction;    //ライト進行方向
			DirectX::XMFLOAT4X4 bone_transforms[MAX_BONES];//ボーン行列
			DirectX::XMFLOAT4 cameraPos;    //カメラ位置
			DirectX::XMFLOAT4 lightColor;    //光の色
			DirectX::XMFLOAT4 nyutoralLightColor;    //環境光
		};
	private:
		ID3D11Buffer* p_BufferConst;//（定数バッファ
		FbxBoneMeshInit* const _fbxBoneMeshInit;
										 //int numIndices;//インデックスの数
		bool flagBoneAnimationEnd;//ボーンアニメーションの終わり

								  // UNIT.21
								  // convert coordinate system from 'UP:+Z FRONT:+Y RIGHT-HAND' to 'UP:+Y FRONT:+Z LEFT-HAND'
								  // y軸とｚ軸を入れ替える


	public:
		//　ロードしたFBXファイルのメッシュデータをもらう
		FbxBoneMesh(ID3D11Device * p_Device, FbxBoneMeshInit* const fbxBoneMeshInit);
		virtual ~FbxBoneMesh();

		//引数
		//p_DeviceContext	:	デバイスコンテキスト
		//DirectX::XMFLOAT4X4 wvp			:	ワールド・ビュー・プロジェクション合成行列
		//DirectX::XMFLOAT4X4 world			:ワールド変換行列
		//float elapsed_time	:秒の時間
		//DirectX::XMFLOAT4 materialColor	:材質色
		//DirectX::XMFLOAT4 lightVector		:ライト進行方向
		//DirectX::XMFLOAT4 cameraPos,			//カメラ位置
		//DirectX::XMFLOAT4 lightColor,			//光の色
		//DirectX::XMFLOAT4 nyutoralLightColor,	//環境光
		//bool  FlagPaint		:"線or塗りつぶし"描画フラグ
		void render(ID3D11DeviceContext *p_DeviceContext,   //デバイスコンテキスト
			const DirectX::XMFLOAT4X4 &wvp,				//ワールド・ビュー・プロジェクション合成行列
			const DirectX::XMFLOAT4X4 &world,				//ワールド変換行列
			float elapsed_time,/*UNIT.23*/
			const DirectX::XMFLOAT4 &materialColor = { 1,1,1,1 },			//材質色
			const DirectX::XMFLOAT4 &lightVector = { 0,0,4,0 },			//ライト進行方向
			const DirectX::XMFLOAT4 &cameraPos = { 0, 0, 20,1 },			//カメラ位置
			const DirectX::XMFLOAT4 &lightColor = { 0.995f,0.995f,0.999f,0.99f },			//光の色
			const DirectX::XMFLOAT4 &nyutoralLightColor = { 0.20f,0.21f,0.20f,1 },	//環境光
			bool  FlagPaint = true								//線・塗りつぶし描画フラグ
		);

		// アニメーションの終わり
		bool GetFlagBoneAnimationEnd()
		{
			return flagBoneAnimationEnd;
		}
	protected:
	};
}



#pragma once

#include <d3d11.h>
//#include <wrl.h>
#include <DirectXMath.h>
#include <vector>
#include "ResourceManager.h"
#include "ObjLoad.h"
#include "ObjAtMesh.h"

namespace Lib_3D {
	class ObjAtMeshInit
	{
		friend class ObjAtMesh;
	private:
		ID3D11VertexShader* p_VertexShader;
		ID3D11PixelShader* p_PixelShader;
		ID3D11InputLayout* p_InputLayout;

		ID3D11RasterizerState* p_RasterizerStateLine;//（線描画
		ID3D11RasterizerState* p_RasterizerStatePaint;//（塗りつぶし描画
		ID3D11DepthStencilState* p_DepthStencilState;

		//追加
		ID3D11SamplerState* p_SampleState;// サンプラーステート
		static ResourceManager resourceManager;//リソースマネージャー

		ObjLoad::MESH mesh;//! objロードデータ(atは違う)
	public:
		ObjAtMeshInit(ID3D11Device* p_Device, const wchar_t* obj_filename = nullptr, bool flipping_v_coordinates = false);
		virtual ~ObjAtMeshInit();
		//! 頂点リストの設計図を取得
		const std::vector<DirectX::XMFLOAT3>& GetVertexList()
		{
			return  mesh.vertexList;
		}
	private:
	};
}



#pragma once
/**
* @file DirectX11Init.h
* @brief DirectX11Initクラスが記述されている
* @details 詳細な説明
*/

#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く

#include <windows.h>


//#include "Blend.h"#include <tchar.h>

#include <d3d11.h>//追加
#include <dxgi.h>
//#include "Font.h"
//
//#include "sprite2D.h"
//#include "GeometricPrimitive.h"
//#include "Static_mesh.h"
//#include "Skinned_mesh.h"
//#include "Blend.h"
#include "Font.h"
#include "Blend.h"




namespace Lib_3D {
	class ResourceManager;
	class BlendMode;
	/**
	* @brief directX11デバイス初期化
	* @par 詳細
	* デバイスとイミィディエイトコンテキストを定義取得できる
	*/
	class DirectX11InitDevice
	{
	private:
		static ID3D11Device* p_Device;//! デバイス
		static ID3D11DeviceContext* p_ImidiateContext;//! イミィディエイトコンテキスト
	public:
		static DirectX11InitDevice* GetInstance()
		{
			static DirectX11InitDevice instance;
			return &instance;
		}

		//! @returnデバイス取得
		static ID3D11Device* GetDevice();

		//! @returnデバイスコンテキスト取得
		static ID3D11DeviceContext* GetImidiateContext();
	private:
		DirectX11InitDevice();
		~DirectX11InitDevice();
	};

	//! @def pDirectX11Device
	//! @brief DirectX11デバイスの取得
#define pDirectX11Device DirectX11InitDevice::GetInstance()->GetDevice()

	//! @def pDirectX11DeviceContext
	//! @brief DirectX11デバイスコンテキストの取得
#define pDirectX11DeviceContext DirectX11InitDevice::GetInstance()->GetImidiateContext()

	/**
	* @brief directX11初期化
	* @par 詳細
	* ウインドウごとにインスタンスを作る(複数ウインドウのための設計)
	*/
	class DirectX11ComInit
	{
	private:
		HWND hwnd;//! ウインドウハンドル
		size_t SCREEN_WIDTH;//! ウインドウ幅
		size_t SCREEN_HEIGHT;//! ウインドウ高さ
		IDXGIFactory* p_Factory;//! スワップチェイン作るとき使う
	public:
		//! @brief ゲッター
		//! @return HWND ウインドウハンドル
		HWND GetHwnd();

		//! @brief ゲッター
		//! @return UINT ウインドウ幅
		size_t GetScreenWidth();

		//! @brief ゲッター
		//! @return UINT ウインドウ高さ
		size_t GetScreenHeight();

		IDXGISwapChain* p_SwapChain;//! スワップチェイン
		ID3D11RenderTargetView* p_RenderTargetView;//! レンダーターゲット（描画ターゲットを決めるとき使う）
		ID3D11DepthStencilView* p_DepthStencilView;//! 深度（描画ターゲットを決めるとき使う）
		BlendMode p_Blend;//! ブレンドモード設定
	public:
		//! @brief 描画するウインドウの情報をもらう
		//! @param[in] hwnd_ ウインドウハンドル
		//! @param[in] SCREEN_WIDTH_ 画面幅
		//! @param[in] SCREEN_HEIGHT_ 画面高さ
		DirectX11ComInit(HWND hwnd_, size_t SCREEN_WIDTH_, size_t SCREEN_HEIGHT_);
		//! @brief COMオブジェクトを破棄する
		~DirectX11ComInit();
	public:
		//! @brief 必要なCOMを定義
		bool initialize();
		//明示的に破棄
		void UnInitialize();
	};
}


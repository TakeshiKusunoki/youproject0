#pragma once
/**
* @file VertexPoligon3D.h
* @brief VertexPoligon3Dクラスが記述されている
* @details 詳細な説明
*/
#include <d3d11.h>
#include <DirectXMath.h>//DirectX::XMFLOAT3のため
#include "ResourceManager.h"
#include "vector.h"
#include "XVertexMesh.h"


namespace Lib_3D {
	//! @brief ポリゴン３Ｄの頂点バッファの情報
	class XVertexMeshInit
	{
		friend class XVertexMesh;
	private:
		//! 画像
		struct VERTEX_SPR3D
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			DirectX::XMFLOAT2 texcoord;
			DirectX::XMFLOAT3 at;
			DirectX::XMFLOAT2 texDistance;
		};
	private:
		ID3D11VertexShader* p_VertexShader;//ID3D11VertexShader //p_device->CreateVertexShader()で作成
		ID3D11PixelShader* p_PixelShader;//ID3D11PixelShader    //p_device->CreatePixelShader()で作成
		ID3D11InputLayout* p_InputLayout;//ID3D11InputLayout    //p_device->CreateInputLayout()で作成
		ID3D11Buffer* p_Buffer;//ID3D11Buffer		    //p_device->CreateBuffer()で作成
		ID3D11RasterizerState* p_RasterizerState;//ID3D11RasterizerState
		ID3D11ShaderResourceView* p_ShaderResourceView;
		D3D11_TEXTURE2D_DESC TEXTURE2D_DESC;
		ID3D11SamplerState* p_SamplerState;
		ID3D11DepthStencilState* p_DepthStencilState;
		static ResourceManager resourceManager;//リソースマネージャー
	public:
		//! @brief ３Ｄロード
		XVertexMeshInit(ID3D11Device* pDevice, const wchar_t* textureFilename);
		~XVertexMeshInit();
	};
}
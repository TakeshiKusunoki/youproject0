#pragma once
#include <d3d11.h>//追加
///#include "sprite3D.h"//ReadBinaryFile2を追加したいから
#define DIRECTX_VECTOR
#ifdef DIRECTX_VECTOR
#include <DirectXMath.h>//DirectX::XMFLOAT3のため
#include "ResourceManager.h"

#endif// DIRECTX_VECTOR


namespace Lib_3D {
	class GeometricPrimitive
	{
	public:
		//頂点と法線
		struct VERTEX3D
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
		};

		//コンスタントバッファ(定数バッファ)
		struct CONSTANT_BUFFER
		{
			DirectX::XMFLOAT4X4 wvp;  //ワールド・ビュー・プロジェクション合成行列
			DirectX::XMFLOAT4X4 world;      //ワールド変換行列
			DirectX::XMFLOAT4 material_color;    //材質色
			DirectX::XMFLOAT4 light_direction;    //ライト進行方向
		};
	public:
		GeometricPrimitive() {}
		GeometricPrimitive(ID3D11Device *p_Device);
		virtual ~GeometricPrimitive();
		//引数
		//p_Device	:	デバイス
		//VERTEX3D* vertices:頂点
		//const int NUM_VRETEX:超点数
		//UINT* indices:頂点番号
		//const int NUM_INDEX:頂点番号数
		void create_buffer(ID3D11Device* p_Device, VERTEX3D* vertices, const int NUM_VRETEX, UINT* indices, const int NUM_INDEX);
		//引数
		//p_DeviceContext	:	デバイスコンテキスト
		//wvp			:	ワールド・ビュー・プロジェクション合成行列
		//world			:ワールド変換行列
		//lightVector		:ライト進行方向
		//materialColor	:材質色
		//FlagPaint		:"線or塗りつぶし"描画フラグ
		void render(ID3D11DeviceContext *p_DeviceContext,   //デバイスコンテキスト
			const DirectX::XMFLOAT4X4 &wvp,				//ワールド・ビュー・プロジェクション合成行列
			const DirectX::XMFLOAT4X4 &world,				//ワールド変換行列
			const DirectX::XMFLOAT4 &lightVector,			//ライト進行方向
			const DirectX::XMFLOAT4 &materialColor,			//材質色
			bool  FlagPaint								//線・塗りつぶし描画フラグ
		);
	protected:
		ID3D11VertexShader* p_VertexShader;
		ID3D11PixelShader* p_PixelShader;
		ID3D11InputLayout* p_InputLayout;
		ID3D11Buffer* p_BufferVs;//（頂点バッファ
		ID3D11Buffer* p_BufferIndex;//（インデックバッファ
		ID3D11Buffer* p_BufferConst;//（定数バッファ
		ID3D11RasterizerState* p_RasterizerStateLine;//（線描画
		ID3D11RasterizerState* p_RasterizerStatePaint;//（塗りつぶし描画
		ID3D11DepthStencilState* p_DepthStencilState;
		//追加
		static ResourceManager resourceManager;//リソースマネージャー
										 ///ID3D11ComputeShader* p_ComputeShader;//コンピュートシェーダー

		int numIndices;//インデックスの数


	};



	//立方体
	class Geometric_cube : public GeometricPrimitive
	{
	public:
		Geometric_cube(ID3D11Device *p_Device);
		~Geometric_cube() {}

	private:

	};

	//円柱
	class Geometric_cylinder : public GeometricPrimitive
	{
	public:
		Geometric_cylinder(ID3D11Device *p_Device, UINT num = 36);
		~Geometric_cylinder() {}
		const int NUM;
	private:

	};
}





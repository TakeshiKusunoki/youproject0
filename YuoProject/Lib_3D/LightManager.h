#pragma once
#include <vector>
#include "Light.h"
#include "../Lib_Base/Template.h"

class LightManager : public Singleton<LightManager>
{
	std::vector<Light> lightList;
public:
	LightManager();
	~LightManager();
};
#define pLightManager LightManager::getInstance()

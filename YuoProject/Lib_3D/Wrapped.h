#pragma once
/**
* @file Wrapped.h
* @brief Lib_3Dに記述されているものを使いたいときはこれをincludeする
* @details 詳細な説明
*/
#include	<d3d11.h>
#include "../Lib_Base/Template.h"
#include <vector>
#include	"DirectX11Init.h"
namespace Lib_3D {
	//! @brief DirectX11のＣＯＭオブジェクト関係を管理するクラス
	class DirectXWindowManager : public Singleton<DirectXWindowManager>
	{
	private:
		std::vector<DirectX11ComInit> insList;
	public:
		//SetFramework
		//! @brief ウインドウごとに初期化
		//! @details ウインドウ破棄時、ちゃんとuninitも呼ぶこと
		bool	DirectXInitWindow(HWND hwnd_, size_t SCREEN_WIDTH_, size_t SCREEN_HEIGHT_);


		//COMオブジェクト
		//p_Device
		ID3D11Device*		GetDevice();

		//p_ImidiateContext
		ID3D11DeviceContext*	GetDeviceContext();

		LONG GetScreenWidth(HWND hwnd_);


		LONG GetScreenHeight(HWND hwnd_);


		//IDXGISwapChain*
		IDXGISwapChain* GetSwapChain(HWND hwnd_);


		//ID3D11RenderTargetView
		ID3D11RenderTargetView* GetRenderTargetView(HWND hwnd_);

		ID3D11RenderTargetView ** GetRenderTargetViewA(HWND hwnd_);


		//ID3D11DepthStencilView*
		ID3D11DepthStencilView* GetDepthStencilView(HWND hwnd_);

		//! @brief セッター
		//! @details このウインドウのブレンドモード設定
		//! @param[in] hwnd_ ブレンドモードを設定するウインドウハンドル
		//! @param[in] mode 設定するブレンドモード( BlendMode::BLEND_MODE )
		void SetBlendMode(HWND hwnd_, BlendMode::BLEND_MODE mode);

		//! このウインドウのをアンイニット
		void UnInitInThisWindow(HWND hwnd_);

		//! 全部アンイニット
		//! @details 最後にこれを呼べばいい(その場合、初期化は１回だけ行う)
		void UnInitAll();

	private:
	};
#define pDirectXWindowManager DirectXWindowManager::getInstance()
}






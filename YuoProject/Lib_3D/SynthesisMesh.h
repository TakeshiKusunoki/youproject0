#pragma once

//! @brief 合成メッシュ
#ifndef INCLUDED_SynthesisMesh
#define INCLUDED_SynthesisMesh
/**
* @file XVertexMeshConstantBuffer.h
* @brief XVertexMeshConstantBufferクラスが記述されている
* @details 詳細な説明
*/

#include <d3d11.h>
#include <DirectXMath.h>//DirectX::XMFLOAT3のため
#include <vector>
#include "XVertexMesh.h"

//追加



namespace Lib_3D {
	class XVertexMesh;
	/**
	* @brief n頂点ポリゴン
	* @par 詳細
	* n頂点指定型のスプライト
	*/
	class SynthesisMesh
	{
	private:
		//! 頂点数
		static const size_t VERTEX_NUM_MAX = 64;
		struct VERTEX_SPR3D
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			DirectX::XMFLOAT2 texcoord;
		};


		//! コンスタントバッファ
		struct CONSTANT_BUFFER
		{
			DirectX::XMFLOAT4X4 wvp;				//! ワールド・ビュー・プロジェクション合成行列
			DirectX::XMFLOAT4X4 world;			//! ワールド変換行列
			DirectX::XMFLOAT3 at[VERTEX_NUM_MAX];		//! 頂点座標差分
			DirectX::XMFLOAT4 material_color;		//! 材質色
			DirectX::XMFLOAT4 light_direction;		//! ライト進行方向
			DirectX::XMFLOAT4 cameraPos;			//! カメラ位置
			DirectX::XMFLOAT4 lightColor;			//! 光の色
			DirectX::XMFLOAT4 nyutoralLightColor;  //! 環境光
		};
	private:
		XVertexMesh* const  _XVertexMesh;//! ロードしたモデルデータ(頂点バッファ)
		ID3D11Buffer* p_BufferConst;//! （定数バッファ
		ID3D11Buffer* p_BufferVs;//（頂点バッファ
		ID3D11Buffer* p_BufferIndex;//（インデックバッファ
		ID3D11Buffer* p_Buffer;
	public:
		SynthesisMesh(ID3D11Device* const  pDevice, XVertexMesh* XVertexMesh_);
		~SynthesisMesh();
	public:
		//void VertexChange(XVertexMesh* XVertexMesh_);
		//------------------------------------------------------
		//  スプライト描画
		//------------------------------------------------------
		/**引数
		* @brief ３角形ポリゴン描画
		* @param[in] p_DeviceContext	:	デバイスコンテキスト
		* @param[in] wvp				:	ワールド・ビュー・プロジェクション合成行列
		* @param[in] world				:	ワールド変換行列
		* @param[in] position[3]			:	2D座標
		* @param[in] texPos[3]			:	テクスチャ3頂点座標
		* @param[in] lightVector			:	ライト進行方向
		* @param[in] materialColor		:	材質色
		* @param[in] cameraPos 			:	カメラ位置
		* @param[in] lightColor 			:	光の色
		* @param[in] nyutoralLightColor	:	環境光
		* @param[in] FlagPaint			:	"線or塗りつぶし"描画フラグ
		* @details 変形する３角形ポリゴン<br>
		* 光の色も反映する
		*/
		void RenderXVertexMesh(ID3D11DeviceContext* const p_DeviceContext,
			const DirectX::XMFLOAT4X4& wvp,
			const DirectX::XMFLOAT4X4& world,
			const DirectX::XMFLOAT3 position[VERTEX_NUM_MAX],
			const DirectX::XMFLOAT2 texPos[VERTEX_NUM_MAX],
			const DirectX::XMFLOAT4& lightVector = { 1,1,1,0 },
			const DirectX::XMFLOAT4& materialColor = { 1,1,1,1 },
			const DirectX::XMFLOAT4& cameraPos = { 0, 0, 20,1 },
			const DirectX::XMFLOAT4& lightColor = { 0.995f,0.995f,0.999f,0.99f },
			const DirectX::XMFLOAT4& nyutoralLightColor = { 0.20f,0.21f,0.20f,1 },
			bool  FlagPaint = false
		) const;

	};
}
#endif

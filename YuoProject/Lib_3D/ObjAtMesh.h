#pragma once
#include <d3d11.h>
//#include <wrl.h>
#include <DirectXMath.h>
#include <vector>
//#include "ResourceManager.h"



namespace Lib_3D {
	class ObjAtMeshInit;

	class ObjAtMesh
	{
	public:
		struct CONSTANT_BUFFER
		{
			DirectX::XMFLOAT4X4 wvp;  //ワールド・ビュー・プロジェクション合成行列
			DirectX::XMFLOAT4X4 world;      //ワールド変換行列
			DirectX::XMFLOAT4 material_color;    //材質色
			DirectX::XMFLOAT4 light_direction;    //ライト進行方向
		};
	private:
		ID3D11Buffer* p_BufferConst;//（定数バッファ
		ObjAtMeshInit*const objAtMeshInit;//ロードしたデータ
	public:
		ObjAtMesh(ID3D11Device* p_Device, ObjAtMeshInit*const objAtMeshInit);
		virtual ~ObjAtMesh();

		//引数
		//! param[in] p_DeviceContext	:	デバイスコンテキスト
		//! param[in] wvp			:	ワールド・ビュー・プロジェクション合成行列
		//! param[in] world			:ワールド変換行列
		//! param[in] at
		//! param[in] lightVector		:ライト進行方向
		//! param[in] materialColor	:材質色
		//! param[in] FlagPaint		:"線or塗りつぶし"描画フラグ
		void render(ID3D11DeviceContext *p_DeviceContext,   //デバイスコンテキスト
			const DirectX::XMFLOAT4X4 &wvp,				//ワールド・ビュー・プロジェクション合成行列
			const DirectX::XMFLOAT4X4 &world,				//ワールド変換行列
			std::vector<DirectX::XMFLOAT3>* const  at = nullptr,
			const DirectX::XMFLOAT4 &lightVector = { 1,1,1,0 },			//ライト進行方向
			const DirectX::XMFLOAT4 &materialColor = { 0.20f,0.21f,0.20f,0.0f },			//材質色
			bool  FlagPaint = true								//線・塗りつぶし描画フラグ
		);

	};
}



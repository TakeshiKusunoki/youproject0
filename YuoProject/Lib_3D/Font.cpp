#include "Font.h"

namespace Lib_3D {
	Sprite2D* Font::img = nullptr;
	float Font::r = 0.0f;
	float Font::g = 0.0f;
	float Font::b = 0.0f;
	float Font::a = 1.0f;
	// float h, float w,画像切り抜き幅
	void Font::RenderText(ID3D11DeviceContext* p_DeviceContext,
		const char* text,
		float x, float y, float h, float w,
		float angle)
	{
		//画像を読み込んでいなければ終了
		if (!img)return;
		float tx, ty, tw, th;
		//画像の切り抜き幅
		th = tw = (float)(img->GetTEXTURE2D_DESC().Width / NUM_1LINE);
		//テキスト描画
		for (const char* p = text; *p; p++)
		{
			//切り抜き位置算出
			tx = ((*p) & 0xf)*tw;
			ty = (((*p) >> 4) & 0xF)*th;
			//描画
			img->Render2(p_DeviceContext, x, y, tx, ty, tw, th, 1, 1, tx / 2, ty / 2, angle, r, g, b, a);
			//1文字分ずらし
			x += w;
		}
	}
}


#pragma once
//#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
//#include <windows.h>
#include <d3d11.h>
//#include <wrl.h>
#include <DirectXMath.h>
//#include <fbxsdk.h>
//#include <vector>
#include "FbxLoad.h"
#include "FbxBoneMesh.h"
#include "ResourceManager.h"

namespace Lib_3D {

	class FbxBoneMeshInit
	{
		friend class FbxBoneMesh;
	private:
		ID3D11VertexShader* p_VertexShader;
		ID3D11PixelShader* p_PixelShader;
		ID3D11InputLayout* p_InputLayout;
		//ID3D11Buffer* p_BufferVs;//（頂点バッファ
		//ID3D11Buffer* p_BufferIndex;//（インデックバッファ
		ID3D11RasterizerState* p_RasterizerStateLine;//（線描画
		ID3D11RasterizerState* p_RasterizerStatePaint;//（塗りつぶし描画
		ID3D11DepthStencilState* p_DepthStencilState;
		//追加
		ID3D11SamplerState* p_SampleState;// サンプラーステート
		static ResourceManager resourceManager;//リソースマネージャー
		std::vector<LoadFbx::MESH> Meshes;

	public:
		//　ロードしたFBXファイルのメッシュデータをもらう
		FbxBoneMeshInit(ID3D11Device* p_Deveice, const char* filenam);
		virtual ~FbxBoneMeshInit();


	};
}


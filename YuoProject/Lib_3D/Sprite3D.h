#pragma once
#ifndef INCLUDED_SPRITE3D
#define INCLUDED_SPRITE3D
/**
* @file Sprite3D.h
* @brief Sprite3Dクラスが記述されている
* @details 詳細な説明
*/

#include <d3d11.h>
#include <DirectXMath.h>//DirectX::XMFLOAT3のため
#include "ResourceManager.h"

//#define SCREEN_W(x) (framework::SCREEN_WIDTH[x])
//#define SCREEN_H(x) (framework::SCREEN_HEIGHT[x])


namespace Lib_3D {
	/**
	* @brief 4頂点ポリゴン
	* @par 詳細
	* ４頂点指定型のスプライト
	*/
	class Sprite3D
	{
	public:
		Sprite3D(ID3D11Device*, const wchar_t* /*texture filename*/);
		~Sprite3D();
	public:
		//! 頂点位置ラベル
		enum PIXEL_POINT
		{
			LEFT_TOP = 0,
			LEFT_LOW,
			RIGHT_TOP,
			RIGHT_LOW,
		};
		//! 画像
		struct VERTEX_SPR3D
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			DirectX::XMFLOAT2 texcoord;
		};
		//コンスタントバッファ
		struct CONSTANT_BUFFER
		{
			DirectX::XMFLOAT4X4 wvp;				//! ワールド・ビュー・プロジェクション合成行列
			DirectX::XMFLOAT4X4 world;			//! ワールド変換行列
			DirectX::XMFLOAT4 material_color;		//! 材質色
			DirectX::XMFLOAT4 light_direction;		//! ライト進行方向
			DirectX::XMFLOAT4 cameraPos;			//! カメラ位置
			DirectX::XMFLOAT4 lightColor;			//! 光の色
			DirectX::XMFLOAT4 nyutoralLightColor;  //! 環境光
		};
	private:
		ID3D11VertexShader* p_VertexShader;//! p_device->CreateVertexShader()で作成
		ID3D11PixelShader* p_PixelShader;//! p_device->CreatePixelShader()で作成
		ID3D11InputLayout* p_InputLayout;//! p_device->CreateInputLayout()で作成
		ID3D11Buffer* p_Buffer;//! p_device->CreateBuffer()で作成
		ID3D11Buffer* p_BufferConst;//! （定数バッファ
		ID3D11RasterizerState* p_RasterizerState;//! ID3D11RasterizerState
		ID3D11ShaderResourceView* p_ShaderResourceView;
		D3D11_TEXTURE2D_DESC TEXTURE2D_DESC;
		ID3D11SamplerState* p_SamplerState;
		ID3D11DepthStencilState* p_DepthStencilState;
		ResourceManager* resourceManager;//! リソースマネージャー

	private:
		D3D11_TEXTURE2D_DESC GetTEXTURE2D_DESC()
		{
			return TEXTURE2D_DESC;
		}
	public:
		//------------------------------------------------------
		//  スプライト描画
		//------------------------------------------------------
		//void render(ID3D11DeviceContext* context,
		//	const VECTOR2& position, const VECTOR2& scale,
		//	const VECTOR2& texPos, const VECTOR2& texSize,
		//	const VECTOR2& center, float angle,
		//	const VECTOR4& color) const;

		//void RenderVertexSprite(ID3D11DeviceContext* context,
		//	const VECTOR2& position, const VECTOR2& scale,
		//	const VECTOR2* vertexPos,
		//	const VECTOR2& texPos, const VECTOR2& texSize,
		//	const VECTOR2& center, float angle,
		//	const VECTOR4& color) const;

		////引数
		////UINT windowNum::ウインドウ番号
		////float x, float y,   //平行移動
		////float tx, float ty,//画像切り抜き位置（左上の座標）
		////float tw, float th,//画像切り抜き幅
		////float sx = 1, float sy = 1,//拡大
		////float cx = 0, float cy = 0,//中心
		////float angle = 0,//angle:Raotation angle(Rotation centre is sprite's each vertices)
		////float r = 1, float g = 1, float b = 1, float a = 1);
		//void Render(ID3D11DeviceContext* context,
		//	float x = 0, float y = 0,   //平行移動
		//								//float w, float h,
		//	float tx = 0, float ty = 0,//画像切り抜き
		//	float tw = 0, float th = 0,//画像切り抜き幅
		//	float sx = 1, float sy = 1,//拡大
		//	float cx = 0, float cy = 0,//中心
		//	float angle = 0,//angle:Raotation angle(Rotation centre is sprite's each vertices)
		//	float r = 1, float g = 1, float b = 1, float a = 1)
		//{
		//	return render(context, VECTOR2(x, y), VECTOR2(sx, sy), VECTOR2(tx, ty), VECTOR2(tw, th), VECTOR2(cx, cy), angle, VECTOR4(r, g, b, a));
		//}


		/**引数
		* @brief 4頂点描画
		* @param[in] p_DeviceContext	:	デバイスコンテキスト
		* @param[in] wvp				:	ワールド・ビュー・プロジェクション合成行列
		* @param[in] world				:	ワールド変換行列
		* @param[in] position[4]			:	2D座標
		* @param[in] texPos[4]			:	テクスチャ4頂点座標
		* @param[in] lightVector			:	ライト進行方向
		* @param[in] materialColor		:	材質色
		* @param[in] cameraPos 			:	カメラ位置
		* @param[in] lightColor 			:	光の色
		* @param[in] nyutoralLightColor	:	環境光
		* @param[in] FlagPaint			:	"線or塗りつぶし"描画フラグ
		* @details 変形する4角形ポリゴン<br>
		* 光の色も反映する
		*/
		void RenderSprite3D(ID3D11DeviceContext *p_DeviceContext,   //デバイスコンテキスト]
			const DirectX::XMFLOAT4X4 &wvp,				//ワールド・ビュー・プロジェクション合成行列
			const DirectX::XMFLOAT4X4 &world,				//ワールド変換行列
			const DirectX::XMFLOAT3 position[4],//2D座標
			const DirectX::XMFLOAT2 texPos[4],//テクスチャ４頂点座標
			const DirectX::XMFLOAT4 &lightVector = { 1,1,1,0 },			//ライト進行方向
			const DirectX::XMFLOAT4 &materialColor = { 1,1,1,1 },			//材質色
			const DirectX::XMFLOAT4 &cameraPos = { 0, 0, 20,1 },			//カメラ位置
			const DirectX::XMFLOAT4 &lightColor = { 0.995f,0.995f,0.999f,0.99f },			//光の色
			const DirectX::XMFLOAT4 &nyutoralLightColor = { 0.20f,0.21f,0.20f,1 },	//環境光
			bool  FlagPaint = false								//線・塗りつぶし描画フラグ
		) const;
	};
}


#endif// !INCLUDED_SPRITE3D
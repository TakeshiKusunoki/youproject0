#pragma once
#ifndef INCLUDED_XVertexMesh
#define INCLUDED_XVertexMesh
/**
* @file XVertexMesh.h
* @brief XVertexMeshクラスが記述されている
* @details 詳細な説明
*/

#include <d3d11.h>
#include <DirectXMath.h>//DirectX::XMFLOAT3のため
#include <vector>



//追加



namespace Lib_3D {
	class XVertexMeshInit;
	class BaseCamera;
	class BaseLight;

	/**
	* @brief n頂点ポリゴン
	* @par 詳細
	* n頂点指定型のスプライト
	*/
	class XVertexMesh
	{
		//コンスタントバッファインデックス
		enum CONSTANT_BUFFER_INDEX
		{
			WORLD,
			CAMERA,
			LIGHT,
			END
		};
		static const size_t CONSTANT_BUFFER_NUM = END;//コンスタントバッファ数
	protected:
		struct VERTEX_SPR3D
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			DirectX::XMFLOAT2 texcoord;
			DirectX::XMFLOAT3 at;
			DirectX::XMFLOAT2 texDistance;
		};


		//! コンスタントバッファ
		struct CONSTANT_BUFFER
		{
			DirectX::XMFLOAT4X4 wvp;				//! ワールド・ビュー・プロジェクション合成行列
			DirectX::XMFLOAT4X4 world;			//! ワールド変換行列
			DirectX::XMFLOAT4 color;		//! 材質色
		};
		//! コンスタントバッファ
		//! カメラ情報
		struct CONSTANT_BUFFER_CAMERA
		{
			DirectX::XMFLOAT4 cameraPos;			//! カメラ位置
		};
		//! 光の情報
		struct CONSTANT_BUFFER_LIGHT_DATA
		{
			DirectX::XMFLOAT4 light_direction;		//! ライト進行方向
			DirectX::XMFLOAT4 lightColor;			//! 光の色
			DirectX::XMFLOAT4 nyutoralLightColor;  //! 環境光
		};

	private:
		XVertexMeshInit* _XVertexMeshInit;//! ロードしたモデルデータ(頂点バッファ)
		ID3D11Buffer* p_BufferConst[CONSTANT_BUFFER_NUM];//! （定数バッファ
		ID3D11Buffer* p_BufferVs;//（頂点バッファ
		ID3D11Buffer* p_BufferIndex;//（インデックバッファ
	protected:
		size_t numIndices;
	public:
		XVertexMesh(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMesh_);
		virtual ~XVertexMesh();
	public:
		//void VertexChange(XVertexMesh* XVertexMesh_);
		//------------------------------------------------------
		//  スプライト描画
		//------------------------------------------------------
		/**引数
		* @brief ３角形ポリゴン描画
		* @param[in] p_DeviceContext	:	デバイスコンテキスト
		* @param[in] wvp				:	ワールド・ビュー・プロジェクション合成行列
		* @param[in] world				:	ワールド変換行列
		* @param[in] position[3]			:	2D座標
		* @param[in] texPos[3]			:	テクスチャ3頂点座標
		* @param[in] lightVector			:	ライト進行方向
		* @param[in] materialColor		:	材質色
		* @param[in] cameraPos 			:	カメラ位置
		* @param[in] lightColor 			:	光の色
		* @param[in] nyutoralLightColor	:	環境光
		* @param[in] FlagPaint			:	"線or塗りつぶし"描画フラグ
		* @details 変形する３角形ポリゴン<br>
		* 光の色も反映する
		*/
		void RenderXVertexMesh(ID3D11DeviceContext* const p_DeviceContext,
			const DirectX::XMFLOAT4X4& wvp,
			const DirectX::XMFLOAT4X4& world,
			const std::vector<DirectX::XMFLOAT3>& at,
			const std::vector<DirectX::XMFLOAT2>& texDistance,
			const DirectX::XMFLOAT4& color = { 1,1,1,1 },
			BaseLight*const light = nullptr,//参照する光の情報(シェーダーで使う)
			BaseCamera*const camera = nullptr,//参照するカメラの情報(シェーダーで使う)
			const bool  FlagPaint = false
		) const;

	protected:
		//! @brief 継承クラスがfriendクラスのメソッドにアクセスするゲッター
		D3D11_TEXTURE2D_DESC GetXVertexMeshInitTexDesc();

		//! @brief バッファ作成
		void XVertexMesh::create_buffer(ID3D11Device * p_Device, VERTEX_SPR3D* vertices, const int NUM_VRETEX, UINT* indices, const int NUM_INDEX);
	};


	//! @brief 多角形クラス
	class RegularPoligon : public XVertexMesh
	{
	public:
		//! param[in] XVertexMeshInit_ : 初期化データ
		//! param[in] vertexNum 頂点数(外から指定する)
		//! param[in] texCenter テクスチャ中心
		//! param[in] texScale１とした時の、テクスチャ拡大率
		RegularPoligon(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMeshInit_, const size_t vertexNum, const DirectX::XMFLOAT2 texCenter, const DirectX::XMFLOAT2 texScale);

	};

}


//
//
//	//! @brief ポリゴン
//	class POLIGON : public XVertexMesh
//	{
//	public:
//		static const size_t VERTEX_NUM = 4;
//	private:
//		static const size_t INDEX_NUM = (VERTEX_NUM - 1) * 3;
//	public:
//		POLIGON(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]) ;
//		~POLIGON(){}
//
//	private:
//	};
//
//	//! @brief スクエア
//	class SQUARE : public XVertexMesh
//	{
//	public:
//		static const size_t VERTEX_NUM = 5;
//	private:
//		static const size_t INDEX_NUM = (VERTEX_NUM - 1) * 3;
//	public:
//		SQUARE(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]) ;
//		~SQUARE(){}
//
//	private:
//
//	};
//
//	//! @brief 5
//	class PENTAGON : public XVertexMesh
//	{
//	public:
//		static const size_t VERTEX_NUM = 6;
//	private:
//		static const size_t INDEX_NUM = (VERTEX_NUM-1)*3;
//	public:
//		PENTAGON(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]);
//		~PENTAGON();
//
//	private:
//
//	};
//
//	//! @brief 6
//	class HEXAGON : public XVertexMesh
//	{
//	public:
//		static const size_t VERTEX_NUM = 7;
//	private:
//		static const size_t INDEX_NUM = (VERTEX_NUM - 1) * 3;
//	public:
//		HEXAGON(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]);
//		~HEXAGON();
//
//	private:
//
//	};
//
//
//	//! @brief 7
//	class HEPTAGON : public XVertexMesh
//	{
//	public:
//		static const size_t VERTEX_NUM = 8;
//	private:
//		static const size_t INDEX_NUM = (VERTEX_NUM - 1) * 3;
//	public:
//		HEPTAGON(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]);
//		~HEPTAGON();
//
//	private:
//
//	};
//
//
//	//! @brief 8
//	class OCTAGON : public XVertexMesh
//	{
//	public:
//		static const size_t VERTEX_NUM = 9;
//	private:
//		static const size_t INDEX_NUM = (VERTEX_NUM - 1) * 3;
//	public:
//		OCTAGON(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]);
//		~OCTAGON();
//
//	private:
//
//	};
//
//
//}



#endif// !INCLUDED_POLIGON3D

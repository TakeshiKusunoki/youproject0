#include "XVertexMeshInit.h"
#include <string>
#include <tchar.h>

namespace Lib_3D {
	XVertexMeshInit::XVertexMeshInit(ID3D11Device * pDevice, const wchar_t * textureFilename)
	{
		HRESULT hr = S_OK;

		// 入力オブジェクトの生成
		// 入力レイアウト定義(インプットレイアウト)
		bool f = true;
		{
			// 頂点データの構造を記述
			D3D11_INPUT_ELEMENT_DESC inputElementDesk[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
				{ "AT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEX_DISTANCE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};
			UINT numElements = ARRAYSIZE(inputElementDesk);


			f = resourceManager.LoadVertexShader(pDevice, "Shader\\XVertexMesh.cso", inputElementDesk, numElements, &p_VertexShader, &p_InputLayout);
			if (!f)
			{
				assert(!"! Poligon3D LoadVertexShader faile");
				return;
			}
		}

		// ピクセルシェーダーオブジェクトの生成
		{
			f = resourceManager.LoadPixelShader(pDevice, "Shader\\XVertexMesh.cso", &p_PixelShader);
			if (!f)
			{
				assert(!"! Poligon3D LoadPixelShader faile");
				return;
			}
			// ラスタライザーステート
			D3D11_RASTERIZER_DESC rasteriserDesk;
			ZeroMemory(&rasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
			rasteriserDesk.FillMode = D3D11_FILL_SOLID;//レンダリング時に使用する描画モードを決定します
			rasteriserDesk.CullMode = D3D11_CULL_NONE;//特定の方向を向いている三角形の描画の有無を示します。
			rasteriserDesk.DepthClipEnable = false;//。UNIT5
			rasteriserDesk.FrontCounterClockwise = true;//三角形が前向きか後ろ向きかを決定します。
														//rasteriserDesk.DepthBias =
			hr = pDevice->CreateRasterizerState(&rasteriserDesk, &p_RasterizerState);
			if (FAILED(hr))
			{
				assert(!"! Poligon3D CreateRasterizerState faile");
				return;
			}
		}


		// テクスチャ画像読み込み
		{
			f = resourceManager.LoadShaderResourceView(pDevice, textureFilename, &p_ShaderResourceView, &TEXTURE2D_DESC);
			if (!f)
			{
				assert(!"! Poligon3D LoadShaderResourceView faile");
				return;
			}
			//TEXTURE2D_DESC.Width �A画像幅
		}


		// サンプラーステートオブジェクト(ID3D11SampleState)の生成
		{
			D3D11_SAMPLER_DESC samplerDesk;
			ZeroMemory(&samplerDesk, sizeof(samplerDesk));
			samplerDesk.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
			samplerDesk.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
			samplerDesk.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
			samplerDesk.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
			samplerDesk.MipLODBias = 0;//みっぷマップレベルからのバイアス
			samplerDesk.MaxAnisotropy = 16;//違法性保管を使用している場合の限界値
			samplerDesk.ComparisonFunc = D3D11_COMPARISON_ALWAYS;//比較オプション
			samplerDesk.BorderColor[0] = 1;
			samplerDesk.BorderColor[1] = 1;
			samplerDesk.BorderColor[2] = 1;
			samplerDesk.BorderColor[3] = 1;
			samplerDesk.MinLOD = 0;
			samplerDesk.MaxLOD = D3D11_FLOAT32_MAX;//アクセス可能なみっぷマップの上限値

			hr = pDevice->CreateSamplerState(&samplerDesk, &p_SamplerState);//ピクセルシェーダーに送る
			if (FAILED(hr))
			{
				assert(!"! Poligon3D CreateSamplerState faile");
				return;
			}
		}


		// 深度ステンシルステート
		{
			D3D11_DEPTH_STENCIL_DESC DepthDesc;
			ZeroMemory(&DepthDesc, sizeof(DepthDesc));
			DepthDesc.DepthEnable = TRUE;									//深度テストあり
			DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//書き込む
			DepthDesc.DepthFunc = D3D11_COMPARISON_LESS;					//手前の物体を描画
			DepthDesc.StencilEnable = FALSE;								//ステンシル テストなし
			DepthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;		//ステンシル読み込みマスク
			DepthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;	//ステンシル書き込みマスク
																			//面が表を向いている場合のステンシルステートの設定
			DepthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
			DepthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
			DepthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
			DepthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
																			  //面が裏を向いている場合のステンシルステートの設定
			DepthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
			DepthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR/*D3D11_STENCIL_OP_KEEP*/;	  //維持
			DepthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
			DepthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
			hr = pDevice->CreateDepthStencilState(&DepthDesc, &p_DepthStencilState);	//
			if (FAILED(hr))
			{
				assert(!"深度ステンシル ステート オブジェクトの生成ができません");
				return;
			}
		}

	}


#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	XVertexMeshInit::~XVertexMeshInit()
	{
		////////////////////////////////////////
		//Added by Unit6
		///////////////////////////////////////
		RELEASE_IF(p_DepthStencilState);
		RELEASE_IF(p_SamplerState);
		RELEASE_IF(p_RasterizerState);
		//RELEASE_IF(p_Buffer);
		resourceManager.ReleaseShaderResourceView(p_ShaderResourceView);
		resourceManager.ReleasePixelShader(p_PixelShader);
		resourceManager.ReleaseVertexShader(p_VertexShader, p_InputLayout);
		resourceManager.Release();
	}
#undef RELEASE_IF
#undef DELETE_IF
}
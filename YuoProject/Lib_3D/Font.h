#pragma once
#include "sprite2D.h"
namespace Lib_3D {
	class Font
	{
	private:
		static const int NUM_1LINE = 16;//�摜�̂P��ɕ���ł��镶����
		static Sprite2D* img;
		static float r, g, b, a;
		Font();
		~Font() {}
	public:
		static bool LoadImage(ID3D11Device* p_Device, const wchar_t* filename)
		{
			ReleaseImage();
			img = new Sprite2D(p_Device, filename);
			return true;
		}
		static void ReleaseImage()
		{
			if (img)
			{
				delete img;
				img = nullptr;
			}
		}
		static void SetColor(float r = 1, float g = 1, float b = 1, float a = 1)
		{
			Font::r = r;
			Font::g = g;
			Font::b = b;
			Font::a = a;
		}
		static void RenderText(ID3D11DeviceContext*,
			const char*,
			float x = 0, float y = 0, float h = 32, float w = 32,
			float angle = 0
		);
	};
}


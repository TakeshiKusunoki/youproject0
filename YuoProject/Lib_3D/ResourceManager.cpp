#include "ResourceManager.h"
#include "WICTextureLoader.h"
#include <cstdio>


namespace Lib_3D {

	bool ReadBinaryFile(const char* filename, BYTE** data, unsigned int& size)
	{
		FILE* fp = 0;
		if (fopen_s(&fp, filename, "rb"))
		{
			return false;
		}

		fseek(fp, 0, SEEK_END);

		size = ftell(fp);

		fseek(fp, 0, SEEK_SET);
		*data = new unsigned char[size];
		fread(*data, size, 1, fp);

		fclose(fp);

		return true;
	}


	void ResourceManager::Release()
	{
		for (auto& it : ResourceInfo)
		{
			it.Release();
		}
		for (auto& it : ResourceVertexShader)
		{
			it.Release();
		}
		for (auto& it : ResourcePixcelShader)
		{
			it.Release();
		}
		ResourceInfo.clear();
		ResourceVertexShader.clear();
		ResourcePixcelShader.clear();
	}

	bool ResourceManager::LoadShaderResourceView(ID3D11Device* p_Device, const wchar_t* fileName, ID3D11ShaderResourceView**const  pShaderResourceView_, D3D11_TEXTURE2D_DESC*const  p_TEXTURE2D_DESC)
	{
		bool flagFind = false;
		ID3D11ShaderResourceView* pShaderResourceView = nullptr;
		ID3D11Resource* p_Resource = nullptr;
		int no = -1;//最初に(番号)
		//////////////////////////
		//同じデータ検索
		for (int i = 0; i < ResourceInfo.size(); i++)
		{
			RESOURCE_INFO* p = &ResourceInfo.at(i);

			//ファイルパスが違うなら無視
			if (p->filename != fileName)
				continue;
			//同名ファイルが存在するなら
			p->p_ShaderResourceView->GetResource(&p_Resource);//Resourceの準備
			pShaderResourceView = p->p_ShaderResourceView;//発見
			flagFind = true;
			break;
		}

		////////////////////////////////////////////////
		//同じデータが見つからなかった = 新規読み込み
		if (!flagFind)
		{
			HRESULT hr = DirectX::CreateWICTextureFromFile(p_Device, fileName, &p_Resource, &pShaderResourceView);
			if (FAILED(hr))
				return false;
			//リソースマネージャーにデータを格納
			ResourceInfo.emplace_back(fileName, pShaderResourceView);
		}

		////////////////////////////////////////////////
		// 最終処理(参照渡しでデータを返す)
		ID3D11Texture2D* Tex2D;//テクスチャデータの準備
		p_Resource->QueryInterface(&Tex2D);
		// データをコピー
		*pShaderResourceView_ = pShaderResourceView;//deleteはリソースマネージャーで行う
		Tex2D->GetDesc(p_TEXTURE2D_DESC);
		// 解放
		Tex2D->Release();
		p_Resource->Release();
		return true;
	}

	bool ResourceManager::LoadVertexShader(ID3D11Device* p_Device, const char* csofileName, const D3D11_INPUT_ELEMENT_DESC* p_INPUT_ELEMENT_DESC, const UINT ELEMENTS_ARRAY_NUM, ID3D11VertexShader** const  p_VertexShader, ID3D11InputLayout** const  p_InputLayout)
	{
		//初期化としてのNULL代入
		*p_VertexShader = nullptr;
		*p_InputLayout = nullptr;
		bool flagFind = false;
		ID3D11VertexShader* p_VertexShader_ = nullptr;
		ID3D11InputLayout* p_InputLayout_ = nullptr;
		///////////////////////
		//const char*->wchar_t*(検索保存で使用)
		wchar_t fileName[256];
		size_t stringSize = 0;
		mbstowcs_s(&stringSize, fileName, csofileName, strlen(csofileName));
		//////////////////////
		//データ検索
		for (int n = 0; n <  ResourceVertexShader.size(); n++)
		{
			RESOURCE_VERTEXSHADER* p = &ResourceVertexShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != fileName )
				continue;
			//同名ファイルが存在した
			p_VertexShader_ = p->p_VertexShader;
			p_InputLayout_ = p->p_InputLayout;
			flagFind = true;
			break;
		}

		////////////////////////////////////////////////
		// データが見つからなかった = 新規読み込み
		if (!flagFind)
		{
			BYTE* data;//頂点シェーダーデータ
			UINT size;//頂点シェーダーデータサイズ
					  // コンパイル済み頂点シェーダーオブジェクトの読み込み
			if (!ReadBinaryFile(csofileName, &data, size))return false;

			// 頂点シェーダーオブジェクトの生成
			HRESULT hr = p_Device->CreateVertexShader(data, size, nullptr, &p_VertexShader_);
			if (FAILED(hr))
			{
				delete[] data;
				return false;
			}

			// 入力レイアウトの作成
			hr = p_Device->CreateInputLayout(p_INPUT_ELEMENT_DESC, ELEMENTS_ARRAY_NUM, data, size, &p_InputLayout_);
			delete[] data;
			if (FAILED(hr))
				return false;

			// 新規データの保存
			//リソースマネージャーにデータを格納
			ResourceVertexShader.emplace_back(fileName, p_VertexShader_, p_InputLayout_);
		}
		////////////////////////////////////////////////
		// 最終処理(参照渡しでデータを返す)
		*p_VertexShader = p_VertexShader_;
		*p_InputLayout = p_InputLayout_;
		return true;
	}


	bool ResourceManager::LoadPixelShader(ID3D11Device* p_Device, const char* csofileName, ID3D11PixelShader**const  p_PixelShader)
	{
		//初期化としてのNULL代入
		*p_PixelShader = nullptr;
		bool flagFind = false;
		ID3D11PixelShader* pPixelShader = nullptr;
		///////////////////////
		//const char*->wchar_t*(検索保存で使用)
		wchar_t fileName[256];
		size_t stringSize = 0;
		mbstowcs_s(&stringSize, fileName, csofileName, strlen(csofileName));
		//////////////////////
		// データ検索
		for (int n = 0; n < ResourcePixcelShader.size(); n++)
		{
			RESOURCE_PIXELSHADER* p = &ResourcePixcelShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != fileName)
				continue;


			//同名ファイルが存在した
			pPixelShader = p->p_PixelShader;
			flagFind = true;//発見
			break;
		}

		////////////////////////////////////////////////
		// データが見つからなかった = 新規読み込み
		if (!flagFind)
		{
			BYTE* data;//ピクセルシェーダデータ
			UINT size;//ピクセルシェーダデータサイズ
					  // コンパイル済み頂点シェーダーオブジェクトの読み込み
			if (!ReadBinaryFile(csofileName, &data, size))return false;
			// ピクセルシェーダーオブジェクトの生成
			HRESULT hr = p_Device->CreatePixelShader(data, size, nullptr, &pPixelShader);
			delete[] data;
			if (FAILED(hr))
				return false;

			// 新規データの保存
			ResourcePixcelShader.emplace_back(fileName, pPixelShader);
		}
		////////////////////////////////////////////////
		// 最終処理(参照渡しでデータを返す)
		*p_PixelShader = pPixelShader;
		return true;
	}

	void ResourceManager::ReleaseShaderResourceView(ID3D11ShaderResourceView* p_ShaderResourceView)
	{
		if (!p_ShaderResourceView)return;
		for (int n = 0; n < ResourceInfo.size(); n++)
		{
			RESOURCE_INFO* p = &ResourceInfo[n];
			//データが違うなら虫
			if (p_ShaderResourceView != p->p_ShaderResourceView)
				continue;
			//データが存在した
			p->Release();//データ開放
			break;
		}
	}

	void ResourceManager::ReleaseVertexShader(ID3D11VertexShader* p_VertexShader, ID3D11InputLayout* p_InputLayout)
	{

		if (!p_VertexShader)
			return;

		for (int n = 0; n < ResourceVertexShader.size(); n++)
		{
			RESOURCE_VERTEXSHADER* p = &ResourceVertexShader[n];
			//データが違うなら虫
			if (p_VertexShader != p->p_VertexShader)
				continue;
			//データが存在した
			p->Release();//データ開放
			break;
		}
	}

	void ResourceManager::ReleasePixelShader(ID3D11PixelShader* p_PixelShader)
	{
		if (!p_PixelShader)
			return;

		for (int n = 0; n < ResourcePixcelShader.size(); n++)
		{
			RESOURCE_PIXELSHADER* p = &ResourcePixcelShader[n];
			//データが違うなら虫
			if (p_PixelShader != p->p_PixelShader)
				continue;
			//データが存在した
			p->Release();//データ開放
			break;
		}
	}
}


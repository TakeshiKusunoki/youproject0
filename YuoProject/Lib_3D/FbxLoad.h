#pragma once
#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <windows.h>
#include <d3d11.h>
#include <wrl.h>
#include <DirectXMath.h>
#include <fbxsdk.h>
#include <vector>

namespace Lib_3D {
	class ResourceManager;

	//! @brief fbx load
	class LoadFbx
	{
	private:
		static ResourceManager* resourceManager;//リソースマネージャー
		static constexpr int MAX_BONE_INFLUENCES_ = 4;//ひとつの頂点が影響を受ける最大ボ−ン
	public:
		struct Material
		{

			DirectX::XMFLOAT4 color = { 0.8f,0.8f,0.8f,1.0f };
			ID3D11ShaderResourceView* p_Shader_resource_view;
		};
		// UNIT.18--------------
		struct SUBSET
		{
			UINT index_start = 0;//start number of index buffer
			UINT index_count = 0;//number of vertices(indices)
			Material diffuse;
		};
		// UNIT.22
		//ボーン構造体
		struct BONE
		{
			DirectX::XMFLOAT4X4 transform;
		};

		// UNIT.23
		typedef std::vector<BONE> SKELTAL;
		struct SKELTAL_ANIMATION :public std::vector<SKELTAL>
		{
			float sampling_time = 1 / 24.0f;
			float animation_tick = 0.0f;
		};

		//----------------------------
		// UNIT.19
		struct MESH
		{
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_VertexBuffer;
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_IndexBuffer;
			std::vector<SUBSET> Subsets;
			DirectX::XMFLOAT4X4 GloabalTransform = {
				1,0,0,0,
				0,1,0,0,
				0,0,1,0,
				0,0,0,1,
			};
			// UNIT.22
			//std::vector<Skinned_mesh::BONE> Skeletal;//ボーン構造体の可変長配列
			// UNIT.23
			SKELTAL_ANIMATION SkeltalAnimation;
		};
		//----------------------------

		//! @breif 頂点バッファ
		struct VERTEX
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			DirectX::XMFLOAT2 texcoord;
			FLOAT bone_weights[MAX_BONE_INFLUENCES_] = { 1,0,0,0 };
			INT bone_indices[MAX_BONE_INFLUENCES_] = {};
		};



	public:
		LoadFbx(ResourceManager* const resourceManager);
		virtual ~LoadFbx();
		//! fbxファイルのロード
		//! return ロードしたメッシュデータを返す
		const std::vector<MESH>& loadFbxFile(ID3D11Device* p_Device, const char * fbx_filename);
	private:
		//引数
		//p_Device	:	デバイス
		//VERTEX3D* vertices:頂点
		//const int NUM_VRETEX:超点数
		//UINT* indices:頂点番号
		//const int NUM_INDEX:頂点番号数
		void create_buffer(ID3D11Device* p_Device, MESH* mesh, VERTEX* vertices, const int NUM_VRETEX, UINT* indices, const int NUM_INDEX);

		//meshデータ取得
		std::vector<fbxsdk::FbxNode*> GetFbxMesh(ID3D11Device * p_Device, const char * fbx_filename, fbxsdk::FbxManager* Manager);

		// Fetch material properties.
		void FetchMaterial(ID3D11Device* p_Device, const fbxsdk::FbxMesh* Fbx_mesh, MESH* mesh, const char * fbx_filename);
		// Count the polygon count of each material
		void SetIndexCount(const fbxsdk::FbxMesh* Fbx_mesh, MESH* mesh);
		// Fetch mesh data
		void FetchMeshData(ID3D11Device* p_Device, const fbxsdk::FbxMesh* Fbx_mesh, MESH* mesh, std::vector<VERTEX>* vertices, std::vector<UINT>* indices);
	};
}




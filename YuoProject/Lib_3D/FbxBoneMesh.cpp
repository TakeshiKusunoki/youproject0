#include "FbxBoneMesh.h"
//#include "LoadFbx.h"
//#include <functional>
//#include "texture.h"
#include "FbxBoneMeshInit.h"


namespace Lib_3D {


	FbxBoneMesh::FbxBoneMesh(ID3D11Device * p_Device, FbxBoneMeshInit* const fbxBoneMeshInit)
		: _fbxBoneMeshInit(fbxBoneMeshInit)
		, flagBoneAnimationEnd(false)
	{
		// COMオブジェクトの初期化-----------------------
		HRESULT hr = S_OK;

		// �H定数バッファオブジェクトの生成
		D3D11_BUFFER_DESC ConstantBufferDesc;
		ZeroMemory(&ConstantBufferDesc, sizeof(ConstantBufferDesc));
		ConstantBufferDesc.Usage = /*D3D11_USAGE_DYNAMIC*/D3D11_USAGE_DEFAULT;			//動的使用法
		ConstantBufferDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;//定数バッファ
		ConstantBufferDesc.CPUAccessFlags = 0/*D3D11_CPU_ACCESS_WRITE*/;//CPUから書き込む
		ConstantBufferDesc.MiscFlags = 0;
		ConstantBufferDesc.ByteWidth = sizeof(CONSTANT_BUFFER);
		ConstantBufferDesc.StructureByteStride = 0;


		hr = p_Device->CreateBuffer(&ConstantBufferDesc, nullptr, &p_BufferConst);
		if (FAILED(hr))
		{
			assert(!"定数バッファオブジェクトの生成ができません");
			return;
		}
	}




#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	FbxBoneMesh::~FbxBoneMesh()
	{
		RELEASE_IF(p_BufferConst);
	}
#undef RELEASE_IF
#undef DELETE_IF




	//定数
#define VERTEX_BUFFER_NUM 1//頂点バッファの数
	//描画
	void FbxBoneMesh::render(ID3D11DeviceContext* p_DeviceContext, const DirectX::XMFLOAT4X4 & wvp, const DirectX::XMFLOAT4X4 & world, float elapsed_time,
		const DirectX::XMFLOAT4 & materialColor, const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 & cameraPos,
		const DirectX::XMFLOAT4 & lightColor, const DirectX::XMFLOAT4 & nyutoralLightColor, bool FlagPaint)
	{
		// 描画するプリミティブ種類の設定
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);//３角形リストを描画する//D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP


																					   // シェーダーの設定;
		p_DeviceContext->VSSetShader(_fbxBoneMeshInit->p_VertexShader, nullptr, 0);
		p_DeviceContext->PSSetShader(_fbxBoneMeshInit->p_PixelShader, nullptr, 0);
		// 入力レイアウト・オブジェクトの設定
		p_DeviceContext->IASetInputLayout(_fbxBoneMeshInit->p_InputLayout);
		// ラスタライザ・ステート・オブジェクトの設定
		p_DeviceContext->RSSetState((FlagPaint ? _fbxBoneMeshInit->p_RasterizerStatePaint : _fbxBoneMeshInit->p_RasterizerStateLine));//ラスタライザステート
																								  ///p_DeviceContext->RSSetScissorRects();//シザー短形

		p_DeviceContext->OMSetDepthStencilState(_fbxBoneMeshInit->p_DepthStencilState, 0);//震度ステンシルステート

		for (LoadFbx::MESH& mesh : _fbxBoneMeshInit->Meshes)
		{
			// �Bコンスタントバッファを設定+
			CONSTANT_BUFFER data = {};

			data.light_direction = lightVector;
			data.lightColor = lightColor;
			data.nyutoralLightColor = nyutoralLightColor;
			data.cameraPos = cameraPos;

			// UNIT.19
			//姿勢行列とwvp座標を掛け合わせる
			DirectX::XMStoreFloat4x4(&data.wvp,
				DirectX::XMLoadFloat4x4(&mesh.GloabalTransform)*DirectX::XMLoadFloat4x4(&wvp));
			//姿勢行列とワールド座標を掛け合わせる
			DirectX::XMStoreFloat4x4(&data.world,
				DirectX::XMLoadFloat4x4(&mesh.GloabalTransform)*DirectX::XMLoadFloat4x4(&world));

			//ダミー行列
			for (int i = 0; i < MAX_BONES; i++)
			{
				XMStoreFloat4x4(&data.bone_transforms[i], DirectX::XMMatrixIdentity());
				//XMStoreFloat4x4(&data.bone_transforms[6], DirectX::XMMatrixRotationRollPitchYaw(angle * 0.01745f, 0, 0));
			}
			// UNIT.23 ボーンアニメーション
			if (mesh.SkeltalAnimation.size() > 0)
			{
				flagBoneAnimationEnd = false;//追加 ボーンアニメーションの終わりフラグ初期化
				size_t frame = 0;
				float x = 0;
				x = mesh.SkeltalAnimation.animation_tick / mesh.SkeltalAnimation.sampling_time;
				frame = static_cast<size_t>(x);
				size_t s = mesh.SkeltalAnimation.size() - 1;
				if (frame > mesh.SkeltalAnimation.size() - 1)
				{
					frame = 0;
					mesh.SkeltalAnimation.animation_tick = 0;
					flagBoneAnimationEnd = true;//ボーンアニメーションの終わり
				}
				std::vector<LoadFbx::BONE>&Skeltal = mesh.SkeltalAnimation.at(frame);
				size_t number_of_bones = Skeltal.size();

				_ASSERT_EXPR(number_of_bones < MAX_BONES, L"t'the number_of_bones' exceeds MAX_BONES");

				for (size_t i = 0; i < number_of_bones; i++)
				{
					DirectX::XMStoreFloat4x4(&data.bone_transforms[i], DirectX::XMLoadFloat4x4(&Skeltal.at(i).transform));
				}
				mesh.SkeltalAnimation.animation_tick += elapsed_time;
			}

			/*static const DirectX::XMFLOAT4X4 coordinate_conversion = {
				1,0,0,0,
				0,0,1,0,
				0,1,0,0,
				0,0,0,1,
			};*/
			// UNIT.21
			// 反転
			/*	DirectX::XMStoreFloat4x4(&data.wvp,
			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform) * XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&wvp));
			DirectX::XMStoreFloat4x4(&data.world,
			DirectX::XMLoadFloat4x4(&mesh.GloabalTransform) * XMLoadFloat4x4(&coordinate_conversion) *    XMLoadFloat4x4(&world));*/


			// �@IAに頂点バッファを設定
			UINT stride[VERTEX_BUFFER_NUM] = { sizeof(LoadFbx::VERTEX) };	 //頂点バッファにふくまれる頂点データのサイズ。
			UINT offset[VERTEX_BUFFER_NUM] = { 0 };					 //頂点バッファのオフセット
			p_DeviceContext->IASetVertexBuffers(0, VERTEX_BUFFER_NUM, mesh.p_VertexBuffer.GetAddressOf(), stride, offset);
			// �AIAにインデックスバッファを設定
			p_DeviceContext->IASetIndexBuffer(mesh.p_IndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

			for (LoadFbx::SUBSET& its : mesh.Subsets)
			{
				// �Bコンスタントバッファを設定+
				//----------------
				//data.material_color = materialColor;
				data.material_color.x = materialColor.x*its.diffuse.color.x;
				data.material_color.y = materialColor.y*its.diffuse.color.y;
				data.material_color.z = materialColor.z*its.diffuse.color.z;
				data.material_color.w = materialColor.w*its.diffuse.color.w;

				p_DeviceContext->UpdateSubresource(p_BufferConst, 0, nullptr, &data, 0, 0);//情報を定数バッファへコピー
				p_DeviceContext->VSSetConstantBuffers(0, VERTEX_BUFFER_NUM, &p_BufferConst);//定数バッファをシェーダへセット

																							// サンプラーステートのセット
				p_DeviceContext->PSSetSamplers(0, 1, &_fbxBoneMeshInit->p_SampleState);
				// シェーダリソースビューのセット
				if (its.diffuse.p_Shader_resource_view)
					p_DeviceContext->PSSetShaderResources(0, 1, &its.diffuse.p_Shader_resource_view);
				// インデックス付けされているプリミティブの描画
				p_DeviceContext->DrawIndexed(its.index_count, its.index_start, 0);
			}
		}
	}
#undef VERTEX_BUFFER_NUM



}























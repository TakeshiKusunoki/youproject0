#pragma once
#include "../Lib_Base\\Template.h"
#include	<DirectXMath.h>
namespace Lib_3D
{

	enum DIR
	{
		FRONT = 1,		//	奥から手前へ
		BACK,				//	手前から奥へ
		UP,					//	下から上へ
		DOWN,				//	上から下へ
		LEFT,				//	右から左へ
		RIGHT,				//	左から右へ

		DIR_MAX
	};


	//! @brief 基底クラス
	//! @details コンスタントバッファ用カメラデータ
	class BaseCamera
	{

	private:
	protected:
		DirectX::XMMATRIX _view;//! ビュー行列
		DirectX::XMVECTOR _target;//! 注視点
		DirectX::XMFLOAT4 _cameraPos;//! カメラ位置
		DirectX::XMMATRIX _projection;//!	投影行列 キャッシュ用
		bool _flagUpdate;//!カメラのメソッドの値が変わったか
	public:
		BaseCamera() {
			_view = DirectX::XMMatrixIdentity();
			_target = DirectX::XMQuaternionIdentity();
			_cameraPos = DirectX::XMFLOAT4(0, 0, 0, 1);
			_projection = DirectX::XMMatrixIdentity();
			_flagUpdate = false;
		}
		BaseCamera(const BaseCamera&) {}
		virtual ~BaseCamera() {}

	public:
		DirectX::XMMATRIX SetOrthographic(float w, float h, float znear, float zfar);// 平行投影行列設定
		DirectX::XMMATRIX SetPerspective(float fov, float aspect, float znear, float zfar);//	投影行列定義

		//! ゲッター
		 // 平行移動カメラ取得
		const  DirectX::XMMATRIX&	GetView()
		{
			return this->_view;
		}
		const DirectX::XMMATRIX& GetProjection()
		{
			return this->_projection;
		}
		const DirectX::XMFLOAT4& GetCameraPos()
		{
			return this->_cameraPos;
		}
		const DirectX::XMVECTOR& GetTarget()
		{
			return this->_target;
		}
		bool GetFlagUpdate()
		{
			return this->_flagUpdate;
		}
	};


	//平行移動カメラ
	class CameraOrthographic : public BaseCamera
	{
	private:
		DirectX::XMVECTOR _position;//位置
		DirectX::XMVECTOR _target;//注視点
		DirectX::XMMATRIX _projection;//	投影行列 キャッシュ用
		DirectX::XMFLOAT4 _cameraPos;
	public:
		CameraOrthographic():BaseCamera() {}
		CameraOrthographic(const CameraOrthographic&) :BaseCamera() {}
		~CameraOrthographic();
		void Update()
		{
			DirectX::XMVECTOR	up;

			static DirectX::XMFLOAT3 base(0.0f, 0.0f, 10.0f);
			static DirectX::XMFLOAT3 tBase(0.0f, 0.0f, -1.0f);

#define CAMERA_MOVE_SPEED 0.1f
			if (GetAsyncKeyState('W') < 0)
			{
				base.y -= CAMERA_MOVE_SPEED;
				tBase.y -= CAMERA_MOVE_SPEED;
			}
			if (GetAsyncKeyState('S') < 0)
			{
				base.y += CAMERA_MOVE_SPEED;
				tBase.y += CAMERA_MOVE_SPEED;
			}
			if (GetAsyncKeyState('R') < 0)
			{
				base.z += CAMERA_MOVE_SPEED;
				tBase.z += CAMERA_MOVE_SPEED;
			}
			if (GetAsyncKeyState('F') < 0)
			{
				base.z -= CAMERA_MOVE_SPEED;
				tBase.z -= CAMERA_MOVE_SPEED;
			}
			if (GetAsyncKeyState('A') < 0)
			{
				base.x += CAMERA_MOVE_SPEED;
				tBase.x += CAMERA_MOVE_SPEED;
			}
			if (GetAsyncKeyState('D') < 0)
			{
				base.x -= CAMERA_MOVE_SPEED;
				tBase.x -= CAMERA_MOVE_SPEED;
			}
			cameraPos = { base.x, base.y, base.z, 1.0f };
			position = DirectX::XMLoadFloat4(&cameraPos);

			target = DirectX::XMVectorSet(tBase.x, tBase.y, tBase.z, 1.0f);
			up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
			if (mode == UP || mode == DOWN)up = DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);

			return DirectX::XMMatrixLookAtLH(position, target, up);
		}
	public:
		DirectX::XMMATRIX SetOrthographic(float w, float h, float znear, float zfar);// 平行投影行列設定
		DirectX::XMMATRIX SetPerspective(float fov, float aspect, float znear, float zfar);//	投影行列定義

	private:

	};

	//! @brief 視点追従カメラ
	class CameraFps : public BaseCamera
	{
	private:
		DirectX::XMFLOAT3*const faceCenterPosition;//中心となる位置
		float faceRadius;//顔半径
		DirectX::XMVECTOR*const orientation;//体の姿勢
		DirectX::XMFLOAT3 faceAngle;//顔の回転//ここからカメラ上方向を決める
		DirectX::XMFLOAT3 eyeAngle;//目の回転//焦点位置
		DirectX::XMFLOAT4 cameraPos;//カメラ位置

		DirectX::XMMATRIX projection;//	投影行列 戻り値用
		DirectX::XMMATRIX view;//	ビュー行列 戻り値用
	public:
		CameraFps(DirectX::XMFLOAT3*const faceCenterPosition, DirectX::XMVECTOR*const orientation)
			: faceCenterPosition(faceCenterPosition)
			, orientation(orientation);
		~CameraFps();
		void Upadate();
		DirectX::XMMATRIX GetView()override;
	private:

	};

}
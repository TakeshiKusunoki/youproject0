//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
#include "XVertexMesh.h"
#include "Camera.h"
#include "Light.h"
#include "vector.h"
#include "XVertexMeshInit.h"

namespace Lib_3D {
	// 画像
	XVertexMesh::XVertexMesh(ID3D11Device* const  pDevice, XVertexMeshInit* const  XVertexMesh)
		: _XVertexMeshInit(XVertexMesh)
		, numIndices(0)
	{
		HRESULT hr = S_OK;

		// �H定数バッファオブジェクトの生成
		D3D11_BUFFER_DESC ConstantBufferDesc;
		ZeroMemory(&ConstantBufferDesc, sizeof(ConstantBufferDesc));
		ConstantBufferDesc.Usage = D3D11_USAGE_DEFAULT;			//動的使用法
		ConstantBufferDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;//定数バッファ
		ConstantBufferDesc.CPUAccessFlags = 0;//CPUから書き込む
		ConstantBufferDesc.MiscFlags = 0;
		ConstantBufferDesc.ByteWidth = sizeof(CONSTANT_BUFFER);
		ConstantBufferDesc.StructureByteStride = 0;

		hr = pDevice->CreateBuffer(&ConstantBufferDesc, nullptr, &p_BufferConst[WORLD]);
		if (FAILED(hr))
		{
			assert(!"定数バッファオブジェクトの生成ができません");
			return;
		}
		// �H定数バッファオブジェクトの生成
		ConstantBufferDesc.ByteWidth = sizeof(CONSTANT_BUFFER_CAMERA);

		CONSTANT_BUFFER_CAMERA constaCamera;
		constaCamera.cameraPos = DirectX::XMFLOAT4(0, 0, 0, 1);

		D3D11_SUBRESOURCE_DATA subResourceData;
		ZeroMemory(&subResourceData, sizeof(subResourceData));
		subResourceData.pSysMem = &constaCamera;//初期化データへのポインターです。
		subResourceData.SysMemPitch = 0;//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
		subResourceData.SysMemSlicePitch = 0;//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
		hr = pDevice->CreateBuffer(&ConstantBufferDesc, &subResourceData, &p_BufferConst[CAMERA]);
		if (FAILED(hr))
		{
			assert(!"定数バッファオブジェクトの生成ができません");
			return;
		}
		// �H定数バッファオブジェクトの生成
		ConstantBufferDesc.ByteWidth = sizeof(CONSTANT_BUFFER_LIGHT_DATA);

		CONSTANT_BUFFER_LIGHT_DATA constLight;
		constLight.lightColor = DirectX::XMFLOAT4(1, 1, 1, 1);
		constLight.light_direction = DirectX::XMFLOAT4(0, 0, 1, 0);
		constLight.nyutoralLightColor = DirectX::XMFLOAT4(0.2f, 0.2f, 0.2f, 0);

		subResourceData.pSysMem = &constLight;
		hr = pDevice->CreateBuffer(&ConstantBufferDesc, &subResourceData, &p_BufferConst[LIGHT]);
		if (FAILED(hr))
		{
			assert(!"定数バッファオブジェクトの生成ができません");
			return;
		}
	}




#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	XVertexMesh::~XVertexMesh()
	{
		for (size_t i = 0; i < CONSTANT_BUFFER_NUM; i++)
		{
			RELEASE_IF(p_BufferConst[i]);
		}
		RELEASE_IF(p_BufferIndex);
		RELEASE_IF(p_BufferVs);
	}
#undef RELEASE_IF
#undef DELETE_IF










	// 3Dのスプライト(3頂点)
	//0　1
	// 2
	void XVertexMesh::RenderXVertexMesh(ID3D11DeviceContext* const p_DeviceContext,
		const DirectX::XMFLOAT4X4& wvp,
		const DirectX::XMFLOAT4X4& world,
		const std::vector<DirectX::XMFLOAT3>& at,
		const std::vector<DirectX::XMFLOAT2>& texDistance,
		const DirectX::XMFLOAT4& color,
		BaseLight*const light,//参照する光の情報
		BaseCamera*const camera,//参照するカメラの情報
		const bool  FlagPaint) const
	{
		if (numIndices <= 0)
			return;
		D3D11_VIEWPORT viewport;
		UINT numViewports = 1;
		p_DeviceContext->RSGetViewports(&numViewports, &viewport);
		light->GetLightColor();
		camera->GetCameraPos();
		//頂点シェーダーへの引数
		//VERTEX_SPR3D* vertices;
		std::vector<VERTEX_SPR3D> vertices;
		vertices.resize(at.size());
		//頂点シェーダーに引数を渡す
		for (int i = 0; i < vertices.size(); i++)
		{
			vertices[i].at.x = at.at(i).x / viewport.Width;
			vertices[i].at.y = at.at(i).y / viewport.Height;
			vertices[i].at.z = at.at(i).z;
			vertices[i].texDistance.x = texDistance.at(i).x / _XVertexMeshInit->TEXTURE2D_DESC.Width;
			vertices[i].texDistance.y = texDistance.at(i).y / _XVertexMeshInit->TEXTURE2D_DESC.Height;
		}

		// �Bコンスタントバッファを設定+		light_direction	{x=0.000000000 y=-1.00000000 z=0.000000000 ...}	DirectX::XMFLOAT4
		CONSTANT_BUFFER constantData = {};//コンスタントバッファデータ
		constantData.wvp = wvp;
		constantData.world = world;
		constantData.color = color;
		p_DeviceContext->UpdateSubresource(p_BufferConst[WORLD], 0, nullptr, &constantData, 0, 0);//情報を定数バッファへコピー

		//コンスタントバッファのカメラ情報書き換え
		if (camera && camera->GetFlagUpdate())
		{
			CONSTANT_BUFFER_CAMERA constantCameraData = {};
			constantCameraData.cameraPos = camera->GetCameraPos();
			p_DeviceContext->UpdateSubresource(p_BufferConst[CAMERA], 0, nullptr, &constantData, 0, 0);//情報を定数バッファへコピー

		}

		//コンスタントバッファの光情報書き換え
		if (light && light->GetFlagUpdate())
		{
			CONSTANT_BUFFER_LIGHT_DATA constantLightData = {};
			constantLightData.lightColor = light->GetLightColor();
			constantLightData.light_direction = light->GetLightVector();
			constantLightData.nyutoralLightColor = light->GetNewtoralColor();
			p_DeviceContext->UpdateSubresource(p_BufferConst[LIGHT], 0, nullptr, &constantLightData, 0, 0);//情報を定数バッファへコピー

		}

		p_DeviceContext->VSSetConstantBuffers(0, CONSTANT_BUFFER_NUM, p_BufferConst);//定数バッファをシェーダへセット
		// 頂点を動的生成
		D3D11_MAPPED_SUBRESOURCE msr;
		p_DeviceContext->Map(p_BufferVs, 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
		memcpy(msr.pData, vertices.data(), vertices.size() * sizeof(VERTEX_SPR3D));
		p_DeviceContext->Unmap(p_BufferVs, 0);


		// 各種ステートを設定
		UINT stride = sizeof(VERTEX_SPR3D);
		UINT offset = 0;
		p_DeviceContext->IASetVertexBuffers(0, 1, &p_BufferVs, &stride, &offset);
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
		p_DeviceContext->IASetInputLayout(_XVertexMeshInit->p_InputLayout);
		p_DeviceContext->RSSetState(_XVertexMeshInit->p_RasterizerState);
		p_DeviceContext->VSSetShader(_XVertexMeshInit->p_VertexShader, nullptr, 0);
		p_DeviceContext->PSSetShader(_XVertexMeshInit->p_PixelShader, nullptr, 0);
		p_DeviceContext->PSSetShaderResources(0, 1, &_XVertexMeshInit->p_ShaderResourceView);
		p_DeviceContext->PSSetSamplers(0, 1, &_XVertexMeshInit->p_SamplerState);
		p_DeviceContext->OMSetDepthStencilState(_XVertexMeshInit->p_DepthStencilState, 1);

		p_DeviceContext->DrawIndexed(numIndices, 0, 0);
	}
















	D3D11_TEXTURE2D_DESC XVertexMesh::GetXVertexMeshInitTexDesc()
	{
		return _XVertexMeshInit->TEXTURE2D_DESC;
	}

	void XVertexMesh::create_buffer(ID3D11Device * p_Device, VERTEX_SPR3D* vertices, const int NUM_VRETEX, UINT* indices, const int NUM_INDEX)
	{
		HRESULT hr = S_OK;

		// 頂点バッファ定義
		D3D11_BUFFER_DESC Bufferdesk;
		ZeroMemory(&Bufferdesk, sizeof(Bufferdesk));
		Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(VERTEX_SPR3D);
		Bufferdesk.Usage = D3D11_USAGE_DYNAMIC;	//GPUのみ
		Bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		Bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		Bufferdesk.MiscFlags = 0;
		Bufferdesk.StructureByteStride = 0;//float?sizeof(DirectX::XMFLOAT3)

		 // サブリソースの初期化に使用されるデータを指定します。
		D3D11_SUBRESOURCE_DATA SubResourceData;
		ZeroMemory(&SubResourceData, sizeof(SubResourceData));
		SubResourceData.pSysMem = vertices;				//(バッファの初期値)初期化データへのポインターです。
		SubResourceData.SysMemPitch = 0;					//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
		SubResourceData.SysMemSlicePitch = 0;				//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
		// バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
		hr = p_Device->CreateBuffer(&Bufferdesk, &SubResourceData, &p_BufferVs);
		if (FAILED(hr))
		{
			assert(!"頂点バッファの作成ができません");
			return;
		}

		///////////////////////////////////////////////////
		// �Gインデックスバッファオブジェクトの生成
		///////////////////////////////////////////////////

		// インデックスバッファの定義
		D3D11_BUFFER_DESC IndexBufferDesc;
		ZeroMemory(&IndexBufferDesc, sizeof(IndexBufferDesc));
		IndexBufferDesc.ByteWidth = NUM_INDEX * sizeof(UINT);
		IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
		IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		IndexBufferDesc.CPUAccessFlags = 0;
		IndexBufferDesc.MiscFlags = 0;
		IndexBufferDesc.StructureByteStride = 0;
		//インデックスの数を補完
		numIndices = NUM_INDEX;

		// インデックス・バッファのサブリソースの定義
		D3D11_SUBRESOURCE_DATA IndexSubResource;
		ZeroMemory(&IndexSubResource, sizeof(IndexSubResource));
		IndexSubResource.pSysMem = indices;
		IndexSubResource.SysMemPitch = 0;
		IndexSubResource.SysMemSlicePitch = 0;

		// インデックス・バッファの作成
		hr = p_Device->CreateBuffer(&IndexBufferDesc, &IndexSubResource, &p_BufferIndex);
		if (FAILED(hr))
		{
			assert(!"インデックス・バッファの作成ができません");
			return;
		}


	}







	RegularPoligon::RegularPoligon(ID3D11Device * const pDevice, XVertexMeshInit * const XVertexMeshInit_,
		const size_t vertexNum, const DirectX::XMFLOAT2 texCenter, const DirectX::XMFLOAT2 texScale)
		: XVertexMesh(pDevice, XVertexMeshInit_)
	{
		std::vector<VERTEX_SPR3D> vertices;//頂点情報
		vertices.resize(vertexNum + 1);
		//初期化
		SecureZeroMemory(vertices.data(), vertices.size() * sizeof(VERTEX_SPR3D));
		for (size_t i = 0; i < vertexNum + 1; i++)
		{
			vertices[i].at = VECTOR3(0, 0, 0);
			vertices[i].texDistance = VECTOR2(0, 0);
		}

		//頂点位置
		{
			const float ONE_ANGLE = DirectX::XMConvertToRadians(360.0f / vertexNum);
			const float ANG90 = DirectX::XMConvertToRadians(90);
			//真ん中
			vertices[vertexNum].position = VECTOR3(0, 0, 0);
			//周り
			for (size_t i = 0; i < vertexNum; i++)
			{
				//右回り
				vertices[i].position = VECTOR3(cosf(-i * ONE_ANGLE + ANG90), sinf(-i * ONE_ANGLE + ANG90), 0);
			}
		}
		//インデックス
		{//最後の頂点番号を除く
			std::vector<UINT> indices;// インデックス
			indices.resize(vertexNum * 3);
			for (size_t i2 = 0; i2 < vertexNum - 1; i2++)
			{
				indices[i2 * 3 + 0] = vertexNum;//真ん中
				indices[i2 * 3 + 1] = i2 + 0;//右回り
				indices[i2 * 3 + 2] = i2 + 1;
			}
			//最後
			indices[vertexNum - 1] = vertexNum - 1;
			indices[vertexNum - 1] = 0;//最初の頂点につなぐ
		}

		//法線
		{
			const VECTOR3 NORMAL = { 0.0f, 0.0f, -1.0f };
			for (size_t i = 0; i < vertexNum; i++)
			{
				vertices[i].normal = NORMAL;
			}
		}

		//テクスチャ
		for (size_t i = 0; i < vertexNum; i++)
		{
			vertices[i].texcoord = VECTOR2(vertices[i].position.x, vertices[i].position.y);
			vertices[i].texcoord.x *= texScale.x;
			vertices[i].texcoord.y *= texScale.y;
			vertices[i].texcoord.x += texCenter.x;
			vertices[i].texcoord.y += texCenter.y;
		}
	}

}





//
//
//
//
//	POLIGON::POLIGON(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]) :XVertexMesh(pDevice, XVertexMesh_)
//	{
//		VERTEX_SPR3D vertices[VERTEX_NUM];
//		SecureZeroMemory(vertices, sizeof(vertices));
//		const VECTOR3 NORMAL = { 0.0f, 0.0f, -1.0f };//法線
//		for (size_t i = 0; i < VERTEX_NUM; i++)
//		{
//			vertices[i].texcoord.x = texPos[i].x / GetXVertexMeshInitTexDesc().Width;
//			vertices[i].texcoord.y = texPos[i].y / GetXVertexMeshInitTexDesc().Height;
//			vertices[i].normal = NORMAL;
//		}
//		//center
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		//top
//		vertices[1].position = DirectX::XMFLOAT3(0, 0.43301270189f, 0);
//		//left
//		vertices[2].position = DirectX::XMFLOAT3(-0.5f, -0.43301270189f, 0);
//		//right
//		vertices[3].position = DirectX::XMFLOAT3(0.5f, -0.43301270189f, 0);
//		//右回り
//		UINT indices[INDEX_NUM] = {
//			0,1,2,
//			0,2,3,
//			0,3,1
//		};
//		numIndices = INDEX_NUM;
//
//		create_buffer(pDevice, vertices, VERTEX_NUM, indices, VERTEX_NUM);
//	}
//
//	SQUARE::SQUARE(ID3D11Device * const pDevice, XVertexMeshInit *const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]) : XVertexMesh(pDevice, XVertexMesh_)
//	{
//		VERTEX_SPR3D vertices[VERTEX_NUM];
//		SecureZeroMemory(vertices, sizeof(vertices));
//		const VECTOR3 NORMAL = { 0.0f, 0.0f, -1.0f };//法線
//		for (size_t i = 0; i < VERTEX_NUM; i++)
//		{
//			vertices[i].texcoord.x = texPos[i].x / GetXVertexMeshInitTexDesc().Width;
//			vertices[i].texcoord.y = texPos[i].y / GetXVertexMeshInitTexDesc().Height;
//			vertices[i].normal = NORMAL;
//		}
//
//		//center
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		//top left
//		vertices[1].position = DirectX::XMFLOAT3(-0.5f, 0.5f, 0);
//		//top right
//		vertices[2].position = DirectX::XMFLOAT3(0.5f, 0.5f, 0);
//		//bootom right
//		vertices[3].position = DirectX::XMFLOAT3(0.5f, -0.5f, 0);
//		//bootom left
//		vertices[4].position = DirectX::XMFLOAT3(-0.5f, -0.5f, 0);
//
//		numIndices = INDEX_NUM;
//		//右回り
//		UINT indices[INDEX_NUM] = {
//			0,1,2,
//			0,2,3,
//			0,3,4,
//			0,4,1
//		};
//		create_buffer(pDevice, vertices, VERTEX_NUM, indices, VERTEX_NUM);
//	}
//
//
//	PENTAGON::PENTAGON(ID3D11Device* const  pDevice, XVertexMeshInit*const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]) : XVertexMesh(pDevice, XVertexMesh_)
//	{
//		VERTEX_SPR3D vertices[VERTEX_NUM];
//
//		SecureZeroMemory(vertices, sizeof(vertices));
//		const VECTOR3 NORMAL = { 0.0f, 0.0f, -1.0f };//法線
//		for (size_t i = 0; i < VERTEX_NUM; i++)
//		{
//			vertices[i].texcoord.x = texPos[i].x / GetXVertexMeshInitTexDesc().Width;
//			vertices[i].texcoord.y = texPos[i].y / GetXVertexMeshInitTexDesc().Height;
//			vertices[i].normal = NORMAL;
//		}
//		//右回り
//		//center
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		//top
//		vertices[1].position = DirectX::XMFLOAT3(0.0f, 0.5f, 0);
//		//top right
//		vertices[2].position = DirectX::XMFLOAT3(0.4755282581475767860582f, 0.1545084971874737120512f, 0);
//		//bootom right
//		vertices[3].position = DirectX::XMFLOAT3(0.4045084971874737120512f, -0.2938926261462365645844f, 0);
//		//bootom left
//		vertices[4].position = DirectX::XMFLOAT3(-0.4045084971874737120512f, -0.2938926261462365645844f, 0);
//		//top left
//		vertices[5].position = DirectX::XMFLOAT3(-0.4755282581475767860582f, 0.1545084971874737120512f, 0);
//
//		numIndices = INDEX_NUM;
//		UINT indices[INDEX_NUM] = {
//			0,1,2,
//			0,2,3,
//			0,3,4,
//			0,4,5,
//			0,5,1
//		};
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		create_buffer(pDevice, vertices, VERTEX_NUM, indices, VERTEX_NUM);
//	}
//
//	HEXAGON::HEXAGON(ID3D11Device * const pDevice, XVertexMeshInit *const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]) : XVertexMesh(pDevice, XVertexMesh_)
//	{
//		VERTEX_SPR3D vertices[VERTEX_NUM];
//
//		SecureZeroMemory(vertices, sizeof(vertices));
//		const VECTOR3 NORMAL = { 0.0f, 0.0f, -1.0f };//法線
//		for (size_t i = 0; i < VERTEX_NUM; i++)
//		{
//			vertices[i].texcoord.x = texPos[i].x / GetXVertexMeshInitTexDesc().Width;
//			vertices[i].texcoord.y = texPos[i].y / GetXVertexMeshInitTexDesc().Height;
//			vertices[i].normal = NORMAL;
//		}
//		//右回り
//		//center
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		//top
//		vertices[1].position = DirectX::XMFLOAT3(0.0f, 0.5f, 0);
//		//top right
//		vertices[2].position = DirectX::XMFLOAT3(0.4330127018922193233819f, 0.25f, 0);
//		//bootom right
//		vertices[3].position = DirectX::XMFLOAT3(0.4330127018922193233819f, -0.25f, 0);
//		//bootom
//		vertices[4].position = DirectX::XMFLOAT3(0.0f, -0.5f, 0);
//		//bootom left
//		vertices[5].position = DirectX::XMFLOAT3(-0.4330127018922193233819f, -0.25f, 0);
//		//top left
//		vertices[6].position = DirectX::XMFLOAT3(-0.4330127018922193233819f, 0.25f, 0);
//
//
//		numIndices = INDEX_NUM;
//		UINT indices[INDEX_NUM] = {
//			0,1,2,
//			0,2,3,
//			0,3,4,
//			0,4,5,
//			0,5,6,
//			0,6,1
//		};
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		create_buffer(pDevice, vertices, VERTEX_NUM, indices, VERTEX_NUM);
//	}
//	HEPTAGON::HEPTAGON(ID3D11Device * const pDevice, XVertexMeshInit *const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]) : XVertexMesh(pDevice, XVertexMesh_)
//	{
//		VERTEX_SPR3D vertices[VERTEX_NUM];
//
//		SecureZeroMemory(vertices, sizeof(vertices));
//		const VECTOR3 NORMAL = { 0.0f, 0.0f, -1.0f };//法線
//		for (size_t i = 0; i < VERTEX_NUM; i++)
//		{
//			vertices[i].texcoord.x = texPos[i].x / GetXVertexMeshInitTexDesc().Width;
//			vertices[i].texcoord.y = texPos[i].y / GetXVertexMeshInitTexDesc().Height;
//			vertices[i].normal = NORMAL;
//		}
//		//右回り
//		//center
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		//top
//		vertices[1].position = DirectX::XMFLOAT3(0.0f, 0.5f, 0);
//		//top right
//		vertices[2].position = DirectX::XMFLOAT3(0.3909157412340149043542f, 0.3117449009293667652625f, 0);
//		//center right
//		vertices[3].position = DirectX::XMFLOAT3(0.4874639560909118035091f, -0.1112604669781572021445f, 0);
//		//bootom right
//		vertices[4].position = DirectX::XMFLOAT3(0.2169418695587790602379f, -0.4504844339512095631181f, 0);
//		//bootom left
//		vertices[5].position = DirectX::XMFLOAT3(-0.2169418695587790602379f, -0.4504844339512095631181f, 0);
//		//center left
//		vertices[6].position = DirectX::XMFLOAT3(-0.4874639560909118035091f, -0.1112604669781572021445f, 0);
//		//top left
//		vertices[7].position = DirectX::XMFLOAT3(-0.390915741234014904354f, 0.3117449009293667652625f, 0);
//
//
//		numIndices = INDEX_NUM;
//		UINT indices[INDEX_NUM] = {
//			0,1,2,
//			0,2,3,
//			0,3,4,
//			0,4,5,
//			0,5,6,
//			0,6,7,
//			0,7,1
//		};
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		create_buffer(pDevice, vertices, VERTEX_NUM, indices, VERTEX_NUM);
//	}
//
//
//	OCTAGON::OCTAGON(ID3D11Device * const pDevice, XVertexMeshInit *const XVertexMesh_, const DirectX::XMFLOAT2 texPos[VERTEX_NUM]) : XVertexMesh(pDevice, XVertexMesh_)
//	{
//		VERTEX_SPR3D vertices[VERTEX_NUM];
//
//		SecureZeroMemory(vertices, sizeof(vertices));
//		const VECTOR3 NORMAL = { 0.0f, 0.0f, -1.0f };//法線
//		for (size_t i = 0; i < VERTEX_NUM; i++)
//		{
//			vertices[i].texcoord.x = texPos[i].x / GetXVertexMeshInitTexDesc().Width;
//			vertices[i].texcoord.y = texPos[i].y / GetXVertexMeshInitTexDesc().Height;
//			vertices[i].normal = NORMAL;
//		}
//		//右回り
//		//center
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		//top
//		vertices[1].position = DirectX::XMFLOAT3(0.0f, 0.5f, 0);
//		//top right
//		vertices[2].position = DirectX::XMFLOAT3(0.3535533905932737622004f, 0.3535533905932737622004f, 0);
//		//center right
//		vertices[3].position = DirectX::XMFLOAT3(0.5f, 0.0f, 0);
//		//bootom right
//		vertices[4].position = DirectX::XMFLOAT3(0.3535533905932737622004f, -0.3535533905932737622004f, 0);
//		//bootom left
//		vertices[5].position = DirectX::XMFLOAT3(0.0f, -0.5f, 0);
//		//center left
//		vertices[6].position = DirectX::XMFLOAT3(-0.3535533905932737622004f, -0.3535533905932737622004f, 0);
//		//top left
//		vertices[7].position = DirectX::XMFLOAT3(-0.5f, 0.0f, 0);
//		//top left
//		vertices[8].position = DirectX::XMFLOAT3(-0.3535533905932737622004f, 0.3535533905932737622004f, 0);
//
//		numIndices = INDEX_NUM;
//		UINT indices[INDEX_NUM] = {
//			0,1,2,
//			0,2,3,
//			0,3,4,
//			0,4,5,
//			0,5,6,
//			0,6,7,
//			0,7,8,
//			0,8,1
//		};
//		vertices[0].position = DirectX::XMFLOAT3(0, 0, 0);
//		create_buffer(pDevice, vertices, VERTEX_NUM, indices, VERTEX_NUM);
//	}
//
//
//
//
//
//}
//
//

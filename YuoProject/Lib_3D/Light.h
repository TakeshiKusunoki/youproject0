#pragma once
#include <directxmath.h>
#include "../Lib_Base/Template.h"
namespace Lib_3D
{
	//! @brief 基底クラス
	//! @details 光レンダーのコンスタントバッファ用データ
	class BaseLight
	{
	protected:
		DirectX::XMFLOAT4 _lightVector;//! 光方向
		DirectX::XMFLOAT4 _lightColor;//! 光の色
		DirectX::XMFLOAT4 _newtoralColor;//! 自然光
		bool flagUpdate;
	public:
		const DirectX::XMFLOAT4& GetLightVector() { return _lightVector; };//! 光方向
		const DirectX::XMFLOAT4& GetLightColor() { return _lightColor; };//! 光の色
		const DirectX::XMFLOAT4& GetNewtoralColor() { return _newtoralColor; };//! 自然光
		bool GetFlagUpdate()
		{
			return flagUpdate;
		}
	};


	//! @brief 点光源
	class LightPoint : public BaseLight
	{
		DirectX::XMFLOAT3 position;//点光源座標
	public:
		LightPoint();
		~LightPoint();

	private:

	};

	//! @brief 線光源
	class LightAbreast : public BaseLight
	{
	public:
		LightAbreast();
		~LightAbreast();

	private:

	};

}

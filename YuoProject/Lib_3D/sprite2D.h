#pragma once
#define DIRECTX_VECTOR
#ifdef DIRECTX_VECTOR
#include <d3d11.h>
#include <DirectXMath.h>//DirectX::XMFLOAT3のため
#endif// DIRECTX_VECTOR
#include "ResourceManager.h"
#include "vector.h"
//追加



namespace Lib_3D {

	class Sprite2D
	{
		//図形
		struct VERTEX2D
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT4 color;
			////////////////////////
			//UNIT4
			////////////////////////
		};
		//画像
		struct VERTEXSPR2D
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT4 color;
			////////////////////////
			//UNIT4
			////////////////////////
			DirectX::XMFLOAT2 texcoord;
		};
	public:
		Sprite2D(ID3D11Device*);
		Sprite2D(ID3D11Device*, const wchar_t* /*texture filename*/);
		~Sprite2D();
	public:
		enum PIXEL_POINT
		{
			LEFT_TOP = 0,
			LEFT_LOW,
			RIGHT_TOP,
			RIGHT_LOW,
		};
	private:
		ID3D11VertexShader* p_VertexShader;//ID3D11VertexShader //p_device->CreateVertexShader()で作成
		ID3D11PixelShader* p_PixelShader;//ID3D11PixelShader    //p_device->CreatePixelShader()で作成
		ID3D11InputLayout* p_InputLayout;//ID3D11InputLayout    //p_device->CreateInputLayout()で作成
		ID3D11Buffer* p_Buffer;//ID3D11Buffer		    //p_device->CreateBuffer()で作成
		ID3D11RasterizerState* p_RasterizerState;//ID3D11RasterizerState
												 ////////////////////////////////////////
												 //Added by Unit4
												 ///////////////////////////////////////
		ID3D11ShaderResourceView* p_ShaderResourceView;
		D3D11_TEXTURE2D_DESC TEXTURE2D_DESC;
		ID3D11SamplerState* p_SamplerState;
		////////////////////////////////////////
		//Added by Unit6
		///////////////////////////////////////
		ID3D11DepthStencilState* p_DepthStencilState;
		////////////////////////////////////////
		//Deleted by Unit7
		///////////////////////////////////////
		//ID3D11BlendState* p_BlendState;
		ResourceManager* resourceManager;//リソースマネージャー
	public:
		//#ifndef _FUNCTION_
		//#define _FUNCTION_
		float CircleHalf_Length2D(float* v1)
		{
			return (float)sqrt((v1[0])*(v1[0]) + (v1[1])*(v1[1]));
		}
		D3D11_TEXTURE2D_DESC GetTEXTURE2D_DESC()
		{
			return TEXTURE2D_DESC;
		}
		//四角形の頂点角度を求める
		/*void ShitaSerch(vertex3D*, float**, float);
		void ShitaSerch(vertexSpr3D*, float**, float);*/
		//bool angleDegree();
		//#endif
		void Render(ID3D11DeviceContext*);
		// (四角形)座標x,y,  拡大scale_x,scaley,   幅w,h,	中心center_x,center_y,   角度(θ)angle,	色(0~1)r,g,b,a
		void Render(ID3D11DeviceContext*,
			float x, float y,//dx,dy;Coordinate of sprite's left-top corner in screen space
			float sx, float sy,
			float w, float h,//dw,dh;Size of sprite in screen space
			float cx, float cy,
			float angle,//angle:Raotation angle(Rotation centre is sprite's each vertices)
			float r, float g, float b, float a);
		//////////////////////////////////////////////
		//Unit4
		////////////////////////////////////////////////
		// すべての頂点位置を指定(四角形)
		void Render(ID3D11DeviceContext*,
			DirectX::XMFLOAT3* position,//頂点位置
			float x, float y,//平行移動位置
			float sx, float sy,
			//float w, float h,//dw,dh;Size of sprite in screen space
			float cx, float cy,//回転の中心
			float angle,//angle:Raotation angle(Rotation centre is sprite's each vertices)
			float r, float g, float b, float a);
		//2点と角度を指定するタイプは不可
		//画像描画
		//------------------------------------------------------
		//  スプライト描画
		//------------------------------------------------------
		void render(ID3D11DeviceContext*,
			const VECTOR2&, const VECTOR2&,
			const VECTOR2&, const VECTOR2&,
			const VECTOR2&, float,
			const VECTOR4&) const;

		//引数
		//UINT windowNum::ウインドウ番号
		//float x, float y,   //平行移動
		//float tx, float ty,//画像切り抜き位置（左上の座標）
		//float tw, float th,//画像切り抜き幅
		//float sx = 1, float sy = 1,//拡大
		//float cx = 0, float cy = 0,//中心
		//float angle = 0,//angle:Raotation angle(Rotation centre is sprite's each vertices)
		//float r = 1, float g = 1, float b = 1, float a = 1);
		void Render2(ID3D11DeviceContext* context,
			float x = 0, float y = 0,   //平行移動
										//float w, float h,
			float tx = 0, float ty = 0,//画像切り抜き
			float tw = 0, float th = 0,//画像切り抜き幅
			float sx = 1, float sy = 1,//拡大
			float cx = 0, float cy = 0,//中心
			float angle = 0,//angle:Raotation angle(Rotation centre is sprite's each vertices)
			float r = 1, float g = 1, float b = 1, float a = 1)
		{
			return render(context, VECTOR2(x, y), VECTOR2(sx, sy), VECTOR2(tx, ty), VECTOR2(tw, th), VECTOR2(cx, cy), angle, VECTOR4(r, g, b, a));
		}


		//UINT windowNum::ウインドウ番号
		//float x, float y,   //平行移動
		//float sx = 1, float sy = 1,//拡大
		//float tx, float ty,//画像切り抜き位置（左上の座標）
		//float tw, float th,//画像切り抜き幅
		//float cx = 0, float cy = 0,//中心
		//float angle = 0,//angle:Raotation angle(Rotation centre is sprite's each vertices)
		//float r = 1, float g = 1, float b = 1, float a = 1);
		void Render3(ID3D11DeviceContext* p_DeviceContext,
			UINT windowNum = 0,
			float x = 0, float y = 0,   //平行移動
			float sx = 1, float sy = 1,//拡大
			float tx = 0, float ty = 0,//画像切り抜き
			float tw = 0, float th = 0,//画像切り抜き幅
			float cx = 0, float cy = 0,//中心
			float angle = 0,
			float r = 1, float g = 1, float b = 1, float a = 1) const;

	};


	//==========================================================================
	//
	//      SpriteBatchクラス
	//
	//==========================================================================
	class SpriteBatch
	{
	private:
		ID3D11VertexShader*         vertexShader;
		ID3D11PixelShader*          pixelShader;
		ID3D11InputLayout*          inputLayout;
		ID3D11Buffer*               buffer;
		ID3D11RasterizerState*      rasterizerState;

		ID3D11ShaderResourceView*   shaderResourceView;
		D3D11_TEXTURE2D_DESC        tex2dDesc;
		ID3D11SamplerState*         samplerState;
		ID3D11DepthStencilState*    depthStencilState;

		size_t MAX_INSTANCES;

		//--------------------------------
		//  構造体定義
		//--------------------------------
		struct instance
		{
			DirectX::XMFLOAT4X4 ndcTransform;
			VECTOR4 texcoordTransform;
			VECTOR4 color;
		};
		ID3D11Buffer* instanceBuffer;

	public:

		//--------------------------------
		//  構造体定義
		//--------------------------------
		struct vertex
		{
			VECTOR3 position;
			VECTOR2 texcoord;
		};

		//------------------------------------------------------
		//  コンストラクタ
		//------------------------------------------------------
		SpriteBatch(ID3D11Device*, const wchar_t*, size_t);

		//------------------------------------------------------
		//  デストラクタ
		//------------------------------------------------------
		~SpriteBatch();

		//------------------------------------------------------
		//  前処理
		//------------------------------------------------------
		void begin(ID3D11DeviceContext*);

		//------------------------------------------------------
		//  スプライトバッチ描画
		//------------------------------------------------------
		void render(
			const VECTOR2&, const VECTOR2&,
			const VECTOR2&, const VECTOR2&,
			const VECTOR2&, float angle,
			const VECTOR4&);

		//------------------------------------------------------
		//  スプライトバッチ描画
		//------------------------------------------------------
		void render(
			float, float, float, float,
			float, float, float, float,
			float, float, float,
			float, float, float, float);

		//------------------------------------------------------
		//  テキスト描画
		//------------------------------------------------------
		float textout(
			std::string, const VECTOR2&,
			const VECTOR2&, const VECTOR4&);

		//------------------------------------------------------
		//  テキスト描画
		//------------------------------------------------------
		float textout(
			std::string, float, float,
			float, float, float, float, float, float);

		//------------------------------------------------------
		//  後処理
		//------------------------------------------------------
		void end(ID3D11DeviceContext*);

	private:

		D3D11_VIEWPORT viewport;

		UINT instanceCount = 0;
		instance* instances = nullptr;
	};

}

//******************************************************************************



//// ポリゴン頂点構造体
//struct vertex2D {
//    float position[3];//x-y-z
//    float color[4];//r-g-b-a
//};
//bool ReadBinaryFile2(const char* filename, BYTE** data, unsigned int& size);
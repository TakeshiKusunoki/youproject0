#pragma once

#include <vector>
#include "Camera.h"
#include "../Lib_Base/Template.h"

class CameraManager : public Singleton<CameraManager>
{
	std::vector<Camera> cameraList;
public:
	CameraManager();
	~CameraManager();
};
#define pCameraManager CameraManager::getInstance()

#pragma once
//#include  <string>
#include <d3d11.h>
#include <vector>

namespace Lib_3D {
	// 画像参照
	class ResourceManager
	{
	private:
		//? ローカル構造体
		struct RESOURCE_INFO
		{
			// 変数
			std::wstring filename;
			ID3D11ShaderResourceView* p_ShaderResourceView;//シェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
			// RESOURCE_INFOの変数を要素に持つ、ローカル関数
			//? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
			RESOURCE_INFO(wchar_t* filename_,ID3D11ShaderResourceView* p_ShaderResourceView_) : p_ShaderResourceView(p_ShaderResourceView_)
			{
				filename = filename_;
			}
			//画像が消えたらcounterを減らす//画像をデリートする関数
			void Release()
			{
				if (p_ShaderResourceView)
				{
					p_ShaderResourceView->Release();
					p_ShaderResourceView = nullptr;
				}
				filename = L"";
			}
		};

		struct RESOURCE_VERTEXSHADER
		{
			// 変数
			std::wstring filename;
			ID3D11VertexShader* p_VertexShader;//シェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
			ID3D11InputLayout* p_InputLayout;
			//? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
			RESOURCE_VERTEXSHADER(wchar_t* filename_, ID3D11VertexShader* p_VertexShader_, ID3D11InputLayout* p_InputLayout_) : p_VertexShader(p_VertexShader_), p_InputLayout(p_InputLayout_)
			{
				filename = filename_;
			}
			//画像が消えたらcounterを減らす//画像をデリートする関数
			void Release()
			{

				if (p_VertexShader)
				{
					p_VertexShader->Release();
					p_VertexShader = nullptr;
				}
				if (p_InputLayout)
				{
					p_InputLayout->Release();
					p_InputLayout = nullptr;
				}
				filename = L"";
			}
		};

		struct RESOURCE_PIXELSHADER
		{
			// 変数
			std::wstring filename;
			ID3D11PixelShader* p_PixelShader;//シェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
											 //? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
			RESOURCE_PIXELSHADER(wchar_t* filename_, ID3D11PixelShader* p_PixelShader) : p_PixelShader(p_PixelShader)
			{
				filename = filename_;
			}
			//画像が消えたらcounterを減らす//画像をデリートする関数
			void Release()
			{

				if (p_PixelShader)
				{
					p_PixelShader->Release();
					p_PixelShader = nullptr;
				}
				filename = L"";
			}
		};
		std::vector<RESOURCE_INFO> ResourceInfo;
		std::vector<RESOURCE_VERTEXSHADER> ResourceVertexShader;
		std::vector<RESOURCE_PIXELSHADER> ResourcePixcelShader;
	public:
		ResourceManager() {}
		~ResourceManager() {}
		void Release();
		bool LoadShaderResourceView(ID3D11Device* , const wchar_t* ,ID3D11ShaderResourceView**const, D3D11_TEXTURE2D_DESC*const);
		bool LoadVertexShader(ID3D11Device* , const char* ,const D3D11_INPUT_ELEMENT_DESC* , const UINT , ID3D11VertexShader**const,  ID3D11InputLayout**const);
		bool LoadPixelShader(ID3D11Device*, const char*, ID3D11PixelShader**const);
		void ReleaseShaderResourceView(ID3D11ShaderResourceView*);
		void ReleaseVertexShader(ID3D11VertexShader*, ID3D11InputLayout*);
		void ReleasePixelShader(ID3D11PixelShader*);
	};
}

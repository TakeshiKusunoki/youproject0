#include "DirectX11Init.h"
#include <sstream>
#include <assert.h>
//#include "misc.h"

#define MARUTI_SAMPLING//マルチサンプリング
//#define MARUTI_THREAD_//マルチスレッドフラグ
//#define BENCH_MARK//ベンチマーク
//#define THREAD_NUM_MAX 1//7個まで

namespace Lib_3D {
	ID3D11Device* DirectX11InitDevice::p_Device = nullptr;
	ID3D11DeviceContext* DirectX11InitDevice::p_ImidiateContext = nullptr;

	DirectX11InitDevice::DirectX11InitDevice()
	{
		HRESULT hr = S_OK;

		// デバイス作成---------------------------------------------
		{
			UINT createDeviceFlag = 0;
#ifdef _DEBUG
			createDeviceFlag |= D3D11_CREATE_DEVICE_DEBUG;
			//#elif
			//		int x = 0;
			//#ifndef _MARUTI_THREAD//マルチスレッドでない
			//		createDeviceFlag = D3D11_CREATE_DEVICE_SINGLETHREADED;
			//#endif
#endif
			//ドライバータイプ
			D3D_DRIVER_TYPE driverTypes[] = {
				D3D_DRIVER_TYPE_UNKNOWN,
				D3D_DRIVER_TYPE_HARDWARE,
				D3D_DRIVER_TYPE_REFERENCE,
				D3D_DRIVER_TYPE_WARP,
			};
			const UINT NUM_DRIVER_TYPES = ARRAYSIZE(driverTypes);//featurelevelsの配列数
																 //サポートレベル
			D3D_FEATURE_LEVEL featureLevels[] =
			{
				D3D_FEATURE_LEVEL_11_0,//DirectX11のみ 9.x以降は無視
				D3D_FEATURE_LEVEL_10_1,
				D3D_FEATURE_LEVEL_10_0,
			};
			const UINT NUM_FEATURE_LEVELS = ARRAYSIZE(featureLevels);//featurelevelsの配列数


			// デバイス作成部分
			D3D_FEATURE_LEVEL featuLevelSupported;//機能レベル
			for (UINT driverTypeIndex = 0; driverTypeIndex < NUM_DRIVER_TYPES; driverTypeIndex++)
			{
				D3D_DRIVER_TYPE drvType = driverTypes[driverTypeIndex];
				hr = D3D11CreateDevice(nullptr, drvType, nullptr, createDeviceFlag, featureLevels, NUM_FEATURE_LEVELS, D3D11_SDK_VERSION,
					&p_Device, &featuLevelSupported, &p_ImidiateContext);
				if (SUCCEEDED(hr))
					break;
			}
			//_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
		}



		// デバイスチェック---------------------------------------------
		{
			//ドライバ消去処理
			hr = p_Device->GetDeviceRemovedReason();
			switch (hr)
			{
				case S_OK:
					break;
				case DXGI_ERROR_DEVICE_HUNG:
				case DXGI_ERROR_DEVICE_RESET:
				case DXGI_ERROR_DEVICE_REMOVED:
				case DXGI_ERROR_DRIVER_INTERNAL_ERROR:
				case DXGI_ERROR_INVALID_CALL:
				default:
					return;//アプリケーションを終了
					break;
			}

			//DirectX10「コンピュータシェーダ」「未処理バッファ」「構造化バッファ」のサポート調査
			D3D11_FEATURE_DATA_D3D10_X_HARDWARE_OPTIONS isSupport;
			p_Device->CheckFeatureSupport(D3D11_FEATURE_D3D10_X_HARDWARE_OPTIONS, &isSupport, sizeof(isSupport));
			if (!isSupport.ComputeShaders_Plus_RawAndStructuredBuffers_Via_Shader_4_x)
			{
				assert(!"コンピュートシェーダができません");
				return;//失敗
			}

			//マルチスレッドができるか？
			D3D11_FEATURE_DATA_THREADING isSupport2;
			hr = p_Device->CheckFeatureSupport(D3D11_FEATURE_THREADING, &isSupport2, sizeof(isSupport2));
			if (FAILED(hr))
			{
				assert(!"マルチスレッドができません");
				return;
			}
		}

	}


#define RELEASE_IF(x) if(x){x->Release();x=nullptr;}
#define DELETE_IF(x) if(x){delete x; x=NULL;}
	DirectX11InitDevice::~DirectX11InitDevice()
	{

		// デバイスステートのクリア
		if (p_ImidiateContext)p_ImidiateContext->ClearState();
		RELEASE_IF(p_ImidiateContext);
		RELEASE_IF(p_Device);
	}
#undef RELEASE_IF
#undef DELETE_IF

	ID3D11Device * DirectX11InitDevice::GetDevice()
	{
		return p_Device;
	}
	ID3D11DeviceContext * DirectX11InitDevice::GetImidiateContext()
	{
		return p_ImidiateContext;
	}















	DirectX11ComInit::DirectX11ComInit(HWND hwnd_, size_t SCREEN_WIDTH_, size_t SCREEN_HEIGHT_)
		: hwnd(hwnd_)
		, SCREEN_WIDTH(SCREEN_WIDTH_)
		, SCREEN_HEIGHT(SCREEN_HEIGHT_)
		, p_Factory(nullptr)
		, p_RenderTargetView(nullptr)
		, p_DepthStencilView(nullptr)
	{
		initialize();
	}


	DirectX11ComInit::~DirectX11ComInit()
	{
		UnInitialize();
	}



	bool DirectX11ComInit::initialize()
	{
		//---------------------------------------
		//01追加----------------------------------
		//---------------------------------------
		HRESULT hr = S_OK;


		// スワップチェイン作成---------------------------------------------
#ifdef MARUTI_SAMPLING
		BOOL flag_enable_4x_msaa = TRUE;
#else
		BOOL flag_enable_4x_msaa = FALSE;
#endif
		{
			//ファクトリー作成
			hr = CreateDXGIFactory(IID_PPV_ARGS(&p_Factory));
			if (FAILED(hr))
			{
				assert(!"CreateDXGIFactoryができません");
				return false;
			}

			{
				//スワップ チェーンを記述します。
				DXGI_SWAP_CHAIN_DESC SwapChainDesc;
				ZeroMemory(&SwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
				SwapChainDesc.BufferDesc.Width = SCREEN_WIDTH;
				SwapChainDesc.BufferDesc.Height = SCREEN_HEIGHT;
				SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
				SwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
				SwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
				SwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
				SwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
				SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
				SwapChainDesc.BufferCount = 1;
				SwapChainDesc.OutputWindow = hwnd;
				UINT msaa_quality_level;//マルチサンプリングのクオリティレベル
				pDirectX11Device->CheckMultisampleQualityLevels(SwapChainDesc.BufferDesc.Format, 4, &msaa_quality_level);
				SwapChainDesc.SampleDesc.Count = flag_enable_4x_msaa ? 4 : 1;
				SwapChainDesc.SampleDesc.Quality = flag_enable_4x_msaa ? msaa_quality_level - 1 : 0;
				SwapChainDesc.Windowed = TRUE;
				SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
				SwapChainDesc.Flags = 0;

				hr = p_Factory->CreateSwapChain(pDirectX11Device, &SwapChainDesc, &p_SwapChain);
				if (FAILED(hr))
				{
					assert(!"CreateSwapChainができません");
					return false;
				}


				// レンダーターゲットの作成
				D3D11_TEXTURE2D_DESC DepthDesk;//2D画面分割のデータ
				ZeroMemory(&DepthDesk, sizeof(DepthDesk));
				{
					// スワップチェインから最初のバックバッファを取得する
					ID3D11Texture2D* pBuckbuffer = nullptr;
					hr = p_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBuckbuffer);

					//_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
					if (FAILED(hr))
					{
						assert(!"スワップチェインから最初のバックバッファを取得するができません");
						return false;
					}


					// 描画ターゲットビューの作成
					hr = pDirectX11Device->CreateRenderTargetView(pBuckbuffer, NULL, &p_RenderTargetView);

					pBuckbuffer->GetDesc(&DepthDesk);
					if (pBuckbuffer)pBuckbuffer->Release();
					//_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
					if (FAILED(hr))
					{
						assert(!"描画ターゲットビューの作成ができません");
						return false;
					}
				}




				//　深度/ステンシルビューの作成
				{
					// 深度/ステンシル・テクスチャの作成
					ID3D11Texture2D* pDepthStencilTexture;
					D3D11_TEXTURE2D_DESC DepthStenstencilBufferDesc = DepthDesk;
					/*	DepthStenstencilBufferDesc.Width = SCREEN_WIDTH;
					DepthStenstencilBufferDesc.Height = SCREEN_HEIGHT;*/
					DepthStenstencilBufferDesc.MipLevels = 1;
					DepthStenstencilBufferDesc.ArraySize = 1;
					DepthStenstencilBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;//DXGI_FORMAT_D32_FLOAT
																					  /*	DepthStenstencilBufferDesc.SampleDesc.Count = 1;
																					  DepthStenstencilBufferDesc.SampleDesc.Quality = 0;*/
					DepthStenstencilBufferDesc.Usage = D3D11_USAGE_DEFAULT;
					DepthStenstencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
					DepthStenstencilBufferDesc.CPUAccessFlags = 0;
					DepthStenstencilBufferDesc.MiscFlags = 0;
					hr = pDirectX11Device->CreateTexture2D(&DepthStenstencilBufferDesc, NULL, &pDepthStencilTexture);//作成する２Dテクスチャの設定
					//_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
					if (FAILED(hr))
					{
						assert(!"深度/ステンシル・テクスチャの作成ができません");
						return false;
					}


					//　深度/ステンシルビューの作成
					D3D11_DEPTH_STENCIL_VIEW_DESC DescDepthStencilView;
					ZeroMemory(&DescDepthStencilView, sizeof(DescDepthStencilView));
					DescDepthStencilView.Format = DepthStenstencilBufferDesc.Format;
					DescDepthStencilView.ViewDimension = flag_enable_4x_msaa ? D3D11_DSV_DIMENSION_TEXTURE2DMS : D3D11_DSV_DIMENSION_TEXTURE2D;
					DescDepthStencilView.Texture2D.MipSlice = 0;
					DescDepthStencilView.Flags = 0;
					hr = pDirectX11Device->CreateDepthStencilView(pDepthStencilTexture, &DescDepthStencilView, &p_DepthStencilView);

					pDepthStencilTexture->Release();
					//_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
					if (FAILED(hr))
					{
						assert(!"深度/ステンシルビューの作成ができません");
						return false;
					}
				}
			}




		}

		/////////////////////////////////////
		////Added by Unit7
		/////////////////////////////////////
		//p_Blend = new BlendMode;
		p_Blend.Initializer(pDirectX11Device);
		// 追加
		p_Blend.Set(pDirectX11DeviceContext, BlendMode::ALPHA);

#ifdef MARUTI_THREAD
#endif

		return true;
	}


#define RELEASE_IF(x) if(x){x->Release();x=nullptr;}
#define DELETE_IF(x) if(x){delete x; x=NULL;}
	void DirectX11ComInit::UnInitialize()
	{
		Font::ReleaseImage();
		//RELEASE_IF(p_Blend);
		p_Blend.Release();
		// 取得したインターフェースのクリア
		RELEASE_IF(p_DepthStencilView);
		RELEASE_IF(p_RenderTargetView);
		RELEASE_IF(p_SwapChain);
		RELEASE_IF(p_Factory);
	}
#undef RELEASE_IF
#undef DELETE_IF


	HWND DirectX11ComInit::GetHwnd()
	{
		return hwnd;
	}

	UINT DirectX11ComInit::GetScreenWidth()
	{
		return SCREEN_WIDTH;
	}

	UINT DirectX11ComInit::GetScreenHeight()
	{
		return SCREEN_HEIGHT;
	}


}


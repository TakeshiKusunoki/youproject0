// UNIT12
#include "ObjAtMesh.h"
#include "misc.h"
//#include <fstream>
//#include "ResourceManager.h"
//#include "texture.h"
#include "ObjAtMeshInit.h"

namespace Lib_3D {

	ObjAtMesh::ObjAtMesh(ID3D11Device * p_Device, ObjAtMeshInit*const objAtMeshInit_)
		:objAtMeshInit(objAtMeshInit_)
	{
		HRESULT hr = S_OK;
		/////////////////////////////////////////////////
		// コンスタントバッファの作成
		/////////////////////////////////////////////////
		{
			D3D11_BUFFER_DESC buffer_desc = {};

			buffer_desc.ByteWidth = sizeof(CONSTANT_BUFFER);
			buffer_desc.Usage = D3D11_USAGE_DEFAULT;
			buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			buffer_desc.CPUAccessFlags = 0;
			buffer_desc.MiscFlags = 0;
			buffer_desc.StructureByteStride = 0;
			hr = p_Device->CreateBuffer(&buffer_desc, nullptr, &p_BufferConst);
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		}


	}

#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	ObjAtMesh::~ObjAtMesh()
	{
		RELEASE_IF(p_BufferConst);
	}
#undef RELEASE_IF
#undef DELETE_IF

	//定数
#define VERTEX_BUFFER_NUM 1//頂点バッファの数

	void ObjAtMesh::render(ID3D11DeviceContext * p_DeviceContext, const DirectX::XMFLOAT4X4 & wvp, const DirectX::XMFLOAT4X4 & world,
		std::vector<DirectX::XMFLOAT3>* const atlist, const DirectX::XMFLOAT4 & lightVector,
		const DirectX::XMFLOAT4 & materialColor, bool FlagPaint)
	{

		UINT stride[VERTEX_BUFFER_NUM] = { sizeof(ObjLoad::VERTEX) };	 //頂点バッファにふくまれる頂点データのサイズ。
		UINT offset[VERTEX_BUFFER_NUM] = { 0 };					 //頂点バッファのオフセット
		p_DeviceContext->IASetVertexBuffers(0, VERTEX_BUFFER_NUM, &objAtMeshInit->mesh.p_VertexBuffer, stride, offset);
		// �AIAにインデックスバッファを設定
		p_DeviceContext->IASetIndexBuffer(objAtMeshInit->mesh.p_IndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

		// 描画するプリミティブ種類の設定
		p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);//３角形リストを描画する
																					   // 入力レイアウト・オブジェクトの設定
		p_DeviceContext->IASetInputLayout(objAtMeshInit->p_InputLayout);

		p_DeviceContext->VSSetShader(objAtMeshInit->p_VertexShader, nullptr, 0);
		p_DeviceContext->PSSetShader(objAtMeshInit->p_PixelShader, nullptr, 0);
		//震度ステンシルステート
		p_DeviceContext->OMSetDepthStencilState(objAtMeshInit->p_DepthStencilState, 0);//震度ステンシルステート

		// �Cラスタライザ・ステート・オブジェクトの設定
		p_DeviceContext->RSSetState((FlagPaint ? objAtMeshInit->p_RasterizerStatePaint : objAtMeshInit->p_RasterizerStateLine));//ラスタライザステート


		//atセット
		if (atlist)
		{
			D3D11_VIEWPORT viewport;
			UINT numViewports = 1;
			p_DeviceContext->RSGetViewports(&numViewports, &viewport);
			auto& p = objAtMeshInit->mesh.vertexList;// エイリアス
			for (size_t i = 0; i < p.size(); i++)
			{
				p.at(i).at.x = atlist->at(i).x / viewport.Width;
				p.at(i).at.y = atlist->at(i).y / viewport.Height;
				p.at(i).at.z = atlist->at(i).z;
			}
			D3D11_MAPPED_SUBRESOURCE msr;
			p_DeviceContext->Map(objAtMeshInit->mesh.p_VertexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
			memcpy(msr.pData, p.data(), p.size());
			p_DeviceContext->Unmap(objAtMeshInit->mesh.p_VertexBuffer.Get(), 0);
		}


		for (auto& it : objAtMeshInit->mesh.materials)
		{
			// �Bコンスタントバッファを設定+		light_direction	{x=0.000000000 y=-1.00000000 z=0.000000000 ...}	DirectX::XMFLOAT4

			CONSTANT_BUFFER data = {};
			data.wvp = wvp;
			data.world = world;
			data.light_direction = lightVector;
			//data.material_color = materialColor;
			data.material_color.x = materialColor.x*it.Kd.x;
			data.material_color.y = materialColor.y*it.Kd.y;
			data.material_color.z = materialColor.z*it.Kd.z;
			data.material_color.w = materialColor.w;

			p_DeviceContext->UpdateSubresource(p_BufferConst, 0, nullptr, &data, 0, 0);//情報を定数バッファへコピー
			p_DeviceContext->VSSetConstantBuffers(0, VERTEX_BUFFER_NUM, &p_BufferConst);//定数バッファをシェーダへセット
																						//
			p_DeviceContext->PSSetShaderResources(0, 1, &it.ShaderResourceView);
			// サンプラーステートのセット
			p_DeviceContext->PSSetSamplers(0, 1, &objAtMeshInit->p_SampleState);
			for (auto its : objAtMeshInit->mesh.subset)
			{
				if (it.newmtl == its.usemtl)
				{
					p_DeviceContext->DrawIndexed(its.index_count, its.index_start, 0);//インデックスずけされているプリミティブの描画
				}
			}
		}

		// --------------

	}
#undef VERTEX_BUFFER_NUM



}

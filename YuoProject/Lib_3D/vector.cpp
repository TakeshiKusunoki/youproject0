//******************************************************************************
//
//
//      Vector
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "Vector.h"

//******************************************************************************
//
//      DirectX::XMFLOAT2
//
//******************************************************************************

//--------------------------------
//  =
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator=(const VECTOR2& v)
{
	x = v.x;
	y = v.y;
	return *this;
}

//--------------------------------
//  +=
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator+=(const DirectX::XMFLOAT2 &v)
{
	x += v.x;
	y += v.y;
	return *this;
}

//--------------------------------
//  -=
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator-=(const DirectX::XMFLOAT2 &v)
{
	x -= v.x;
	y -= v.y;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator+=(const VECTOR2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x += v.x;
	y += v.y;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator-=(const VECTOR2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x -= v.x;
	y -= v.y;
	return *this;
}

//--------------------------------
//  *=
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator*=(float f)
{
	x *= f;
	y *= f;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator*=(const DirectX::XMFLOAT2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x *= v.x;
	y *= v.y;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator*=(const VECTOR2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x *= v.x;
	y *= v.y;
	return *this;
}

//--------------------------------
//  /=
//--------------------------------
DirectX::XMFLOAT2 &VECTOR2::operator/=(float f)
{
	x /= f;
	y /= f;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator/=(const DirectX::XMFLOAT2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x /= v.x;
	y /= v.y;
	return *this;
}

DirectX::XMFLOAT2 & VECTOR2::operator/=(const VECTOR2 &v)
{
	// TODO: return ステートメントをここに挿入します
	x /= v.x;
	y /= v.y;
	return *this;
}

//--------------------------------
//  +（符号）
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator+() const
{
	return DirectX::XMFLOAT2(x, y);
}

//--------------------------------
//  -（符号）
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator-() const
{
	return DirectX::XMFLOAT2(-x, -y);
}

//--------------------------------
//  +（和）
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator+(const DirectX::XMFLOAT2 &v) const
{
	return DirectX::XMFLOAT2(x + v.x, y + v.y);
}

//--------------------------------
//  -（差）
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator-(const DirectX::XMFLOAT2 &v) const
{
	return DirectX::XMFLOAT2(x - v.x, y - v.y);
}

//--------------------------------
//  *
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator*(float f) const
{
	return DirectX::XMFLOAT2(x * f, y * f);
}

DirectX::XMFLOAT2 VECTOR2::operator*(const DirectX::XMFLOAT2& v) const
{
	return DirectX::XMFLOAT2(x * v.x, y * v.y);
}

DirectX::XMFLOAT2 VECTOR2::operator*(const VECTOR2& v) const
{
	return DirectX::XMFLOAT2(x * v.x, y * v.y);
}

//--------------------------------
//  *
//--------------------------------
XM_CONSTEXPR DirectX::XMFLOAT2 operator*(float f, const DirectX::XMFLOAT2 &v)
{
	return DirectX::XMFLOAT2(v.x * f, v.y * f);
}



//--------------------------------
//  /
//--------------------------------
DirectX::XMFLOAT2 VECTOR2::operator/(float f) const
{
	return DirectX::XMFLOAT2(x / f, y / f);
}

DirectX::XMFLOAT2 VECTOR2::operator/(const DirectX::XMFLOAT2 &v) const
{
	return DirectX::XMFLOAT2(x / v.x, y / v.y);
}

DirectX::XMFLOAT2 VECTOR2::operator/(const VECTOR2 &v) const
{
	return DirectX::XMFLOAT2(x / v.x, y / v.y);
}

//--------------------------------
//  ==
//--------------------------------
bool VECTOR2::operator == (const DirectX::XMFLOAT2& v) const
{
	return (x == v.x) && (y == v.y);
}

//--------------------------------
//  !=
//--------------------------------
bool VECTOR2::operator != (const DirectX::XMFLOAT2& v) const
{
	return (x != v.x) || (y != v.y);
}

//--------------------------------
//  ==
//--------------------------------
bool VECTOR2::operator==(const VECTOR2 & v) const
{
	return (x == v.x) && (y == v.y);
}

//--------------------------------
//  !=
//--------------------------------
bool VECTOR2::operator!=(const VECTOR2 &v) const
{
	return (x != v.x) || (y != v.y);
}

// ! 長さの2乗を取得
float VECTOR2::vec2LengthSq() const noexcept
{
	return x * x + y * y;
}

//!  長さを取得
float VECTOR2::vec2Length() const noexcept
{
	return sqrtf(vec2LengthSq());
}

//!  長さを1にする
const DirectX::XMFLOAT2& VECTOR2::vec2Normalize() const noexcept
{
	float d = vec2Length();
	if (d == 0.0f) return *this;

	return *this * (1 / d);
}




















XM_CONSTEXPR VECTOR3 operator*(float f, const DirectX::XMFLOAT3& v) noexcept
{
	return DirectX::XMFLOAT3(v.x * f, v.y * f, v.z * f);
}

////******************************************************************************
////
////      DirectX::XMFLOAT3
////
////******************************************************************************
//
//VECTOR3::operator DirectX::XMVECTOR() const
//{
//	DirectX::XMFLOAT3 temp = *this;
//	DirectX::XMVECTOR Vec = DirectX::XMLoadFloat3(&temp);
//	return Vec;
//}
//
////--------------------------------
////  =
////--------------------------------
//DirectX::XMFLOAT3 &VECTOR3::operator=(const VECTOR3& v)
//{
//	x = v.x;
//	y = v.y;
//	z = v.z;
//	return *this;
//}
//
//VECTOR3 & VECTOR3::operator=(const DirectX::XMVECTOR & other)
//{
//	DirectX::XMVECTOR temp = other;
//	DirectX::XMStoreFloat3(this, temp);
//	return *this;
//}
//
//DirectX::XMFLOAT3 & VECTOR3::operator+=(const DirectX::XMFLOAT3 & v)
//{
//	// TODO: return ステートメントをここに挿入します
//	x += v.x;
//	y += v.y;
//	z += v.z;
//	return *this;
//}
//
////--------------------------------
////  +=
////--------------------------------
//DirectX::XMFLOAT3 &VECTOR3::operator+=(const VECTOR3 &v)
//{
//	x += v.x;
//	y += v.y;
//	z += v.z;
//	return *this;
//}
//
//DirectX::XMFLOAT3 & VECTOR3::operator-=(const DirectX::XMFLOAT3 & v)
//{
//	// TODO: return ステートメントをここに挿入します
//	x -= v.x;
//	y -= v.y;
//	z -= v.z;
//	return *this;
//}
//
////--------------------------------
////  -=
////--------------------------------
//DirectX::XMFLOAT3 &VECTOR3::operator-=(const VECTOR3 &v)
//{
//	x -= v.x;
//	y -= v.y;
//	z -= v.z;
//	return *this;
//}
//
////--------------------------------
////  *=
////--------------------------------
//DirectX::XMFLOAT3 &VECTOR3::operator*=(float f)
//{
//	x *= f;
//	y *= f;
//	z *= f;
//	return *this;
//}
//
////--------------------------------
////  /=
////--------------------------------
//DirectX::XMFLOAT3 &VECTOR3::operator/=(float f)
//{
//	x /= f;
//	y /= f;
//	z /= f;
//	return *this;
//}
//
//DirectX::XMFLOAT3 & VECTOR3::operator*=(const DirectX::XMFLOAT3 & v)
//{
//	// TODO: return ステートメントをここに挿入します
//	x *= v.x;
//	y *= v.y;
//	z *= v.z;
//	return *this;
//}
//
//DirectX::XMFLOAT3 & VECTOR3::operator/=(const DirectX::XMFLOAT3 & v)
//{
//	// TODO: return ステートメントをここに挿入します
//	x /= v.x;
//	y /= v.y;
//	z /= v.z;
//	return *this;
//}
//
//DirectX::XMFLOAT3 & VECTOR3::operator*=(const VECTOR3 & v)
//{
//	// TODO: return ステートメントをここに挿入します
//	x *= v.x;
//	y *= v.y;
//	z *= v.z;
//	return *this;
//}
//
//DirectX::XMFLOAT3 & VECTOR3::operator/=(const VECTOR3 & v)
//{
//	// TODO: return ステートメントをここに挿入します
//	x /= v.x;
//	y /= v.y;
//	z /= v.z;
//	return *this;
//}
//
////--------------------------------
////  +（符号）
////--------------------------------
//DirectX::XMFLOAT3 VECTOR3::operator+() const
//{
//	return DirectX::XMFLOAT3(x, y, z);
//}
//
////--------------------------------
////  -（符号）
////--------------------------------
//DirectX::XMFLOAT3 VECTOR3::operator-() const
//{
//	return DirectX::XMFLOAT3(-x, -y, -z);
//}
//
//DirectX::XMFLOAT3 VECTOR3::operator+(const DirectX::XMFLOAT3 & v) const
//{
//	return   DirectX::XMFLOAT3(x + v.x, y + v.y, z + v.z);
//}
//
////--------------------------------
////  +（和）
////--------------------------------
//DirectX::XMFLOAT3 VECTOR3::operator+(const VECTOR3 &v) const
//{
//	return DirectX::XMFLOAT3(x + v.x, y + v.y, z + v.z);
//}
//
//DirectX::XMFLOAT3 VECTOR3::operator+(float f) const
//{
//	return DirectX::XMFLOAT3(x + f, y + f, z + f);
//}
//
//DirectX::XMFLOAT3 VECTOR3::operator-(const DirectX::XMFLOAT3 & v) const
//{
//	return  DirectX::XMFLOAT3(x - v.x, y - v.y, z - v.z);
//}
//
////--------------------------------
////  -（差）
////--------------------------------
////constexpr DirectX::XMFLOAT3 VECTOR3::operator-(const VECTOR3 &v) const
////{
////	
////}
//
//DirectX::XMFLOAT3 VECTOR3::operator-(float f) const
//{
//	return DirectX::XMFLOAT3(x - f, y - f, z - f);
//}
//
////--------------------------------
////  *
////--------------------------------
//DirectX::XMFLOAT3 VECTOR3::operator*(float f) const
//{
//	return DirectX::XMFLOAT3(x * f, y * f, z * f);
//}
//
//DirectX::XMFLOAT3 VECTOR3::operator*(const DirectX::XMFLOAT3 & v) const
//{
//	return DirectX::XMFLOAT3(x * f, y * f, z * f);
//}
//
//DirectX::XMFLOAT3 VECTOR3::operator*(const VECTOR3 & v) const
//{
//	return DirectX::XMFLOAT3(x*v.x, y*v.y, z*v.z);
//}
//
////--------------------------------
////  *
////--------------------------------
//XM_CONSTEXPR VECTOR3 operator*(float f, const DirectX::XMFLOAT3 &v) noexcept
//{
//	return DirectX::XMFLOAT3(v.x * f, v.y * f, v.z * f);
//}
//
////--------------------------------
////  /
////--------------------------------
//DirectX::XMFLOAT3 VECTOR3::operator/(float f) const
//{
//	return DirectX::XMFLOAT3(x / f, y / f, z / f);
//}
//
//DirectX::XMFLOAT3 VECTOR3::operator/(const DirectX::XMFLOAT3 & v) const
//{
//	return DirectX::XMFLOAT3(x/v.x, y/v.y, z/v.z);
//}
//
//DirectX::XMFLOAT3 VECTOR3::operator/(const VECTOR3 & v) const
//{
//	return DirectX::XMFLOAT3(x/v.x, y/v.y, z/v.z);
//}
//
////VECTOR3 VECTOR3::operator*()
////{
////	return *this;
////}
//
////--------------------------------
////  ==
////--------------------------------
//bool VECTOR3::operator==(const DirectX::XMFLOAT3 & v) const
//{
//	return (x == v.x) && (y == v.y) && (z == v.z);
//}
//
////--------------------------------
////  !=
////--------------------------------
//bool VECTOR3::operator!=(const DirectX::XMFLOAT3 & v) const
//{
//	return (x != v.x) || (y != v.y) || (z != v.z);
//}
//
////--------------------------------
////  ==
////--------------------------------
//bool VECTOR3::operator == (const VECTOR3& v) const
//{
//	return (x == v.x) && (y == v.y) && (z == v.z);
//}
//
////--------------------------------
////  !=
////--------------------------------
//bool VECTOR3::operator != (const VECTOR3& v) const
//{
//	return (x != v.x) || (y != v.y) || (z != v.z);
//}
//
////******************************************************************************

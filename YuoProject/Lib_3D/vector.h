#ifndef INCLUDED_VECTOR
#define INCLUDED_VECTOR
//******************************************************************************
//
//
//      Vectorクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <DirectXMath.h>

//==============================================================================
//
//      DirectX::XMFLOAT2クラス
//
//==============================================================================


class VECTOR2 final : public DirectX::XMFLOAT2
{
public:
	XM_CONSTEXPR VECTOR2() : DirectX::XMFLOAT2(0, 0) {}
	XM_CONSTEXPR VECTOR2(float x, float y) : DirectX::XMFLOAT2(x, y) {}
	XM_CONSTEXPR VECTOR2(const DirectX::XMFLOAT2& v) :DirectX::XMFLOAT2() { x = v.x; y = v.y; }
	~VECTOR2() = default;

	XM_CONSTEXPR DirectX::XMFLOAT2 &operator=(const VECTOR2&) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator+=(const DirectX::XMFLOAT2&) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator-=(const DirectX::XMFLOAT2&) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator+=(const VECTOR2&) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator-=(const VECTOR2&) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator*=(float) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator*=(const DirectX::XMFLOAT2&) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator*=(const VECTOR2&) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator/=(float) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator/=(const DirectX::XMFLOAT2&) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 &operator/=(const VECTOR2&) noexcept;

	XM_CONSTEXPR DirectX::XMFLOAT2 operator+() const noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 operator-() const noexcept;

	XM_CONSTEXPR DirectX::XMFLOAT2 operator+(const DirectX::XMFLOAT2&) const noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 operator-(const DirectX::XMFLOAT2&) const noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 operator*(float) const noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 operator*(const DirectX::XMFLOAT2&) const noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 operator*(const VECTOR2&) const noexcept;
	friend XM_CONSTEXPR DirectX::XMFLOAT2 operator*(float, const DirectX::XMFLOAT2&) noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 operator/(float) const noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 operator/(const DirectX::XMFLOAT2&) const noexcept;
	XM_CONSTEXPR DirectX::XMFLOAT2 operator/(const VECTOR2&) const noexcept;

	XM_CONSTEXPR bool operator == (const DirectX::XMFLOAT2&) const noexcept;
	XM_CONSTEXPR bool operator != (const DirectX::XMFLOAT2&) const noexcept;
	XM_CONSTEXPR bool operator == (const VECTOR2&) const noexcept;
	XM_CONSTEXPR bool operator != (const VECTOR2&) const noexcept;

	XM_CONSTEXPR float vec2LengthSq() const noexcept;
	XM_CONSTEXPR float vec2Length() const noexcept;
	XM_CONSTEXPR const DirectX::XMFLOAT2& vec2Normalize() const noexcept;
};

//------< プロトタイプ宣言 >-----------------------------------------------------



//==============================================================================
//
//      DirectX::XMFLOAT3クラス
//
//==============================================================================

class VECTOR3 final: public DirectX::XMFLOAT3
{
	static constexpr float EPSILON = 0.000001f;	// 誤差
public:
	XM_CONSTEXPR VECTOR3() : DirectX::XMFLOAT3(0, 0, 0) {}
	XM_CONSTEXPR VECTOR3(float x, float y, float z) : DirectX::XMFLOAT3(x, y, z) {}
	XM_CONSTEXPR VECTOR3(const DirectX::XMFLOAT3& v) : DirectX::XMFLOAT3() { x = v.x; y = v.y; z = v.z; }
	XM_CONSTEXPR VECTOR3(const DirectX::XMVECTOR& other) :DirectX::XMFLOAT3(){DirectX::XMVECTOR temp = other;DirectX::XMStoreFloat3(this, temp);}//! @brief ストア用のコンストラクタ

	~VECTOR3() = default;

	//! @brief ロード用のキャスト
	XM_CONSTEXPR operator DirectX::XMVECTOR() const noexcept
	{
		DirectX::XMFLOAT3 temp = *this;
		return DirectX::XMLoadFloat3(&temp);
	}


	XM_CONSTEXPR DirectX::XMFLOAT3 &operator=(const VECTOR3&v) noexcept
	{
		x = v.x;		y = v.y;	z = v.z;
		return *this;
	}
	//! @brief ストア用の代入
	XM_CONSTEXPR VECTOR3& operator=(const DirectX::XMVECTOR& other) noexcept
	{
		DirectX::XMVECTOR temp = other;
		DirectX::XMStoreFloat3(this, temp);
		return *this;
	}

	XM_CONSTEXPR VECTOR3& operator+=(const DirectX::XMFLOAT3&v) noexcept
	{
		x += v.x;	y += v.y;	z += v.z;
		return *this;
	}
	XM_CONSTEXPR VECTOR3& operator+=(const VECTOR3&v) noexcept
	{
		x += v.x;	y += v.y;	z += v.z;
		return *this;
	}
	XM_CONSTEXPR VECTOR3& operator-=(const DirectX::XMFLOAT3&v) noexcept
	{
		x -= v.x;	y -= v.y;	z -= v.z;
		return *this;
	}
	XM_CONSTEXPR VECTOR3& operator-=(const VECTOR3&v) noexcept
	{
		x -= v.x;	y -= v.y;	z -= v.z;
		return *this;
	}
	XM_CONSTEXPR VECTOR3& operator*=(float f) noexcept
	{
		x *= f;	y *= f;	z *= f;
		return *this;
	}
	XM_CONSTEXPR VECTOR3& operator/=(float f) noexcept
	{
		x /= f;	y /= f;	z /= f;
		return *this;
	}

	XM_CONSTEXPR VECTOR3& operator*=(const DirectX::XMFLOAT3&v) noexcept
	{
		x *= v.x;	y *= v.y;	z *= v.z;
		return *this;
	}
	XM_CONSTEXPR VECTOR3& operator/=(const DirectX::XMFLOAT3&v) noexcept
	{
		x /= v.x;	y /= v.y;	z /= v.z;
		return *this;
	}
	XM_CONSTEXPR VECTOR3& operator*=(const VECTOR3&v) noexcept
	{
		x *= v.x;	y *= v.y;	z *= v.z;
		return *this;
	}
	XM_CONSTEXPR VECTOR3& operator/=(const VECTOR3&v) noexcept
	{
		x /= v.x;	y /= v.y;	z /= v.z;
		return *this;
	}

	XM_CONSTEXPR VECTOR3 operator+() const noexcept
	{
		return { x, y, z };
	}
	XM_CONSTEXPR VECTOR3 operator-() const noexcept
	{
		return { -x, -y, -z };
	}

	XM_CONSTEXPR VECTOR3 operator+(const DirectX::XMFLOAT3&v) const noexcept
	{
		return { x + v.x, y + v.y, z + v.z };
	}
	XM_CONSTEXPR VECTOR3 operator+(const VECTOR3&v) const noexcept
	{
		return { x + v.x, y + v.y, z + v.z };
	}
	XM_CONSTEXPR VECTOR3 operator+(float f) const noexcept
	{
		return { x + f, y + f, z + f };
	}
	XM_CONSTEXPR VECTOR3 operator-(const DirectX::XMFLOAT3&v) const noexcept
	{
		return { x - v.x, y - v.y, z - v.z };
	}
	XM_CONSTEXPR VECTOR3 operator-(const VECTOR3&v) const noexcept
	{
		return { x - v.x, y - v.y, z - v.z };
	}
	XM_CONSTEXPR VECTOR3 operator-(float f) const noexcept
	{
		return { x - f, y - f, z - f };
	}
	XM_CONSTEXPR VECTOR3 operator*(float f) const noexcept
	{
		return { x * f, y * f, z * f };
	}
	XM_CONSTEXPR VECTOR3 operator*(const DirectX::XMFLOAT3&v) const noexcept
	{
		return { x * v.x, y * v.y, z * v.z };
	}
	XM_CONSTEXPR VECTOR3 operator*(const VECTOR3& v) const noexcept
	{
		return { x * v.x, y * v.y, z * v.z };
	}
	friend XM_CONSTEXPR VECTOR3 operator*(float f, const DirectX::XMFLOAT3&v) noexcept;
	XM_CONSTEXPR VECTOR3 operator/(float f) const noexcept
	{
		return { x / f, y / f, z / f };
	}
	XM_CONSTEXPR VECTOR3 operator/(const DirectX::XMFLOAT3&v) const noexcept
	{
		return { x / v.x, y / v.y, z / v.z };
	}
	XM_CONSTEXPR VECTOR3 operator/(const VECTOR3&v) const noexcept
	{
		return { x / v.x, y / v.y, z / v.z };
	}
	//VECTOR3 operator*();

	XM_CONSTEXPR bool operator == (const DirectX::XMFLOAT3&v) const noexcept
	{
		return (x == v.x) && (y == v.y) && (z == v.z);
	}
	XM_CONSTEXPR bool operator != (const DirectX::XMFLOAT3&v) const noexcept
	{
		return (x != v.x) || (y != v.y) || (z != v.z);
	}
	XM_CONSTEXPR bool operator == (const VECTOR3&v) const noexcept
	{
		return (x == v.x) && (y == v.y) && (z == v.z);
	}
	XM_CONSTEXPR bool operator != (const VECTOR3&v) const noexcept
	{
		return (x != v.x) || (y != v.y) || (z != v.z);
	}

	private:
		//! length用のみ
		template < typename T >
		XM_CONSTEXPR T sqrt_(T s)const noexcept
		{
			T x = s / 2.0;
			T prev = 0.0;

			while (x != prev)
			{
				prev = x;
				x = (x + s / x) / 2.0;
			}
			return x;
		}
	public:
	//! @brief 長さ
	XM_CONSTEXPR float Length() const noexcept
	{
		return sqrt_<float>(x * x + y * y + z * z);
	}
	//! @brief べき乗長さ
	XM_CONSTEXPR float LengthSq() const noexcept
	{
		return x * x + y * y + z * z;
	}
	//! @return 点と点との距離を返す
	XM_CONSTEXPR float Length(const DirectX::XMFLOAT3& v) const
	{
		auto V = *this - v;
		return sqrt_<float>(V.x * V.x + V.y * V.y + V.z * V.z);
	}
	//! @return 点と点との距離の2乗を返す
	XM_CONSTEXPR float LengthSq(const DirectX::XMFLOAT3& v) const
	{
		auto V = *this - v;
		return V.x * V.x + V.y * V.y + V.z * V.z;
	}
	//! @brief 正規化
	//! o,o,oなら
	XM_CONSTEXPR VECTOR3 Normalize() const noexcept
	{
		return ((*this == VECTOR3{ 0, 0, 0 }) ? { 0, 0, 0 } : { *this / Length() });
		
	}
	//! 内積
	XM_CONSTEXPR float Dot(const VECTOR3 &v) const noexcept
	{
		return x * v.x + y * v.y + z * v.z;
	}
	//! 外積
	XM_CONSTEXPR VECTOR3 Cross(const VECTOR3 &v) const noexcept
	{
		return {y * v.z - z * v.y,		z * v.x - x * v.z,	x * v.y - y * v.x};
	}


	//! 垂直関係にあるならtrue
	XM_CONSTEXPR bool IsVertical(const VECTOR3 &r) const noexcept
	{
		float d = Dot(r);
		return (-EPSILON < d && d <  EPSILON);	//誤差範囲内なら垂直と判定 //内積が０なら直角
	}

	//! 平行関係にあるならtrue
	XM_CONSTEXPR bool IsParallel(const VECTOR3 &r) const noexcept
	{
		float d = Cross(r).LengthSq();
		return (-EPSILON < d && d <  EPSILON);	//誤差範囲内なら平行と判定//外積が０なら平行
	}

	// 鋭角ならtrue
	XM_CONSTEXPR bool IsSharpAngle(const VECTOR3 &r) const noexcept
	{
		return (Dot(r) >= 0.0f);//内積が０以上なら鋭角
	}

};

//==============================================================================
//
//      VECTOR4クラス
//
//==============================================================================

class VECTOR4 final : public DirectX::XMFLOAT4
{
public:
	XM_CONSTEXPR VECTOR4() : DirectX::XMFLOAT4(0, 0, 0, 0) {}
	XM_CONSTEXPR VECTOR4(float x, float y, float z, float w) : DirectX::XMFLOAT4(x, y, z, w) {}
	~VECTOR4() = default;
};

//******************************************************************************

#endif // !INCLUDED_VECTOR
#pragma once
#include <d3d11.h>//追加

//////////////////////////////////////////////
//Unit7
////////////////////////////////////////////////

namespace Lib_3D {
	class BlendMode
	{
	public:
		enum BLEND_MODE
		{
			NONE = 0,//合成なし（デフォルト）
			ALPHA,	    //α合成
			ADD,	    //加算合成
			SUB,	    //減産合成
			REPLACE,    //置き換え
			MULTIPLY,   //乗算
			LIGHTEN,    //比較（明）
			DARKEN,	    //比較（暗）
			SCREEN,	    //スクリーン
			MODE_MAX,   //
		};
	private:
		ID3D11BlendState* BlendState[MODE_MAX];//ブレンド設定配列
		bool bLoad;//true:設定配列作成済み
		BLEND_MODE enumMode;//現在使用してるブレンドモード

	public:
		BlendMode()
		{
			BlendMode::BlendState[BlendMode::MODE_MAX] = { nullptr };
			BlendMode::bLoad = false;
			BlendMode::enumMode;
		};
		~BlendMode() { Release(); };


	public:
		bool Initializer(ID3D11Device* p_Device);
		void Release();
		//ブレンド設定用関数
		void Set(ID3D11DeviceContext* p_DeviceContext, BLEND_MODE mode = NONE);

	private:
	};
}



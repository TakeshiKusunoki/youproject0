#include "BaseRender.h"
#include	"../Lib_3D//Wrapped.h"

namespace Lib_Render {

	template<class T>
	void BaseLoder<T>::Loder(wchar_t * textureFilename[], size_t size)
	{
		//null検索 配列数決定
		if (size == 0)
		{
			for (; textureFilename[size] != nullptr; size++)
			{
			}
		}

		for (size_t i = 0; i < size; i++)
		{
			std::wstring copy = textureFilename[i];
			char* name;
			wcstombs(name, textureFilename[i], copy.size());
			fileNameList.push_back(name);
			loadDataList.emplace_back(Lib_3D::pDirectXWindowManager->GetDevice(), textureFilename[i]);
		}
	}

	template<class T>
	void BaseLoder<T>::Loder(char * textureFilename[], size_t size)
	{
		//null検索 配列数決定
		if (size == 0)
		{
			for (; textureFilename[size] != nullptr; size++)
			{
			}
		}

		for (size_t i = 0; i < size; i++)
		{
			fileNameList.push_back(textureFilename[i]);
			loadDataList.emplace_back(Lib_3D::pDirectXWindowManager->GetDevice(), textureFilename[i]);
		}
	}

	template<class T>
	void BaseLoder<T>::Release()
	{
		loadDataList.clear();
		fileNameList.clear();
	}

	template<class T>
	T* BaseLoder<T>::GetMeshData(const wchar_t *const  fileName)
	{
		char* name;
		const std::wstring copy = fileName;
		wcstombs(name, fileName, copy.size());
		for (size_t i = 0; i < loadDataList.size(); i++)
		{
			if (fileNameList[i] == name)//同じなら
				return  loadDataList[i];
		}
		return nullptr;
	}

	template<class T>
	 T * BaseLoder<T>::GetMeshData(const char * const fileName)
	{
		for (size_t i = 0; i < loadDataList.size(); i++)
		{
			if (fileNameList[i] == fileName)//同じなら
				return  loadDataList[i];
		}
		return nullptr;
	}
}
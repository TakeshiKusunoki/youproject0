#pragma once
#include <directxmath.h>
#include "BaseRender.h"

namespace Lib_Render {
	class Lib_3D::FbxBoneMesh;
	class Lib_3D::FbxBoneMeshInit;

	class FbxLoad : public BaseLoder, public Singleton<FbxLoad>
	{
	private:
	public:
	};


	//! fbxモデル描画
	class MyMesh : public BaseRender
	{
	private:
		bool					bLoad;//ロードしたか
	public:
		Lib_3D::FbxBoneMesh* mesh;//	メッシュ情報
		DirectX::XMFLOAT3	pos;//	座標
		DirectX::XMFLOAT3	angle;//	回転角度
		DirectX::XMFLOAT3	scale;//	大きさ
		DirectX::XMFLOAT4	color;//	メッシュの色


		//	情報の初期化
		void	Initialize();

		//	FBXの読込
		bool	Load(Lib_3D::FbxBoneMeshInit* const fbxBoneMeshInit);

		//	プリミティブの設定
		//	引数
		//primitive:メッシュモデル
		void	SetPrimitive(Lib_3D::FbxBoneMesh* primitive);

		//	既存MyMeshデータの使い回し
		//	引数
		//primitive:メッシュモデル
		void	SetMesh(MyMesh& org);

		//	メッシュの解放
		void	Release();

		//	引数
		//	view:ビュー変換行列
		//	projection:投影変換行列
		//  elapsed_time : タイマー
		void Render(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time);

		///bool StanbyPresant(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time);


		//	ワールド変換行列の取得
		DirectX::XMMATRIX	GetWorldMatrix();

		// アニメーションの終わり
		bool GetFlagBoneAnimationEnd()
		{
			return mesh->GetFlagBoneAnimationEnd();
		}
	};
}

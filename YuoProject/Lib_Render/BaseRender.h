#pragma once
//#include "../Lib_Vassel\BaseClass.h"
//#include "../Lib_Base/Template.h"
#include <vector>
#include <utility>

namespace Lib_Render {
	//! @breif ローダー
	template <class T>
	class BaseLoder
	{
	protected:
		//! @brief VertexPoligon3D objの実態
		std::vector<std::string> fileNameList;
		std::vector<T*> loadDataList;
	public:
		//! @brief ファイルネームからロード
		//! @param[in] textureFilename[] テクスチャ名配列
		//! @param[in] size 配列数(指定しない場合、要素の最後にはnullptrがあると判断する)
		virtual void Loder(wchar_t* textureFilename[], size_t size = 0);
		//! @brief ファイルネームからロード 要素の最後にはnullptr
		virtual void Loder(char* textureFilename[], size_t size = 0);
		//! @brief リリース
		void Release();
		//! @brief ファイル名の頂点データ取得
		T* GetMeshData(const wchar_t*const  fileName);
		//! @brief ファイル名の頂点データ取得
		T* GetMeshData(const char*const  fileName);
	public:
	};



	//! @brief 描画オブジェクトのデータ
	//! @breif レンダラーの基底クラス
	class BaseRender
	{
	public:
		virtual void Render() = 0;
	};


}

//
////! @brief 描画オブジェクト管理クラス
//class ObjRenderManager : public Singleton<BaseRenderer>
//{
//protected:
//public:
//	ObjRenderManager();
//	~ObjRenderManager();
//	void Add();
//	void Update() override;
//protected:
//};

#pragma once
#include <directxmath.h>
#include "BaseRender.h"
#include "../Lib_3D/XVertexMesh.h"
#include "../Lib_Base/Template.h"

namespace Lib_Render {
	class LoderXVertexMesh : public BaseLoder<Lib_3D::XVertexMesh>, public Singleton<LoderXVertexMesh>
	{
	private:
	public:

	};
#define pLoderXVertexMesh LoderXVertexMesh::getInstance()

	//! @brief ３角形ポリゴン描画クラス
	//! @details この中の値は一切動的に変更しない
	class RenderXVertexMesh : public BaseRender/*, public AlignedAllocationPolicy<16>*/
	{
	private:
		bool bLoad;
		Lib_3D::XVertexMesh _obj;
		const DirectX::XMFLOAT3*	_pos;//!	座標
		const DirectX::XMVECTOR*	_orientation;//!	回転角度
		const DirectX::XMFLOAT3*	_scale;//!	大きさ
		const std::vector<DirectX::XMFLOAT3>& at;
		const std::vector<DirectX::XMFLOAT2>& texDistance;
		DirectX::XMMATRIX _view;
		DirectX::XMMATRIX _projection;
		//const DirectX::XMFLOAT4*	_color;//!	メッシュの色
		//const DirectX::XMFLOAT4* _lightVector;//! 光方向
		//const DirectX::XMFLOAT4* _camerapos;//! カメラ位置
		//const DirectX::XMFLOAT4* _lightColor;//! 光の色
		//const DirectX::XMFLOAT4* _newtoralColor;//! 自然光
	public:
		RenderXVertexMesh(const Lib_3D::XVertexMeshInit* vertexData,
			const std::vector<DirectX::XMFLOAT3>* at, const std::vector<DirectX::XMFLOAT2>* texDistance,
			const DirectX::XMFLOAT3* pos, const DirectX::XMVECTOR*	orientation, const DirectX::XMFLOAT3*	scale
		);
		~RenderXVertexMesh() {}


		//	情報の初期化
		/*void	Initialize();*/

		//	FBXの読込
		//bool	Load(const wchar_t * fbx_filename);

		//	既存MyMeshデータの使い回し
		//	引数
		//primitive:メッシュモデル
		//void	SetMesh(RenderXVertexMesh& org);

		//	プリミティブの設定
		//	引数
		//primitive:メッシュモデル
		void	VertexChange_(Lib_3D::XVertexMeshInit* vertexPoligon3D);

		//ゲッター
		//const Poligon3D& GetObj();

		//	メッシュの解放
		//void	Release();

		//! @brief ビューの取得(値渡し)
		void SetWorldView(const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& projection);

		//! @brief それぞれのrender関数を呼ぶ
		void Render()override;

		//	ワールド変換行列の取得
		DirectX::XMMATRIX	GetWorldMatrix()const;
	private:
	};
}


#pragma once
#include <directxmath.h>
#include "..\Lib_3D\\Sprite3D.h"
#include "BaseRender.h"

//! @brief 3Dポリゴン1個描画
class RenderSprite3D : public Lib_Render::BaseRender
{
public:
	Lib_3D::Sprite3D* obj;
	DirectX::XMFLOAT3	pos;//	座標
	DirectX::XMFLOAT3	angle;//	回転角度
	DirectX::XMFLOAT3	scale;//	大きさ
	DirectX::XMFLOAT4	color;//	メッシュの色
public:
	RenderSprite3D();
	~RenderSprite3D();
	void Render();

};


#include "RenderXVertexMesh.h"
#include	"../Lib_3D\\Wrapped.h"
#include "Camera.h"

namespace Lib_Render {

	Lib_Render::RenderXVertexMesh::RenderXVertexMesh(const Lib_3D::XVertexMeshInit * vertexData,
		const std::vector<DirectX::XMFLOAT3>* at, const std::vector<DirectX::XMFLOAT2>* texDistance,
		const DirectX::XMFLOAT3 * pos, const DirectX::XMVECTOR * orientation, const DirectX::XMFLOAT3 * scale)
		: _pos(pos)
		, _vertex(vertex)
		, _texpos(texpos)
		, _orientation(orientation)
		, _scale(scale)
		, _obj(Lib_3D::pDirectXWindowManager->GetDevice(), const_cast<VertexPoligon3D *>(vertexData))
	{
	}

	void Lib_Render::RenderXVertexMesh::SetWorldView(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection)
	{
	}

	void Lib_Render::RenderXVertexMesh::Render()
	{
		SetWorldView(pCameraManager->GetProjection(), pCameraManager->GetView_());

		DirectX::XMMATRIX matrix = GetWorldMatrix();
		//	Matrix -> Float4x4 �ϊ�
		DirectX::XMFLOAT4X4 world_view_projection;
		DirectX::XMFLOAT4X4 world;
		DirectX::XMStoreFloat4x4(&world, matrix);
		DirectX::XMStoreFloat4x4(&world_view_projection, matrix * _projection * _view);
		_obj.RenderXVertexMesh(Lib_3D::pDirectXWindowManager->GetDeviceContext(), world_view_projection, world, at, texDistance);
	}

	DirectX::XMMATRIX	 Lib_Render::RenderXVertexMesh::GetWorldMatrix()const
	{
		//����]�p�x�ϊ�
		DirectX::XMMATRIX R = DirectX::XMMatrixRotationQuaternion(*_orientation);
		DirectX::XMMATRIX S = DirectX::XMMatrixScaling(_scale->x, _scale->y, _scale->z);
		DirectX::XMMATRIX matrix = DirectX::XMMatrixIdentity();
		matrix = R*S;
		matrix.r[3].m128_f32[0] = _pos->x;
		matrix.r[3].m128_f32[1] = _pos->y;
		matrix.r[3].m128_f32[2] = _pos->z;

		return matrix;
	}

}
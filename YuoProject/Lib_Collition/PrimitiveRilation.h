#pragma once
#include "PrimitivClass.h"
#include <math.h>

/**
* @file PrimitiveRilation.h
* @brief 〇〇クラスが記述されている
* @details 幾何学図形同士の関係
*/





namespace Lib_Collition {

	//! 点と直線の最短距離
	//! @param[in]  p : 点
	//! @param[in]  l : 直線
	//! @param[out] h : 点から下ろした垂線の足（戻り値）
	//! @param[out]  t :ベクトル係数（戻り値）
	//! @return 戻り値: 最短距離
	inline float CalculatePointLineDist(const Point &p, const  Line  &l, Point &h, float &t)
	{
		float lenSqV = l.v.LengthSq();
		t = 0.0f;
		if (lenSqV > 0.0f)
			t = l.v.Dot(p() - l.p()) / lenSqV;

		h.p = l.p() + t * l.v;
		return static_cast<VECTOR3>(h() - p()).Length();
	}


	//! ∠p1p2p3は鋭角？
	//! param[in] 角をなす3つの頂点座標 p1→p2→p3
	XM_CONSTEXPR inline bool isSharpAngle(const Point &p1, const Point &p2, const Point &p3) noexcept
	{
		return static_cast<VECTOR3>(p1.p - p2.p).IsSharpAngle(p3.p - p2.p);
	}
	
	//! 点と線分の最短距離
	//! @param[in] p : 点
	//! @param[in]  seg : 線分
	//! @param[out]  h : 最短距離となる端点（戻り値）
	//! @param[out]  t : 端点位置（ t < 0: 始点の外, 0 <= t <= 1: 線分内, t > 1: 終点の外 ）
	//! @return 戻り値: 最短距離
	inline float CalculatePointSegmentDist(const Point &p, const Segment &seg, Point &h, float &t)
	{
		// 垂線の長さ、垂線の足の座標及びtを算出
		float len = CalculatePointLineDist(p, Line(seg.start, seg.end.p - seg.start.p), h, t);

		if (isSharpAngle(p, seg.start, seg.end) == false)
		{
			// 始点側の外側
			h = seg.start;
			return static_cast<VECTOR3>(seg.start.p - p.p).Length();
		}
		else if (isSharpAngle(p, seg.end, seg.start) == false)
		{
			// 終点側の外側
			h = seg.end;
			return static_cast<VECTOR3>(seg.end.p - p.p).Length();
		}

		return len;
	}
	
	//! 2直線の最短距離
	//! @param[in] l1 : L1
	//! @param[in] l2 : L2
	//! @param[out] p1 : L1側の垂線の足（戻り値）
	//! @param[out] p2 : L2側の垂線の足（戻り値）
	//! @param[out] t1 : L1側のベクトル係数（戻り値）
	//! @param[out] t2 : L2側のベクトル係数（戻り値）
	//! @return 戻り値: 最短距離
	inline float CalculateLineLineDist(const Line &l1, const Line &l2, Point &p1, Point &p2, float &t1, float &t2)
	{

		// 2直線が平行？
		if (l1.v.IsParallel(l2.v) == true)
		{
			// 点P11と直線L2の最短距離の問題に帰着
			float len = CalculatePointLineDist(l1.p, l2, p2, t2);
			p1 = l1.p;
			t1 = 0.0f;
			return len;
		}

		// 2直線はねじれ関係
		float DV1V2 = l1.v.Dot(l2.v);
		float DV1V1 = l1.v.LengthSq();
		float DV2V2 = l2.v.LengthSq();
		VECTOR3 P21P11 = l1.p.p - l2.p.p;
		t1 = (DV1V2 * l2.v.Dot(P21P11) - DV2V2 * l1.v.Dot(P21P11)) / (DV1V1 * DV2V2 - DV1V2 * DV1V2);
		p1 = l1.LineOnPoint(t1);
		t2 = l2.v.Dot(p1.p - l2.p.p) / DV2V2;
		p2 = l2.LineOnPoint(t2);

		return static_cast<VECTOR3>(p2.p - p1.p).Length();
	}

	//! 2直線の最短距離時の交差点
	//! @param[in] line1 : L1
	//! @param[in] line2 : L2
	//! @param[out] p1 : L1上の交点（戻り値）
	//! @param[out] p2 : L2上の交点（戻り値）
	//! @return 2直線の最短距離
	inline float CalculateLineLineDist_(const Line &line1, const Line &line2, Point*const p1 = nullptr, Point*const p2 = nullptr)
	{
		VECTOR3 c0 = line1.p().Cross((line2.p() - line1.p())),
			c1 = line1.p().Cross(line2.p());
		float t0 = c0.Dot(c1) / c0.LengthSq();

		Point
			p1_{ line1.p() + t0 * line1.p() },
			p2_{ line1.p() + t0 * line1.p() };

		if (p1 == nullptr)
			return p1_.p.Length(p2_());
		*p1 = p1_;
		if (p2 == nullptr)
			return p1_.p.Length(p2_());
		*p2 = p2_;
		return p1_.p.Length(p2_());
	}

	//! 0〜1の間にクランプ
	inline void clamp01(float &v)
	{
		if (v < 0.0f)
			v = 0.0f;
		else if (v > 1.0f)
			v = 1.0f;
	}

	//! 2線分の最短距離
	//! @param[in] s1 : S1(線分1)
	//! @param[in] s2 : S2(線分2)
	//! @param[out] p1 : S1側の垂線の足（戻り値）
	//! @param[out] p2 : S2側の垂線の足（戻り値）
	//! @param[out] t1 : S1側のベクトル係数（戻り値）
	//! @param[out] t2 : S2側のベクトル係数（戻り値）
	//! @return 戻り値: 最短距離
	[[deprecated]] inline float CalculateSegmentSegmentDist(const Segment &s1, const Segment &s2, Point &p1, Point &p2, float &t1, float &t2)
	{
		constexpr float EPSILON = 0.000001f;
		// S1が縮退している？
		if (s1.directiron.LengthSq() < EPSILON)
		{
			// S2も縮退？
			if (s2.directiron.LengthSq() < EPSILON)
			{
				// 点と点の距離の問題に帰着
				float len = static_cast<VECTOR3>(s2.p - s1.p).Length();
				p1 = s1.start;
				p2 = s2.start;
				t1 = t2 = 0.0f;
				return len;
			}
			else
			{
				// S1の始点とS2の最短問題に帰着
				float len = CalculatePointSegmentDist(s1.start, s2, p2, t2);
				p1 = s1.start;
				t1 = 0.0f;
				clamp01(t2);
				return len;
			}
		}

		// S2が縮退している？
		else if (s2.directiron.LengthSq() < EPSILON)
		{
			// S2の始点とS1の最短問題に帰着
			float len = CalculatePointSegmentDist(s2.start, s1, p1, t1);
			p2 = s2.start;
			clamp01(t1);
			t2 = 0.0f;
			return len;
		}

		/* 線分同士 */

		// 2線分が平行だったら垂線の端点の一つをP1に仮決定
		if (s1.directiron.IsParallel(s2.directiron) == true)
		{
			t1 = 0.0f;
			p1 = s1.start;
			float len = CalculatePointSegmentDist(s1.start, s2, p2, t2);
			if (0.0f <= t2 && t2 <= 1.0f)
				return len;
		}
		else
		{
			// 線分はねじれの関係
			// 2直線間の最短距離を求めて仮のt1,t2を求める
			float len = CalculateLineLineDist(s1.ThisLine(), s2.ThisLine(), p1, p2, t1, t2);
			if (
				0.0f <= t1 && t1 <= 1.0f &&
				0.0f <= t2 && t2 <= 1.0f
				)
			{
				return len;
			}
		}

		// 垂線の足が外にある事が判明
		// S1側のt1を0〜1の間にクランプして垂線を降ろす
		clamp01(t1);
		p1 = s1.ThisLine().LineOnPoint(t1);
		float len = CalculatePointSegmentDist(p1, s2, p2, t2);
		if (0.0f <= t2 && t2 <= 1.0f)
			return len;

		// S2側が外だったのでS2側をクランプ、S1に垂線を降ろす
		clamp01(t2);
		p2 = s2.ThisLine().LineOnPoint(t2);
		len = CalculatePointSegmentDist(p2, s1, p1, t1);
		if (0.0f <= t1 && t1 <= 1.0f)
			return len;

		// 双方の端点が最短と判明
		clamp01(t1);
		p1 = s1.ThisLine().LineOnPoint(t1);
		return static_cast<VECTOR3>(p2 - p1).Length();
	}






	//! ある点から平面までの距離
	inline float CaluculatePointToPlane(const Point& point, const Plane& plane)
	{
		float l{ point().Dot(plane.n) };//射影長
		return plane.GetOriginNearPiliodDistance() - l;
	}

	//! ある点から１番近い平面の点
	inline const VECTOR3& NearPosAboutPointAndPlane(const Point& point, const Plane& plane)
	{
		float l{ point().Dot(plane.n) };//射影長
		float distance{ plane.GetOriginNearPiliodDistance() - l };//ある点から平面までの距離
		return point() + plane.n * distance;
	}


	//! 内分ベクトル
	//! @param[in] v1 ベクトル
	//! @param[in] v2 ベクトル
	//! @param[in] ratio 比 (a : 1 - a)
	//! @return (v1-v2)ベクトルを比で内分する点へのベクトル
	XM_CONSTEXPR inline VECTOR3 InternalDivisionVectorl(const VECTOR3& v1, const VECTOR3& v2, float ratio) noexcept
	{
		return (v1* (1 - ratio)) + v2*ratio ;
	}
	
	//! 内分比 (a : 1 - a = n1 : n2)
	//! @param n1
	//! @param n2
	//! @return ratio 比
	XM_CONSTEXPR inline float SeekInternalRatio(float n1, float n2)
	{
		return{ n1 / (n1 + n2) };
	}

	//! 平面と直線の交点
	XM_CONSTEXPR inline Point CaluculateInterSectionPlaneWithLine(const Plane& plane, const Line& line)
	{
		VECTOR3 vec = line.p.CalcVectorToPlane(plane);//点から平面への最短距離ベクトル

		float cos = line.v.Normalize().Dot(vec.Normalize());//cos
		if (cos == 0)
			return Point();
		float noname = vec.Length() / cos;//ななめ
		return Point{ line.p() + line.v.Normalize() * noname };
	}


	XM_CONSTEXPR void b()
	{
		XM_CONSTEXPR auto b = Point();
		
		XM_CONSTEXPR auto m = b.p.Normalize();
		XM_CONSTEXPR auto n = CaluculateInterSectionPlaneWithLine(Plane(8, VECTOR3(1, 1, 1)), Line(Point{}, VECTOR3(1, 1, 1)));
	}
}








#pragma once
/**
* @file CollsionDitection.h
* @brief 当たり判定選別用のパーツクラス(外で明確な実体を作る)
* @details  CollitonDitection、CollisionSwitchだけ外で使う
*/
#include "PrimitivClass.h"

namespace Lib_Collition {
	//! マップがキーを持っているか調べる
	//! @return マップがキーを持っているならtrue
	[[nodiscard]] bool IsKeyExist(const PrimitiveVariant& cl1, const PrimitiveVariant& cl2);

	//! 当たり判定の種類ごとに当たり判定をさせる
	//! @param[in] cl1 当たり判定オブジェクト1
	//! @param[in] cl2 当たり判定オブジェクト2
	[[nodiscard]] bool CollisionSwitch(const PrimitiveVariant& cl1, const PrimitiveVariant&  cl2);
}



#pragma once
#include "PrimitiveRilation.h"
/**
* @file CollisionFunc.h
* @brief 〇〇クラスが記述されている
* @details 幾何学図形同士の衝突判定
*/
namespace
{
	//! length用のみ
	template < typename T >
	[[nodiscard]] XM_CONSTEXPR T sqrt_(T s)noexcept
	{
		T x = s / 2.0;
		T prev = 0.0;
		while (x != prev)
		{
			prev = x;
			x = (x + s / x) / 2.0;
		}
		return x;
	}
}
namespace Lib_Collition
{
	//! 当たり判定関数--------------------------------------------
	//! 直線と平面の衝突
	//! @param[in] line 線
	//! @param[in] plane 平面
	[[nodiscard]] XM_CONSTEXPR inline bool CollisionLinePlane(const PrimitiveVariant& uLine, const PrimitiveVariant& uPlane) noexcept
	{
		Line line = std::get<Line>(uLine);
		Plane plane = std::get<Plane>(uPlane);

		//線ベクトルと法線が垂直に交わる
		if (line.v.IsVertical(plane.n))
			return false;
		return true;
	}
	
	//逆
	[[nodiscard]] XM_CONSTEXPR inline bool CollisionLinePlaneRe( const PrimitiveVariant& uPlane, const PrimitiveVariant& uLine) noexcept
	{
		return CollisionLinePlane(uLine, uPlane);
	}

	//! カプセル同士の衝突判定(カプセルは球を線分分動かしたものなので・・)
	//! @param[in] c1 : S1(線分1)
	//! @param[in] c2 : S2(線分2)
	//! @return 戻り値: 衝突していたらtrue
	[[nodiscard]] inline bool CollisionCapsuleCapsule(const PrimitiveVariant& uCapsule0, const PrimitiveVariant& uCapsule1)
	{
		Capsule capsule0 = std::get<Capsule>(uCapsule0);
		Capsule capsule1 = std::get<Capsule>(uCapsule0);
		float d = CalculateSegmentSegmentDist(capsule0.s, capsule1.s);
		return (d <= capsule0.r + capsule1.r);
	}

	//! レイと球の衝突
	//! @param[in] lay 半直線
	//! @param[in] sphere 球
	//! @return あたってたらtrue
	[[nodiscard]] XM_CONSTEXPR inline bool CalcRaySphere(
		const PrimitiveVariant& uLine,
		const PrimitiveVariant& uSphere
	)
	{
		Line line = std::get<Line>(uLine);
		Sphere sphere = std::get<Sphere>(uSphere);

		VECTOR3 pos = sphere.p() - line.p();
		float A = line.v.x * line.v.x + line.v.y * line.v.y + line.v.z * line.v.z;
		float B = line.v.x * pos.x + line.v.y * pos.y + line.v.z * pos.z;
		float C = pos.x * pos.x + pos.y * pos.y + pos.z * pos.z - sphere.r * sphere.r;

		if (A == 0.0f)
			return false; // レイの長さが0
		float s = B * B - A * C;
		if (s < 0.0f)
			return false; // 衝突していない
		s = sqrt_<float>(s);
		float a1 = (B - s) / A;
		float a2 = (B + s) / A;

		if (a1 < 0.0f || a2 < 0.0f)
			return false; // レイの反対で衝突
		return true;
	}


	//! 三角形と点の当たり判定(３Dの場合)
	// 点と三角形は同一平面上にあるものとしています。同一平面上に無い場合は正しい結果になりません
	// 線上は外とみなします。
	// ABCが三角形かどうかのチェックは省略...
	//! @param[in] point 点
	//! @param[in] tryPoligon 三角形ポリゴン
	//! @return 戻り値    true:三角形の内側に点がある    false:三角形の外側に点がある
	[[nodiscard]] XM_CONSTEXPR inline bool CollisionPointTryangle(const PrimitiveVariant& uPoint, const PrimitiveVariant& uTryangle) noexcept
	{
		Point point = std::get<Point>(uPoint);
		Triangle tryangle = std::get<Triangle>(uTryangle);

		VECTOR3 AB = tryangle.p2.p - tryangle.p1.p;
		VECTOR3 BP = point.p - tryangle.p2.p;

		VECTOR3 BC = tryangle.p3.p - tryangle.p2.p;
		VECTOR3 CP = point.p - tryangle.p3.p;

		VECTOR3 CA = tryangle.p1.p - tryangle.p3.p;
		VECTOR3 AP = point.p - tryangle.p1.p;

		VECTOR3 c1 = AB.Cross(BP);
		VECTOR3 c2 = BC.Cross(CP);
		VECTOR3 c3 = CA.Cross(AP);

		//内積で順方向か逆方向か調べる
		double dot_12 = c1.Dot(c2);
		double dot_13 = c1.Dot(c3);

		if (dot_12 > 0 && dot_13 > 0)//三角形の内側に点がある
			return true;
		return false;//三角形の外側に点がある

	}


	//! 線分と平面の衝突判定
	[[nodiscard]] inline bool CollisionSegmentPlane(const PrimitiveVariant& uSeg, const PrimitiveVariant& uPlane) noexcept
	{
		Segment seg = std::get<Segment>(uSeg);
		Plane plane = std::get<Plane>(uPlane);

		const PrimitiveVariant uLine(seg.ThisLine());
		if (!CollisionLinePlane(uLine, uPlane))
			return false;
		//法線ベクトルnとv1,v2の内積をとって、その符号が異なると線分は平面と衝突している
		if (plane.n.Dot(seg.end.p) * plane.n.Dot(seg.start.p) > 0)
			return false;
		return true;
	}

	//! 線分と板ポリゴン
	[[nodiscard]] XM_CONSTEXPR inline bool CollisionSegmentSquare(const PrimitiveVariant& uSeg, const PrimitiveVariant& uSquare)
	{
		Segment seg = std::get<Segment>(uSeg);
		Square square = std::get<Square>(uSquare);

		if (!CollisionLinePlane(seg, plane))
			return false;
		//法線ベクトルnとv1,v2の内積をとって、その符号が異なると線分は平面と衝突している
		if (plane.n.Dot(seg.end.p) * plane.n.Dot(seg.start.p) > 0)
			return false;
	}



	//! 平面とAABBとの交差判定
	//! @param[in] aabb AABB
	//! @param[in] plane 平面
	//! @return 衝突しているならtrue
	[[nodiscard]] XM_CONSTEXPR inline bool CollisionPlaneAABB(const PrimitiveVariant& uAABB, const PrimitiveVariant& uPlane) noexcept
	{
		AABB aabb = std::get<AABB>(uAABB);
		Plane plane = std::get<Plane>(uPlane);

		// 対角線上
		VECTOR3 pos_[8] = {
				{ aabb._MinPos.p.x, aabb._MinPos.p.y, aabb._MinPos.p.z },
				{ aabb._MinPos.p.x, aabb._MaxPos.p.y, aabb._MaxPos.p.z },
				{ aabb._MinPos.p.x, aabb._MaxPos.p.y, aabb._MinPos.p.z },
				{ aabb._MaxPos.p.x, aabb._MinPos.p.y, aabb._MaxPos.p.z },
				{ aabb._MinPos.p.x, aabb._MinPos.p.y, aabb._MaxPos.p.z },
				{ aabb._MaxPos.p.x, aabb._MaxPos.p.y, aabb._MinPos.p.z },
				aabb._MinPos.p,
				aabb._MaxPos.p
		};
		VECTOR3 vvvv = plane.n * plane.ThisOriginPiliodDistance();
		float k = aabb.DiagonalLength();

		for (size_t i = 0; i < 4; i++)
		{
			float verMin = (pos_[i * 2] - vvvv).Dot(plane.n);//原点p.nベクトルがどちら方向へのびるか
			if (k < verMin || k < -verMin)
				break;
			float verMax = (pos_[i * 2 + 1] - vvvv).Dot(plane.n);
			if (k < verMax || k < -verMax)
				break;
			//min頂点（負）が平面の表にあるか
			if (verMin * verMax < 0)
				return true;
		}
		return false;
	}
}
	////! カプセル同士の衝突判定(カプセルは球を線分分動かしたものなので・・)
	////! @param[in] c1 : S1(線分1)
	////! @param[in] c2 : S2(線分2)
	////! @return 戻り値: 衝突していたらtrue
	//[[nodiscard]] inline bool CollisionCapsuleCapsule(const Capsule &c1, const Capsule &c2)
	//{
	//	Point p1, p2;
	//	float t1, t2;
	//	float d = CalculateSegmentSegmentDist(c1.s, c2.s, p1, p2, t1, t2);
	//	return (d <= c1.r + c2.r);
	//}

	//namespace
	//{
	//	//! length用のみ
	//	template < typename T >
	//	XM_CONSTEXPR T sqrt_(T s)noexcept
	//	{
	//		T x = s / 2.0;
	//		T prev = 0.0;
	//		while (x != prev)
	//		{
	//			prev = x;
	//			x = (x + s / x) / 2.0;
	//		}
	//		return x;
	//	}
	//}
	////! レイと球の衝突
	////! @param[in] lay 半直線
	////! @param[in] sphere 球
	////! @return あたってたらtrue
	//[[nodiscard]] XM_CONSTEXPR inline bool CalcRaySphere(const Line& line, const Sphere& sphere) noexcept
	//{
	//	VECTOR3 pos = sphere.p.p - line.p.p;
	//	float A = line.v.x * line.v.x + line.v.y * line.v.y + line.v.z * line.v.z;
	//	float B = line.v.x  * pos.x + line.v.y * pos.y + line.v.z * pos.z;
	//	float C = pos.x * pos.x + pos.y * pos.y + pos.z * pos.z - sphere.r * sphere.r;

	//	if (A == 0.0f)
	//		return false; // レイの長さが0
	//	float s = B * B - A * C;
	//	if (s < 0.0f)
	//		return false; // 衝突していない
	//	s = sqrt_<float>(s);
	//	float a1 = (B - s) / A;
	//	float a2 = (B + s) / A;

	//	if (a1 < 0.0f || a2 < 0.0f)
	//		return false; // レイの反対で衝突
	//	return true;
	//}
	//
	//
	////! 三角形と点の当たり判定(３Dの場合)
	//// 点と三角形は同一平面上にあるものとしています。同一平面上に無い場合は正しい結果になりません
	//// 線上は外とみなします。
	//// ABCが三角形かどうかのチェックは省略...
	////! @param[in] point 点
	////! @param[in] tryPoligon 三角形ポリゴン
	////! @return 戻り値    true:三角形の内側に点がある    false:三角形の外側に点がある
	//[[nodiscard]] XM_CONSTEXPR inline bool CollisionPointTryangle(Point point, Triangle tryPoligon) noexcept
	//{
	//	VECTOR3 AB = tryPoligon.p2() - tryPoligon.p1();
	//	VECTOR3 BP = point() - tryPoligon.p2();

	//	VECTOR3 BC = tryPoligon.p3() - tryPoligon.p2();
	//	VECTOR3 CP = point() - tryPoligon.p3();

	//	VECTOR3 CA = tryPoligon.p1() - tryPoligon.p3();
	//	VECTOR3 AP = point() - tryPoligon.p1();
	//	
	//	VECTOR3 c1 = AB.Cross(BP);
	//	VECTOR3 c2 = BC.Cross(CP);
	//	VECTOR3 c3 = CA.Cross(AP);
	//	//内積で順方向か逆方向か調べる
	//	double dot_12 = c1.Dot(c2);
	//	double dot_13 = c1.Dot(c3);

	//	if (dot_12 > 0 && dot_13 > 0)//三角形の内側に点がある
	//		return true;
	//	return false;//三角形の外側に点がある

	//}
	//
	////! 直線と平面の衝突
	//[[nodiscard]] inline XM_CONSTEXPR bool CollisionLinePlane(const Line& line, const Plane& plane) noexcept
	//{
	//	//線ベクトルと法線が垂直に交わる
	//	if (line.v.IsVertical(plane.n))
	//		return false;
	//	return false;
	//}

	////! 直線と平面の衝突 逆
	//[[nodiscard]] inline XM_CONSTEXPR bool CollisionLinePlaneRe(const Plane& plane, const Line& line)
	//{
	//	return CollisionLinePlane(line, plane);
	//}
	//
	////! 線分と平面の衝突判定
	//[[nodiscard]] inline XM_CONSTEXPR bool CollisionSegmentPlane(const Segment& seg, const Plane& plane)
	//{
	//	if (seg.directiron.IsVertical(plane.n))
	//		return false;
	//	//法線ベクトルnとv1,v2の内積をとって、その符号が異なると線分は平面と衝突している
	//	if (plane.n.Dot(seg.end.p) * plane.n.Dot(seg.start.p) > 0)
	//		return false;
	//	return true;
	//}

	////! 線分と板ポリゴン
	//[[nodiscard]] inline XM_CONSTEXPR bool CollisionSegmentSquare(const Segment& seg, const Plane& plane)
	//{
	//	if (!CollisionLinePlane(seg.ThisLine(), plane))
	//		return false;
	//	//法線ベクトルnとv1,v2の内積をとって、その符号が異なると線分は平面と衝突している
	//	if (plane.n.Dot(seg.end.p) * plane.n.Dot(seg.start.p) > 0)
	//		return false;
	//}
	//

	////! 平面とAABBとの交差判定
	////! @param[in] aabb AABB
	////! @param[in] plane 平面
	////! @return 衝突しているならtrue
	//[[nodiscard]] XM_CONSTEXPR inline bool CollisionPlaneAABB(const AABB& aabb, const Plane& plane) noexcept
	//{
	//	// 対角線上
	//	VECTOR3 pos_[8] = {
	//			{ aabb._MinPos.p.x, aabb._MinPos.p.y, aabb._MinPos.p.z },
	//			{ aabb._MinPos.p.x, aabb._MaxPos.p.y, aabb._MaxPos.p.z },
	//			{ aabb._MinPos.p.x, aabb._MaxPos.p.y, aabb._MinPos.p.z },
	//			{ aabb._MaxPos.p.x, aabb._MinPos.p.y, aabb._MaxPos.p.z },
	//			{ aabb._MinPos.p.x, aabb._MinPos.p.y, aabb._MaxPos.p.z },
	//			{ aabb._MaxPos.p.x, aabb._MaxPos.p.y, aabb._MinPos.p.z },
	//			aabb._MinPos.p,
	//			aabb._MaxPos.p
	//	};
	//	VECTOR3 vvvv = plane.n * plane.d;
	//	float k = aabb.DiagonalLength();

	//	for (size_t i = 0; i < 4; i++)
	//	{
	//		float verMin = (pos_[i * 2] - vvvv).Dot(plane.n);//原点p.nベクトルがどちら方向へのびるか
	//		if (k < verMin || k < -verMin)
	//			break;
	//		float verMax = (pos_[i * 2 + 1] - vvvv).Dot(plane.n);
	//		if (k < verMax || k < -verMax)
	//			break;
	//		//min頂点（負）が平面の表にあるか
	//		if (verMin * verMax < 0)
	//			return true;
	//	}
	//	return false;
	//}
	

////! レイと球の衝突座標を算出
////! @param[in] lx, ly, lz : レイの始点
////! @param[in] vx, vy, vz : レイの方向ベクトル
////! @param[in] px, py, pz : 球の中心点の座標
////! @param[in] r : 球の半径
////! @param[out] q1x, q1y, q1z: 衝突開始点（戻り値）
////! @param[out] q2x, q2y, q2z: 衝突終了点（戻り値）
//bool calcRaySphere(
//	float lx, float ly, float lz,
//	float vx, float vy, float vz,
//	float px, float py, float pz,
//	float r,
//	float &q1x, float &q1y, float &q1z,
//	float &q2x, float &q2y, float &q2z
//)
//{
//	px = px - lx;
//	py = py - ly;
//	pz = pz - lz;

//	float A = vx * vx + vy * vy + vz * vz;
//	float B = vx * px + vy * py + vz * pz;
//	float C = px * px + py * py + pz * pz - r * r;

//	if (A == 0.0f)
//		return false; // レイの長さが0

//	float s = B * B - A * C;
//	if (s < 0.0f)
//		return false; // 衝突していない

//	s = sqrtf(s);
//	float a1 = (B - s) / A;
//	float a2 = (B + s) / A;

//	if (a1 < 0.0f || a2 < 0.0f)
//		return false; // レイの反対で衝突

//	q1x = lx + a1 * vx;
//	q1y = ly + a1 * vy;
//	q1z = lz + a1 * vz;
//	q2x = lx + a2 * vx;
//	q2y = ly + a2 * vy;
//	q2z = lz + a2 * vz;

//	return true;
//}
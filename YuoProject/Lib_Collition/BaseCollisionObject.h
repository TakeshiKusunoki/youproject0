#pragma once
#include "PrimitivClass.h"
#include "..//Lib_Base/Property.h"

#define PRIMITIV_ARRAY(Type) Lib_Collition::PrimitiveVariantIndex::index_of_t<Type>::value

namespace Lib_Collition
{
	//! T : 当たり判定タイプ(todo でないならコンパイルエラーするようにする...)
	template<typename T>
	class BaseCollisionObject
	{
		//! 当たり判定形
		_PROPERTY_READABLE_REFERENCE(PrimitiveVariantC<PRIMITIV_ARRAY(T)>, CollisionShape)
		
		//! プリミティブ型操作api(当たり判定型はこれを介して、操作する)
		_PROPERTY_READABLE_REFERENCE(std::function(T), PrimitiveContolole);

		_PROPERTY_WRITEABLE(int ,n)
		//! letter for octtree
		_PROPERTY_READABLE(LeterForCollisonObject, Leter)
	public:
		//! param[in] 当たり判定
		constexpr BaseCollisionObject()
			:_CollisionShape(T{})
		{
			//_CollisionShape.GetThis();
			//_CollisionShape.GetVariant().index();

			_PrimitiveContolole = std::function(T);//タイプから関数を選ぶ

			this->_Leter._PrimitiveV = this->_CollisionShape.GetVariant();
		}

		void Leter()
		{
			
		}

		//!
		void gh()
		{
			PrimitiveContolole(_CollisionShape);
		}
	};
}//Lib_Collition



#pragma once

#include <DirectXMath.h>

using namespace DirectX;
namespace Lib_Collition {

	// 三角形ポリゴンの法線を求める
	const XMVECTOR&& TriangleNormal(XMVECTOR p0, XMVECTOR p1, XMVECTOR p2)
	{
		XMVECTOR v10 = XMVectorSubtract(p1, p0);
		XMVECTOR v20 = XMVectorSubtract(p2, p0);
		XMVECTOR nor = XMVector3Cross(v10, v20);
		return XMVector3Normalize(nor);
	}

	// 三角形ポリゴンの角度を求める
	// ベクトルp1-p0とp2-p0の角度
	float TriangleAngle(XMVECTOR p0, XMVECTOR p1, XMVECTOR p2)
	{
		XMVECTOR v10 = XMVector3Normalize(XMVectorSubtract(p1, p0));
		XMVECTOR v20 = XMVector3Normalize(XMVectorSubtract(p2, p0));
		// 内積 v10・v20 = |v10||v20|cosθ
		// 正規化しているので|v10| = |v20| = 1
		XMVECTOR cs = XMVector3Dot(v10, v20);//cosθ
		return XMScalarACos(XMVectorGetX(cs));//θ：2つのベクトルの角度
	}

	// 三角形の面積を求める
	float TriangleArea(XMVECTOR p0, XMVECTOR p1, XMVECTOR p2)
	{
		XMVECTOR v10 = XMVectorSubtract(p1, p0);
		XMVECTOR v20 = XMVectorSubtract(p2, p0);
		XMVECTOR cross = XMVector3Cross(v10, v20);
		// 外積の長さ＝v10とv20で作られる平行四辺形の面積
		// 半分にすると三角形の面積
		return XMVectorGetX(XMVector3Length(cross))*0.5f;
	}









}
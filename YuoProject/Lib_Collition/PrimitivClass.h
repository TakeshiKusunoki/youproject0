#ifndef __PRIMITIVE_CLASS_H__
#define __PRIMITIVE_CLASS_H__

#pragma once
#include <math.h>
#include <array>
#include <typeinfo>
#include "../Lib_3D/vector.h"
#include "../Lib_Base/Property.h"
#include "..//Lib_Base/Variant.h"
#include <vector>
#include <variant>
#include <functional>
#include <map>

namespace Lib_Collition
{
	struct Line;
	struct Segment;
	//! 平面
	struct Plane final
	{
		VECTOR3 n = VECTOR3();//! 法線
		float height = 0;//! 原点からの高さ
		Plane() = default;
		XM_CONSTEXPR Plane(const float height, const VECTOR3& n)
			: height(height)
			, n(n)
		{};
		DEFAULT_COPY_MOVE_CONSTRACTOR(Plane)
		~Plane() = default;

		//! 原点からの距離
		XM_CONSTEXPR float ThisOriginPiliodDistance() const
		{
			return VECTOR3(0, height, 0).Dot(n);
		}

		//! 原点から１番近い点
		XM_CONSTEXPR const VECTOR3& GetOriginNearPiliodPos() const noexcept
		{
			return ThisOriginPiliodDistance() * n;
		}
	};

	//! 点
	//! ポリモーフィズムを利用しない
	struct Point final
	{
		VECTOR3 p = VECTOR3();//! 座標

		Point() = default;
		XM_CONSTEXPR Point(float x, float y, float z) : p(x, y, z) {}
		XM_CONSTEXPR explicit Point(const DirectX::XMFLOAT3& v) : p(v) {}
		XM_CONSTEXPR Point(const VECTOR3& v) : p(v) {}
		XM_CONSTEXPR Point(const DirectX::XMVECTOR& other) : p(other) {}
		DEFAULT_COPY_MOVE_CONSTRACTOR(Point)
		~Point() = default;

		//デバッグでは遅い[[deprecated]] のけた
		XM_CONSTEXPR const VECTOR3& operator()() const noexcept
		{
			return this->p;
		}

		//! @点から平面へ下したベクトル(点から平面への最短距離ベクトル)
		//! length-p.d
		XM_CONSTEXPR VECTOR3 CalcVectorToPlane(const Plane& plane)const
		{
			float l = this->p.Dot(plane.n);
			float l2 = plane.ThisOriginPiliodDistance() - l;
			return { plane.n * l };//点から平面への最短距離ベクトル
		}

		//! 点から直線へ下したベクトル(点から直線への最短距離ベクトル)
		//! @param[in]  p : 点
		//! @param[in]  l : 直線
		//! @return 戻り値: 直線上の点の座標
		XM_CONSTEXPR const Point& CalcVectorToLine(const Line& l)const;

		//! 点と直線の最短距離
		//! @param[in]  l : 直線
		//! @return 戻り値: 最短距離
		XM_CONSTEXPR float CalculatePointLineDist(const Line& l)const;

		//! この点からp1、p2へのびる角は鋭角か？
		//! param[in] 角をなす3つの頂点座標 this→p2 this→p1
		//! @return 鋭角ならtrue
		XM_CONSTEXPR bool isSharpAngle(const Point& p1, const Point& p2);

		/*XM_CONSTEXPR const Point& LineOnPoint(float t) const
		{
			return Point(p + t * v);
		}*/
	private:

	};

	//! 直線
	struct Line final
	{
		Point p = Point();//! 線上のある点
		VECTOR3 v = VECTOR3(1.0f, 0.0f, 0.0f);//! 方向ベクトル

		Line() = default;
		XM_CONSTEXPR Line(const Point& p, const VECTOR3& v) : p(p), v(v) {}
		DEFAULT_COPY_MOVE_CONSTRACTOR(Line)
			~Line() = default;

		//! @return AABB
		XM_CONSTEXPR const AABB& ThisAABB() const noexcept
		{
			return { p , 0 };
		}

	};


	//! 線分
	struct Segment final
	{
		Point start = Point();//! 始点座標
		Point end = Point();//! 終点座標

		Segment() = default;
		XM_CONSTEXPR Segment(const Point& p, const Point& p2) : start(p), end(p2) {}
		DEFAULT_COPY_MOVE_CONSTRACTOR(Segment)
		~Segment() = default;
		//! 傾き
		XM_CONSTEXPR VECTOR3 ThisDirection() const noexcept
		{
			return (end.p - start.p).Normalize();
		}

		//! 内分比 1:2なら　i = 1; k = 3
		//! @param i 分子
		//! @param k 分母
		XM_CONSTEXPR const VECTOR3& GetInternalDivision(float i, float k) const noexcept
		{
			float l_ = start.p.Length() * i / k;
			return start.p + start.p.Normalize() * l_;
		}
		//! @brief 線分
		XM_CONSTEXPR const Line& ThisLine()const noexcept
		{
			return Line(start, ThisDirection());
		}

		//! @return AABB
		XM_CONSTEXPR const AABB& ThisAABB() const noexcept
		{
			Point halfPos = (end() - start()) / 2 + start();//真ん中の座標
			VECTOR3 hl;//辺の長さ
			hl.x = fabsf(start.p.x - halfPos.p.x);
			hl.y = fabsf(start.p.y - halfPos.p.y);
			hl.z = fabsf(start.p.z - halfPos.p.z);
			return {halfPos , hl};
		}
	};




	//! 球
	struct Sphere final
	{
		Point p = Point();//! 中心点
		float r = 0;	// 半径
		Sphere() = default;
		XM_CONSTEXPR Sphere(const Point& p, float r) : p(p), r(r) {}
		DEFAULT_COPY_MOVE_CONSTRACTOR(Sphere)
		~Sphere() = default;
		//! @return AABB
		XM_CONSTEXPR const AABB& ThisAABB() const noexcept
		{
			return { p , r };
		}
	};


	//! カプセル
	struct Capsule final
	{
		Segment s{ Point(), Point() };//! 線分
		float r = 0;	//! 半径

		Capsule() = default;
		XM_CONSTEXPR Capsule(const Segment& s, float r) : s(s), r(r) {}
		XM_CONSTEXPR Capsule(const Point& p1, const Point& p2, float r) : s(p1, p2), r(r) {}
		DEFAULT_COPY_MOVE_CONSTRACTOR(Capsule)
		~Capsule() = default;

		//! @return AABB
		XM_CONSTEXPR const AABB& ThisAABB() const/* noexcept*/
		{
			auto halfl = (s.end() - s.start()).Length() / 2 + r;//カプセルのの半分の長さ
			
			Point halfPos = (s.end() - s.start()) / 2 + s.start();//真ん中の座標
			auto tol = (s.end() - halfPos()).Dot({1,0,0});//射影長
			auto ansl = halfl - tol;
			auto ansv = s.ThisDirection() * ansl + s.end();

			VECTOR3 hl;//辺の長さ
			hl.x = fabsf(ansv.x - halfPos.p.x);
			hl.y = fabsf(ansv.y - halfPos.p.y);
			hl.z = fabsf(ansv.z - halfPos.p.z);
			return { halfPos , hl };
		}
	};

	//! AABB
	struct AABB final
	{
		Point _MinPos = Point();	//! １番座標上で小さい座標
		Point _MaxPos = Point();	//! １番座標上で大きい座標
		//float diagonalLength;
		//VECTOR3 _hl;	// 各軸の辺の長さの半分
		AABB() = default;
		//! @param[in] p 中心点
		//! @param[in] hl 各軸の辺の長さの半分
		XM_CONSTEXPR AABB(const Point& p, const VECTOR3& hl) /*: _hl(hl)*/
		{
			_MinPos.p = p.p - hl;
			_MaxPos.p = p.p + hl;
		}
		//! @param[in] p 中心点
		//! @param[in] hl 最大幅
		XM_CONSTEXPR AABB(const Point& p, const float hl)/* : _hl{ hl, hl, hl }*/
		{
			_MinPos.p = p.p - hl / 2;
			_MaxPos.p = p.p + hl / 2;
		}
		DEFAULT_COPY_MOVE_CONSTRACTOR(AABB)
			~AABB() = default;

		//! @対角線の距離
		XM_CONSTEXPR float DiagonalLength() const noexcept
		{
			return _MinPos.p.Length(_MaxPos.p);
		}


		// 辺の長さを取得
		/*float lenX() const { return _hl.x * 2.0f; };
		float lenY() const { return _hl.y * 2.0f; };
		float lenZ() const { return _hl.z * 2.0f; };
		float len(int i) const
		{
			return *((&_hl.x) + i) * 2.0f;
		}*/
	};

	//! @brief 三角形ポリゴン
	struct Triangle final
	{
		Point p1 = Point();//! ポリゴンの頂点
		Point p2 = Point();//! ポリゴンの頂点
		Point p3 = Point();//! ポリゴンの頂点
		Triangle() = default;
		XM_CONSTEXPR Triangle(const Point& p1_, const Point& p2_, const Point& p3_) :p1(p1_), p2(p2_), p3(p3_) {}
		DEFAULT_COPY_MOVE_CONSTRACTOR(Triangle)
			~Triangle() = default;
		//! @return 線分
		XM_CONSTEXPR const std::array<Segment, 3> & GetSegment() const
		{
			return std::array<Segment, 3>{Segment{ p1, p2 }, Segment{ p2, p3 }, Segment{ p3, p1 }};
		}
	};


	//! 四角形ポリゴン
	struct Square final
	{
		Point p1;//! ポリゴンの頂点
		Point p2;//! ポリゴンの頂点
		Point p3;//! ポリゴンの頂点
		Point p4;//! ポリゴンの頂点
		Square() = default;
		XM_CONSTEXPR explicit Square(const VECTOR3& n) {};
		~Square() = default;
		float GetOriginNearPiliodDistance() = delete;
		VECTOR3 GetOriginNearPiliodPos() = delete;
	};















	using PrimitiveVariant = std::variant<
		std::monostate,
		Plane,
		Point,
		Line,
		Segment,
		Sphere,
		Capsule,
		Triangle,
		Square,
		AABB
	>;

	using PrimitiveVariantIndex = Lib_Base::VariantT<
		std::monostate,
		Plane,
		Point,
		Line,
		Segment,
		Sphere,
		Capsule,
		Triangle,
		Square,
		AABB
	>;

	template<int I>
	using PrimitiveVariantC =Lib_Base::VariantC<I,
		std::monostate,
		Plane,
		Point,
		Line,
		Segment,
		Sphere,
		Capsule,
		Triangle,
		Square,
		AABB
	>;

	
//	namespace
//	{
//#define MACRO(type0, type1) std::make_pair(PrimitiveVariantIndex::index_of_t<type0>::value,PrimitiveVariantIndex::index_of_t<type1>::value)
//		
//		std::map<std::pair<std::size_t, std::size_t>, bool(*)()> mip{
//			{MACRO(Plane, Point),}
//		};
//		
//		template<std::size_t i, class tVariant>
//		void printElement(tVariant const& iVariant0, tVariant const& iVariant1)
//		{
//			constexpr std::size_t n = std::variant_size<tVariant>::size;
//			constexpr std::size_t siz0 = i % n;//1番目のindex
//			constexpr std::size_t siz1 = i / n;//2番目のindex
//			using t0 = std::variant_alternative< siz0, tVariant >::type;
//			using t1 = std::variant_alternative< siz1, tVariant >::type;
//			auto var0 = std::get<siz0>(iVariant0);//1番目のvariantの型
//			auto var1 = std::get<siz1>(iVariant1);//2番目のvariantの型
//			auto pair = std::make_pair(siz0, siz1);
//			const auto& func = mip[pair];
//			func(iVariant0, iVariant1);
//		}
//
//		template<class tVariant, std::size_t... indices>
//		void printImpl(tVariant const& iVariant0, tVariant const& iVariant1, std::size_t i, std::index_sequence<indices...>)
//		{
//			using Func = void (*)(tVariant const&);
//			Func aFuncArray[] =
//			{
//				{(printElement<indices, tVariant>)...}
//			};
//			aFuncArray[i](iVariant0, iVariant1);
//		}
//
//		template<class tVariant>
//		void print(tVariant const& iVariant0, tVariant const& iVariant1)
//		{
//			constexpr std::size_t n = std::variant_size<tVariant>::size;
//			printImpl(iVariant0, iVariant1, iVariant0.index() + iVariant1.index() * n, std::make_index_sequence<n*n>{});
//		}
//	}
	
	

	

	////! 最初に一回だけ型を決めるvariant
	//template<const int I>
	//class PrimitiveVariantC_
	//{
	//	///using PrimitiveType = PrimitiveVariant::at_t<I>::type;
	//	_PROPERTY_READABLE_REFERENCE(PrimitiveVariant, Variant)
	//		constexpr static int n = 0;
	//private:
	//public:
	//	PrimitiveVariantC_(int n)
	//		:_Variant(n)
	//	{
	//		constexpr int x[3];
	//		constexpr auto xx = array_length(x);
	//		sizeof(PrimitiveVariant);
	//	}
	//	//PrimitiveVariant::at_t<I>::type
	//	
	//	PrimitiveVariant::at_t<I>::type GetThis()
	//	{

	//		return Lib_Base::apply_visitor(Lib_Base::is_this_visitor_t<I, PrimitiveVariant>(), _Variant);
	//	}

	//	void x(PrimitiveVariant::at_t<I>::type v)
	//	{}
	//};

	

	struct VisitorGetAABB
	{
	
		AABB operator()(Plane p) { return inf; }
		AABB operator()(Point p) { return inf; }
		AABB operator()(Line p) { return inf; }
		AABB operator()(Segment p) { return p.ThisAABB(); }
		AABB operator()(Sphere p) { return p.ThisAABB(); }
		AABB operator()(Capsule p) { return p.ThisAABB(); }
		AABB operator()(Triangle p) { return p.ThisAABB(); }
		AABB operator()(Square p) { return p.ThisAABB(); }
		AABB operator()(AABB p) { return p; }
	private:
		static constexpr AABB inf{ Point(INFINITY,INFINITY,INFINITY), VECTOR3(INFINITY,INFINITY,INFINITY) };
	};

	int x()
	{
		PrimitiveVariantIndex::index_of_t<Point>::value;
		Lib_Base::metatemplate::index_of_t<Plane, PrimitiveVariant >::value;
		PrimitiveVariantC<1> n{};
		n.GetVariant();
		n.GetThis();
		auto x_ = n.GetThis();
		
		auto x = std::variant_alternative<1, PrimitiveVariant>::type{};
		PrimitiveVariant pj;
		auto l = std::get<2>( pj );

		PrimitiveVariantC<0>::at_t;
		n.GetThis();
		std::visit(Visi{}, ly);
	}
	

	
	//+ -------------------------------
	//! 当たり判定形共用体
	union PrimitiveUnion
	{
		_PROPERTY_READABLE(const std::type_info&, PrimitiveType)
	public:
		Plane plane;
		Point point;
		Line line;
		Segment seg;
		Sphere sphere;
		Capsule capsule;
		Triangle tryangle;
		Square square;
		AABB aabb;
		XM_CONSTEXPR explicit PrimitiveUnion::PrimitiveUnion(const Plane& p)
			: plane(p)
			, _PrimitiveType(typeid(p))
		{}
		XM_CONSTEXPR explicit PrimitiveUnion::PrimitiveUnion(const Point& p)
			: point(p)
			, _PrimitiveType(typeid(p))
		{}
		XM_CONSTEXPR explicit PrimitiveUnion::PrimitiveUnion(const Line& p)
			: line(p)
			, _PrimitiveType(typeid(p))
		{}
		XM_CONSTEXPR explicit PrimitiveUnion::PrimitiveUnion(const Segment& p)
			: seg(p)
			, _PrimitiveType(typeid(p))
		{}
		XM_CONSTEXPR explicit PrimitiveUnion::PrimitiveUnion(const Sphere& p)
			: sphere(p)
			, _PrimitiveType(typeid(p))
		{}
		XM_CONSTEXPR explicit PrimitiveUnion::PrimitiveUnion(const Capsule& p)
			: capsule(p)
			, _PrimitiveType(typeid(p))
		{}
		XM_CONSTEXPR explicit PrimitiveUnion::PrimitiveUnion(const Triangle& p)
			: tryangle(p)
			, _PrimitiveType(typeid(p))
		{}
		XM_CONSTEXPR explicit PrimitiveUnion::PrimitiveUnion(const Square& p)
			: square(p)
			, _PrimitiveType(typeid(p))
		{}
		XM_CONSTEXPR explicit PrimitiveUnion::PrimitiveUnion(const AABB& p)
			: aabb(p)
			, _PrimitiveType(typeid(p))
		{}
		DEFAULT_COPY_MOVE_CONSTRACTOR(PrimitiveUnion)
		~PrimitiveUnion() = default;

		
	};
}
#endif






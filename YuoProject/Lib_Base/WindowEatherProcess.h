#pragma once


#include "WinMainClass.h"

namespace Lib_Base {
	class WindowCleate;
	//! @brief このクラスをウインドウ生成マネージャーから呼んでやる
	class WindowEatherProcess
	{
	public:
		//! @brief ウインドウごとにさせたい処理
		//! @param[in] ウインドウ情報を引数でもらえる
		virtual void WinEatherProcess(WindowCleate* win) = 0;
	};


}


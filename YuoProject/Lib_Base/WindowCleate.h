#pragma once
/**
* @file WindowCleate.h
* @brief Window生成クラスが記述されている
* @details 詳細な説明
*/
#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <Windows.h>
#include <list>
#include "WindowEatherProcess.h"

class WindowEatherProcess;
namespace Lib_Base {

	// ウインドウ定義関数の関数ポインタの型
	//#define InitWindowFuncPoint(x)		BOOL(*x)(HWND hwnd, char* _className, int left, int top, size_t width, size_t height)
	typedef BOOL(*WIN_INIT_TYPE)(HWND& hwnd, char* _className, int left, int top, size_t width, size_t height);

	/**
	* @brief ウインドウ生成クラス
	* @par 詳細
	* ウインドウ生成クラス
	*/
	class WindowCleate
	{
		friend class WindowCreateManager;
	private:
		//! ウインドウハンドル
		HWND _hWnd;
		char* _className;
		int _left;
		int _top;
		//! ウインドウ大きさ幅(管理クラスから取得する)
		size_t _windowWidth;
		//! ウインドウ大きさ高さ(管理クラスから取得する)
		size_t _windowHeight;
		//! カーソル位置
		POINT	_cursorPos;
		//! @brief 関数ポインタ
		WIN_INIT_TYPE _InitWindowFunc;
		//InitWindowFuncPoint(_InitWindowFunc);

	public:
		WindowCleate() {}
		~WindowCleate() {}
		//! @brief  ゲッター-----------
		HWND GetHWnd();
		//! @brief  ゲッター-
		size_t GetWindowWidth();
		//! @brief  ゲッター-
		size_t GetWindowHeight();
		//! @brief  ゲッター-
		const char* GetClassName_();
		//! @brief  ゲッター-
		POINT GetCursorPos_();


	private:
		//! @brief 表示するウィンドウの定義、登録、表示(InitInstanceより先に呼ぶ)
		//! @param[in] hInstance インスタンス
		//! @param[in] windowSort 作るウインドウの種類
		//  return BOOL　：正常終了のとき１、異常終了のとき０
		void InitWindow();

		//! @brief ウインドウ定義を消す
		void UnInit();

		//! @brief カーソル座標の更新
		void UpdateCursorPos();

	};


	/**
	* @brief ウインドウ生成管理クラス
	* @par 詳細
	* ウインドウ生成管理クラス　(シングルトン) <br>
	* ウインドウ情報を調べるときはウインドウクラス名から検索する<br>
	* ウインドウの破棄もこのクラスで管理する
	*/
	class WindowCreateManager
	{
	private:
		//! 定義されたウインドウのリスト
		std::list<WindowCleate> _windowList;
	public:
		//! @brief シングルトン
		static WindowCreateManager* GetInstance()
		{
			static WindowCreateManager instance;
			return &instance;
		}
		//! @brief リストに追加
		//! @detaile ウインドウ作成
		//! @param[in] initWindow 作成するウインドウの定義
		//! @param[in] className ウインドウクラス名
		//! @param[in] left ウインドウの左上頂点位置(左)
		//! @param[in] top ウインドウの左上頂点位置(上)
		//! @param[in] windowWidth ウインドウ大きさ幅
		//! @param[in] windowHeigh ウインドウ大きさ高さ
		void AddWindow(WIN_INIT_TYPE initWindow, char* className, int left, int top, size_t width, size_t height);

		void DestoroyAllWindow();
		//! @brief メインウインドウ以外破棄
		//! @details これを呼ぶとウインドウを破棄
		//! @param[in]  className 引数で指定したクラス名以外のクラスを破棄
		void DestoroyAllSubWindow(const char*  className);
		//! @brief ウインドウ破棄
		//! @details クラス名からウインドウ破棄
		void DestoroyWindowWithClassName(const char*  className);

		//! @brief ウインドウごとに処理をさせる
		//! param[in] させたい処理
		void ProcessExectuteEachWindow(WindowEatherProcess* process);


		//! @brief ゲッター-----------------------------------
		// リストサイズ取得
		size_t GetListSize();
		//! @brief  ゲッター-
		WindowCleate* GetWindowWithClassName(const char*  className);

	private:
	};

	//! @def pWindowCreateManager
	//! @brief ウインドウ生成管理クラスのインスタンス
#define pWindowCreateManager WindowCreateManager::GetInstance()

}
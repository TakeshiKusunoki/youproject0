#pragma once
#include "..//Lib_Base/variant_util.h"
#include <string.h>
namespace Lib_Base
{
	template <class Visitor, class Variant>
	typename std::invoke_result<Visitor(const Variant&)>::type
		apply_visitor(Visitor visitor, const Variant& var)
	{
		return visitor(var);
	}


	template <typename T>
	struct get_visitor_t
	{
		template <class Variant>
		const T& operator()(const Variant& var) const
		{
			static_assert(Variant::template contains_t<T>::value, "");
			return *reinterpret_cast<const T * const>(var.storage());
		}
	};

	template <typename T>
	struct assign_visitor_t
	{
		const T& value_;
		assign_visitor_t(const T& value) : value_(value)
		{}
		template <class Variant>
		void operator()(Variant& var) const
		{
			static_assert(Variant::template contains_t<T>::value, "");
			static_assert(sizeof(T) <= Variant::kStorageSize, "");
			var.mutable_which() = Variant::template index_of_t<T>::value;
			std::memset(var.mutable_storage(), 0, Variant::kStorageSize);
			new (var.mutable_storage()) T(value_);
		}
	};

	//! 
	template <int I, class Variant>
	struct is_equal_visitor_t
	{
		const Variant& other_;
		is_equal_visitor_t(const Variant& other) : other_(other)
		{}
		bool operator()(const Variant& var) const
		{
			// valueの型に一致するVisitorを再帰的に探す
			if (var.which() != I)
			{
				return apply_visitor(is_equal_visitor_t<I - 1, Variant>(other_), var);
			}

			using T = typename Variant::template at_t<I>::type;
			return var.template get<T>() == other_.template get<T>();
		}
	};

	template <int I, class Variant>
	struct is_this_visitor_t
	{
		//! 再起探索
		[[nodiscard]] constexpr auto operator()(const Variant& var) const
		{
			using T = typename Variant::template at_t<I>::type;
			// 番号があるまで再帰して探す
			if (var.which() != I)
			{
				auto x = is_this_visitor_t<I - 1, Variant>(var);
			}
			return var.template get<T>();
		}
	};

	template <typename ...Types>
	class VariantT
	{
	public:
		static const int kNumTypes = sizeof...(Types);                //!< 型数
		static const int kStorageSize = metatemplate::max_size_t<Types...>::value;  //!< 最大サイズを持つ型のサイズ
	private:
		const int which_;                 //!< 現在セットされている値の型
		char storage_[kStorageSize]; //!< 値をセットする領域
	public:
		 //! 指定した型が含まれているか調べるメタ関数
		template <typename T>
		using contains_t = metatemplate::contains_t<T, Types...>;
		//! 指定した型のインデックスを返すメタ関数
		template <typename T>
		using index_of_t = metatemplate::index_of_t<T, Types...>;
		//! 指定したインデックスの型を返すメタ関数
		template <int I>
		using at_t = metatemplate::at_t<I, Types...>;

		constexpr VariantT(int n) : which_(n)
		{
			std::memset(storage_, 0, kStorageSize);
		};
		template <typename T>
		VariantT& operator=(const T& value)
		{
			assign(value);
			return *this;
		}

		constexpr int which() const
		{
			return which_;
		}

		const char* const storage() const
		{
			return storage_;
		}

		bool operator==(const VariantT& other) const
		{
			if (which() != other.which())
			{
				return false;
			}
			return apply_visitor(is_equal_visitor_t<kNumTypes - 1, VariantT>(other), *this);
		}

		auto get_()
		{
			return apply_visitor(is_this_visitor_t<kNumTypes - 1, VariantT>(), *this);
		}

		template <typename T>
		const T& get() const
		{
			return apply_visitor(get_visitor_t<T>(), *this);
		}
	private:
		void assign(const T& value)
		{
			apply_visitor(assign_visitor_t<T>(value), *this);
		}
		
	};

	//調べるようVariant_
	template <typename ...Types>
	struct VariantS
	{
		static const int kNumTypes = sizeof...(Types);                //!< 型数
		static const int kStorageSize = metatemplate::max_size_t<Types...>::value;  //!< 最大サイズを持つ型のサイズ

		//! 指定した型が含まれているか調べるメタ関数
		template <typename T>
		using contains_t = metatemplate::contains_t<T, Types...>;
		//! 指定した型のインデックスを返すメタ関数
		template <typename T>
		using index_of_t = metatemplate::index_of_t<T, Types...>;
		//! 指定したインデックスの型を返すメタ関数
		template <int I>
		using at_t = metatemplate::at_t<I, Types...>;
	};


	//! 最初に一回だけ型を決めるvariant
	template <int I, typename ...Types>
	class VariantC
	{
		///using PrimitiveType = PrimitiveVariant::at_t<I>::type;
		using val = std::variant<char, short, int, long>;
		_PROPERTY_READABLE(std::variant<Types...>, Variant)
	private:
	public:
		using at_t = metatemplate::at_t<I, Types...>;

		VariantC() = default;
		//PrimitiveVariant::at_t<I>::type
		
		/*Variant_<Types...>::at_t<I>::type*/
		//! 自身が保持している値を返す
		constexpr std::variant_alternative_t<I, std::variant<Types...>> GetThis() const
		{
			return std::get<I>(_Variant);
			//return apply_visitor(Lib_Base::is_this_visitor_t<I, std::variant<Types...>>(), _Variant);
		}
	};

	// !Variant__の最初に一回だけ型を決めるvariant
	template <int I, typename ...Types>
	class VariantTC
	{
		_PROPERTY_READABLE(VariantT<Types...>, Variant)
	private:
	public:
		constexpr VariantTC(int n) 
			: _Variant(n)
		{
		};
		using at_t = VariantT<Types...>::at_t<I>::type;

		//PrimitiveVariant::at_t<I>::type

		/*Variant_<Types...>::at_t<I>::type*/
		//! 自身が保持している値を返す
		constexpr at_t GetThis() const
		{
			return _Variant.get<at_t>();
			//return apply_visitor(Lib_Base::is_this_visitor_t<I, std::variant<Types...>>(), _Variant);
		}
	};
}// Lib_Base

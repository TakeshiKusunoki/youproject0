#include "WinMainClass.h"
#include <sstream>
//! ベンチマークするフラグ
#define BENCH_MARK_




namespace Lib_Base {
	//+ WinMainClass---------------------------------------------------------
	WinMainClass* WinMainClass::_my_instance = NULL;
	WinMainClass* WinMainClass::_my_previnstance = NULL;

	PSTR   WinMainClass::_cmd_lin_ = nullptr;
	HINSTANCE  WinMainClass::_hInstance = nullptr;
	int WinMainClass::_nShowCmd = 0;
	HINSTANCE WinMainClass::_prev_instance_ = nullptr;
	high_resolution_timer WinMainClass::_Timer;

	WinMainClass::WinMainClass()
	{
		_my_instance = this;
		_my_previnstance = nullptr;
	}


	int WinMainClass::Boot()
	{
		//WinMainClass();

		//初期化
		Init();

		//メインループ
		MsgLoop();

		//終了処理
		UnInit();

		//メインループ切り替え//my_instanceがＮＵＬＬなら終了
		if (_my_instance)
			WinMainClass::_my_instance->Boot();

		return 0;
	}

	void WinMainClass::MsgLoop()
	{
		LONGLONG CountsPerSecond;
		LONGLONG CountsPerFrame;//1/60秒
		LONGLONG NextCounts;
		LARGE_INTEGER LargeInteger;

		QueryPerformanceFrequency(&LargeInteger);
		CountsPerSecond = LargeInteger.QuadPart;
		CountsPerFrame = CountsPerSecond / 60;

		// 時間計測開始
		QueryPerformanceCounter(&LargeInteger);
		NextCounts = LargeInteger.QuadPart;

		MSG msg;
		do
		{
			Sleep(1);
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) { DispatchMessage(&msg); }
			else
			{
				// フレーム制御
				LARGE_INTEGER count;
				QueryPerformanceCounter(&count);
#ifndef BENCH_MARK
				if (NextCounts > count.QuadPart)
				{
					DWORD t = (DWORD)((NextCounts - count.QuadPart) * 1000 / CountsPerSecond);
					Sleep(t);
					continue;
				}
#endif
				_Timer.tick();
				Calculate_frame_stats();

				//過去のインスタンス取得
				_my_previnstance = _my_instance;

				//メインループが切り替わる、または終わったらループを抜ける
				if (_my_instance != _my_previnstance || !MainLoop())
				{
					msg.message = WM_QUIT;
				}

				//次の活動予定時間
				NextCounts += CountsPerFrame;
			}
		} while (msg.message != WM_QUIT);
	}


	void WinMainClass::Calculate_frame_stats()
	{
		// Code computes the average frames per second, and also the
		// average time it takes to render one frame.  These stats
		// are appended to the window caption bar.
		static int frames = { 0 };
		static float time_tlapsed = { 0.0f };

		frames++;

		// Compute averages over one second period.
		if ((_Timer.time_stamp() - time_tlapsed) >= 1.0f)
		{
			float fps = static_cast<float>(frames); // fps = frameCnt / 1
			float mspf = 1000.0f / fps;

			std::ostringstream outs;
			outs.precision(6);
			outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";

			HWND hwnd = pWindowCreateManager->GetWindowWithClassName(WINMAIN_CLASS_NAME)->GetHWnd();//メインウインドウのウインドウハンドル
			SetWindowTextA(hwnd, outs.str().c_str());

			// Reset for next average.
			frames = 0;
			time_tlapsed += 1.0f;
		}
	}




	//+ ここからウインドウの定義------------------------------------------------

	/**
	* @namespace MainWindow
	* @brief メインウインドウ
	* @details 詳細な説明
	*/
	namespace MainWindow {


		LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
		{
			switch (msg)
			{
				case WM_DESTROY:
					WinMainClass::_my_instance = nullptr;//完全終了
					PostQuitMessage(0);
					return 0;
			}
			return DefWindowProc(hwnd, msg, wp, lp);
		}

		// ウインドウ定義
		BOOL InitWindow(HWND& hWnd, char* className, int left, int top, size_t width, size_t height)
		{
			WNDCLASS winc;

			winc.style = CS_HREDRAW | CS_VREDRAW;
			winc.lpfnWndProc = WndProc;
			winc.cbClsExtra = winc.cbWndExtra = 0;
			winc.hInstance = WinMainClass::_hInstance;
			winc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
			winc.hCursor = LoadCursor(NULL, IDC_ARROW);
			winc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
			winc.lpszMenuName = NULL;
			winc.lpszClassName = className;

			if (!RegisterClass(&winc)) return 0;

			hWnd = CreateWindow(
				className, TEXT("☆main "),
				WS_OVERLAPPEDWINDOW,
				left, top, width, height, NULL, NULL,
				WinMainClass::_hInstance, NULL
			);

			if (hWnd == NULL) return 0;

			ShowWindow(hWnd, SW_SHOW);

			return (TRUE);
		}
#undef WINMAIN_CLASS_NAME
	}





}
//+	WinMain---------------------------------------------------------
//	windowsアプリが始まると最初にここへ入ります。
int WINAPI WinMain(HINSTANCE hInstance_, HINSTANCE prev_instance, PSTR cmd_lin, int cmd_show)
{
	Lib_Base::WinMainClass::_hInstance = hInstance_;
	Lib_Base::WinMainClass::_prev_instance_ = prev_instance;
	Lib_Base::WinMainClass::_cmd_lin_ = cmd_lin;
	Lib_Base::WinMainClass::_nShowCmd = cmd_show;
	return Lib_Base::WinMainClass::_my_instance->Boot();
}
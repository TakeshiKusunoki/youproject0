#pragma once
/**
* @file Flag.h
* @brief フラグ管理クラスが記述されている
*/
#include "../Lib_Vassel/BaseClass.h"

using Lib_Vassel::BaseTask;
using Lib_Vassel::BaseDecide;



/**
* @brief フラグ管理クラス
* @details boolの変数をクラスに定義したいときにboolがたの代わりとして使う
*/
class FlagM
{
private:
	bool flag;
public:
	FlagM()
		:flag(false)
	{
	}
	//! @brief コピーコンストラクタ
	explicit FlagM(const FlagM&) {}
	~FlagM() {}
	//! @brief コピーコンストラクタ
	void operator=(const FlagM&) {}
public:
	//! @brief コピーコンストラクタ
	void operator=(bool f)
	{
		flag = f;
	}
	//! @brief フラグを反転する
	void Reverce()
	{
		flag ^= flag;
	}
	//!  @brief フラグを立てる
	void OnFlag()
	{
		flag = true;
	};
	//! @brief デフラグ
	void DeFlag()
	{
		flag = false;
	};
	//! @brief flagの値とは逆を返す(ゲッター)
	bool GetReverce()
	{
		bool f = flag;
		f ^= flag;
		return f;
	}
	//! @brief (ゲッター)
	//! @return flagを返す
	bool get()
	{
		return flag;
	}

};


/**
* @brief 呼ばれている時だけ、フラグを立てるクラス
* @detailes BaseDecide* baseCl: MultiTaskが呼ばれる条件(戻り値がtrueなら呼ぶ)
*/
class FlagCallOn : public BaseTask
{
public:

	/**
	* @fn void sum()
	* @brief
	/* @detailes bool(*func)() : MultiTaskが呼ばれる条件(戻り値がtrueなら呼ぶ)
	*/
	FlagCallOn(BaseDecide* baseCl)
	{
		Call.DeFlag();
		TermsCl = baseCl;
	}
	~FlagCallOn() {}
	//! @brief 条件を満たすならフラグを立てる
	void MultiTask()override
	{
		if (TermsCl->Decide())
		{
			Call.OnFlag();
		}
		else
		{
			Call.DeFlag();
		}

	}
	//! @brief フラグの取得
	bool GetFlag()
	{
		return Call.get();
	}
private:
	//! MultiTaskが呼ばれる条件(戻り値がtrueなら呼ぶ)
	BaseDecide* TermsCl;
	//! MultiTaskが呼ばれたかのフラグ
	FlagM Call;
};


//! @brief 呼ばれた時、１回だけ真
class FlagForOneTimeCall
{
private:
	//! このクラスの判断フラグ
	bool decideFlag;
	//! 返すフラグ
	bool mainFlag;
public:
	FlagForOneTimeCall()
		:mainFlag(true)
		, decideFlag(false)
	{
	}
	~FlagForOneTimeCall() {}
	//! @brief 呼ばれた時、１回だけ真
	bool Decide()
	{
		if (decideFlag)
		{
			mainFlag = false;
		}
		decideFlag = true;
		return mainFlag;
	}
};

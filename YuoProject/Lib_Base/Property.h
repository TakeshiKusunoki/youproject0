#pragma once

//! @def PROPERTY_WRITEABLE
//! @brief プロパティマクロ getter setter
#define _PROPERTY_WRITEABLE( Type, Name)\
private: Type _##Name;	\
public:	\
Type Get##Name()const noexcept{return _##Name; }	\
void Set##Name(Type value)noexcept{_##Name = value;};	\
_declspec(property(get = Get##Name, put = Set##Name)) Type Name;


//! @def PROPERTY_READABLE
//! @brief 読み取り専用変数定義  getter
#define _PROPERTY_READABLE( Type, Name)\
private: Type _##Name;	\
public:					\
constexpr Type Get##Name()const noexcept{return _##Name; }

//! @def PROPERTY_READABLE_REFERENCE
//! @brief 読み取り専用変数定義
//! @return getter const &
#define _PROPERTY_READABLE_REFERENCE( Type, Name)\
private: Type _##Name;	\
public:					\
const Type& Get##Name()const noexcept{return _##Name; }




//Protected------------------------------------------


//! @def PROPERTY_WRITEABLE_PROTECTED
//! @brief プロパティマクロ getter setter
#define _PROPERTY_WRITEABLE_PROTECTED( Type, Name)\
protected: Type _Protected##Name;	\
public:	\
Type GetProtected##Name()const noexcept{return _Protected##Name; }	\
void SetProtected##Name(Type value)noexcept{_Protected##Name = value;};	\
_declspec(property(get = GetProtected##Name, put = SetProtected##Name)) Type ProtectedName;


//! @def PROPERTY_READABLE_PROTECTED
//! @brief 読み取り専用変数定義  getter
#define _PROPERTY_READABLE_PROTECTED( Type, Name)\
protected: Type _Protected##Name;	\
public:					\
constexpr Type GetProtected##Name()const noexcept{return _Protected##Name; }

//! @def PROPERTY_READABLE_PROTECTED
//! @brief 読み取り専用変数定義
//! @return getter const &
#define _PROPERTY_READABLE_REFERENCE_PROTECTED( Type, Name)\
protected: Type _Protected##Name;	\
public:					\
const Type& GetProtected##Name()const noexcept{return _Protected##Name; }

//x このクラスは継承しない
//x #define _CONCEPT_NOT_POLIMORFIZM(Adr, Constrctor) Adr: virtual ~Constrctor() = delete


//// 自分が右辺値だった場合、代入不可にする
//template<typename T>
//struct UnSubstitution
//{
//	UnSubstitution<T>& operator=(T i) & { v = i; return *this; }
//	UnSubstitution<T>& operator=(T i) && = delete; // 自分が右辺値だった場合、代入不可
//private:
//	T v;
//};

//! @def REP
//! forループマクロ
#define REP(i,n) for(int i=0, i##_len=(n); i<i##_len; ++i)
//! @def RREP
//! forループマイナス
#define RREP(i,x) for(int i=(static_cast<int>(x)-1);i>=0;i--)
//! @def RREPS
//! forループマイナス
#define RREPS(i,x) for(int i=static_cast<int>(x);i>0;i--)

//! @def DEFAULT_COPY_MOVE_CONSTRACTOR
//! デフォルト　（コピー/ムーブ）コンストラクタ　
//! クラス名
//! アクセス指定子名
#define DEFAULT_COPY_MOVE_CONSTRACTOR(ClassName)\
ClassName(const ClassName&) = default;\
void operator=(const ClassName&){}\
ClassName(const ClassName&&) = default;\
void operator=(const ClassName&&){}

//
////! プロパティ基底クラス
//template <typename T>
//class Property
//{
//public:
//	Property(T& value) : _value(value) {}
//	Property(const Property& ref) : _value(ref._value) {}
//	virtual ~Property() {}
//
//	Property<T>& operator = (const Property<T>& ref)
//	{
//		this->_value = T(ref._value);
//		return *this;
//	}
//
//Protected:
//	T&      _value;
//};
//
////! デフォルトのgetterポリシー
//template <typename T>
//struct SimplePropertyGetter
//{
//	static const T& Get(const T& value) { return value; }
//};
//
////! デフォルトのsetterポリシー
//template <typename T>
//struct SimplePropertySetter
//{
//	static void Set(T& value, const T& var) { value = T(var); }
//};
//
////! getter/setter有りなProperty
//template <typename T, class Getter = SimplePropertyGetter<T>, class Setter = SimplePropertySetter<T>>
//class WritableProperty : public Property<T>, private Getter, private Setter
//{
//public:
//	WritableProperty(T& value) : Property<T>(value) {}
//	WritableProperty(const WritableProperty& ref) : Property<T>(ref) {}
//	virtual ~WritableProperty() {}
//
//public:
//	operator const T& () const { return this->Get(this->_value); }
//	const T& operator -> () const { return this->Get(this->_value); }
//
//	WritableProperty<T, Getter, Setter>& operator = (const T& var) { this->Set(this->_value, var); return *this; }
//};
//
////! getterのみのProperty
//template <typename T, class Getter = SimplePropertyGetter<T>>
//class ReadOnlyProperty : public Property<T>, private Getter
//{
//public:
//	ReadOnlyProperty(T& value) : Property<T>(value) {}
//	ReadOnlyProperty(const ReadOnlyProperty& ref) : Property<T>(ref) {}
//	virtual ~ReadOnlyProperty() {}
//
//public:
//	operator const T& () const { return this->Get(this->_value); }
//	const T& operator -> () const { return this->Get(this->_value); }
//};
#ifndef INCLUDED_MY_UTIL
#define INCLUDED_MY_UTIL

//******************************************************************************
//
//
//      ユーティリティー
//
//
//******************************************************************************

#include <sstream>
#include <bitset>
#include <assert.h>
#include <algorithm>
#include <DirectXMath.h>
#define NOMINMAX
//------< inline function >-----------------------------------------------------

//------------------------------------------------------
//  整数を2進数（16bit）のstringに変換する
//------------------------------------------------------
//  const int n     変換する整数
//------------------------------------------------------
//  戻り値：std::string     数値を2進数に変換したもの(string)
//------------------------------------------------------
inline std::string strBit16(const int n)
{
	std::stringstream ss;
	ss << static_cast<std::bitset<16>>(n);
	return ss.str();
}

//------------------------------------------------------
//  値を範囲内に収める
//------------------------------------------------------
//  const T& v  範囲内に収めたい値
//  const T& lo 下限値
//  const T& hi 上限値（lo以上の値でなければならない）
//------------------------------------------------------
//  戻り値：const T&    vをloからhiまでの範囲に収めた数値
//------------------------------------------------------
template <typename T> constexpr const T& clamp(const T& v, const T& lo, const T& hi)
{
	assert(hi >= lo);
	return (std::max)((std::min)(v, hi), lo);
}

//------------------------------------------------------
//  値をラップアラウンド（範囲を繰り返す）させる
//------------------------------------------------------
//  const int v  範囲を繰り返させたい値
//  const int lo 下限値
//  const int hi 上限値（loより大きい値でなければならない）
//------------------------------------------------------
//  戻り値：int    vをloからhiまでの範囲でラップアラウンドさせた数値
//------------------------------------------------------
inline int wrap(const int v, const int lo, const int hi)
{
	assert(hi > lo);
	const int n = (v - lo) % (hi - lo); // 負数の剰余はc++11から使用可になった
	return n >= 0 ? (n + lo) : (n + hi);
}

// float版
inline float wrap(const float v, const float lo, const float hi)
{
	assert(hi > lo);
	const float n = std::fmod(v - lo, hi - lo);
	return n >= 0 ? (n + lo) : (n + hi);
}





// 2Dベクトルの外積
inline float Vec2Cross(DirectX::XMFLOAT2* v1, DirectX::XMFLOAT2* v2)
{
	return v1->x * v2->y - v1->y * v2->x;
}
struct Segment
{
	DirectX::XMFLOAT2 start;//! 始点
	DirectX::XMFLOAT2 end;//! 方向ベクトル（線分の長さも担うので正規化しないように！）
};

// 線分の衝突
inline bool ColSegments(
	Segment &seg1,          // 線分1
	Segment &seg2          // 線分2
)
{
	float x = seg2.start.x - seg1.start.x;
	float y = seg2.start.y - seg1.start.y;
	DirectX::XMFLOAT2 v = DirectX::XMFLOAT2(x, y);
	float Crs_v1_v2 = Vec2Cross(&seg1.end, &seg2.end);
	if (Crs_v1_v2 == 0.0f)
	{
		// 平行状態
		return false;
	}

	float Crs_v_v1 = Vec2Cross(&v, &seg1.end);
	float Crs_v_v2 = Vec2Cross(&v, &seg2.end);

	float t1 = Crs_v_v2 / Crs_v1_v2;
	float t2 = Crs_v_v1 / Crs_v1_v2;

	const float eps = 0.00001f;
	if (t1 + eps < 0 || t1 - eps > 1 || t2 + eps < 0 || t2 - eps > 1)
	{
		// 交差していない
		return false;
	}
	return true;
}

inline bool ColSegments_(
	Segment &seg1,          // 線分1
	Segment &seg2,          // 線分2
	float* outT1 = nullptr,       // 線分1の内分比（出力）
	float* outT2 = nullptr,       // 線分2の内分比（出力
	DirectX::XMFLOAT2* outPos = nullptr // 交点（出力）
)
{
	float x = seg2.start.x - seg1.start.x;
	float y = seg2.start.y - seg1.start.y;
	DirectX::XMFLOAT2 v = DirectX::XMFLOAT2(x, y);
	float Crs_v1_v2 = Vec2Cross(&seg1.end, &seg2.end);
	if (Crs_v1_v2 == 0.0f)
	{
		// 平行状態
		return false;
	}

	float Crs_v_v1 = Vec2Cross(&v, &seg1.end);
	float Crs_v_v2 = Vec2Cross(&v, &seg2.end);

	float t1 = Crs_v_v2 / Crs_v1_v2;
	float t2 = Crs_v_v1 / Crs_v1_v2;

	if (outT1)
		*outT1 = Crs_v_v2 / Crs_v1_v2;
	if (outT2)
		*outT2 = Crs_v_v1 / Crs_v1_v2;

	const float eps = 0.00001f;
	if (t1 + eps < 0 || t1 - eps > 1 || t2 + eps < 0 || t2 - eps > 1)
	{
		// 交差していない
		return false;
	}

	float x_ = seg1.end.x * t1;
	float y_ = seg1.end.y * t1;
	DirectX::XMFLOAT2 v_ = DirectX::XMFLOAT2(x_, y_);
	x_ = seg1.start.x + v_.x;
	y_ = seg1.start.y + v_.y;
	v_ = DirectX::XMFLOAT2(x_, y_);
	if (outPos)
		*outPos = v_;

	return true;
}

//! 累乗
template < typename T,
	typename std::enable_if<std::is_integral<T>::value, std::nullptr_t>::type = nullptr >
[[nodiscard]] inline XM_CONSTEXPR T pow_(T base, int exp) noexcept
{
	return (exp == 0 ? 1 : base * pow_(base, exp - 1));
}



template<typename ...types>
class Swallow
{
	types... xdd;
public:
	Swallow();
	~Swallow();
	void operator[](types... var)
	{
		xdd = var;
	}
private:

};

//#define LOOP(x, condition, operator_) 
//template <typename ...types, typename T, \
//	typename std::enable_if<std::is_integral<T>::value, std::nullptr_t>::type = nullptr >\
//	[[nodiscard]] inline XM_CONSTEXPR auto powArray_(int base, int exp) noexcept\
//{\
//int i; \
//for (i = x; condition; operator_)\
//{\
//\
//}\
//Swallow<types...> n; \
//for (i = x; x < y; operator_)\
//{\
//Swallow<i> n; \
//n[i] = pow_(base, exp + i); \
//}\
//return n;
//}


//! length用のみ
template < typename T,
	typename std::enable_if<std::is_integral<T>::value, std::nullptr_t>::type = nullptr >
[[nodiscard]] inline XM_CONSTEXPR T sqrt_(T s)noexcept
{
	T x = s / 2.0;
	T prev = 0.0;
	while (x != prev)
	{
		prev = x;
		x = (x + s / x) / 2.0;
	}
	return x;
}


//! @brief ルート（マイナスでも値が出る）
inline double sqrtMy(double x)
{
#define ACCURACY 14//精度
	if (x == 0)
		return 0;
	else
	{
		double y, z, result;
		y = 1;

		for (int i = 0; i <= ACCURACY; i++)
		{
			z = x / y;
			result = (y + z) / 2;
			y = result;
		}
		return result;
	}
#undef ACCURACY
}





//! @brief 2次元ベクトルから角度を求める(角度を度数法で返す)
inline float atan2My(float _y, float _x)
{
	float x = fabs(_x);
	float y = fabs(_y);
	float   z;
	bool    c;

	c = y > x;
	if (c)
	{
		z = (float)x / y;
	}
	else
	{
		z = (float)y / x;
	}
	float a;
	//a = z * (-1556 * z + 6072);                     //2次曲線近似
	//a = z * (z * (-448 * z - 954) + 5894);          //3次曲線近似
	a = z * (z * (z * (829 * z - 2011) - 58) + 5741); //4次曲線近似

	if (c)
	{
		if (_y > 0)
		{
			if (_x < 0)
			{
				a = 36000 - a;
			}
			if (_x > 0)
			{
			}
		}
		if (_y < 0)
		{
			if (_x > 0)
			{
				a = 18000 - a;
			}
			if (_x < 0)
			{
				a = a + 18000;
			}
		}
	}

	if (!c)
	{
		if (_y > 0)
		{
			if (_x > 0)
			{
				a = 9000 - a;
			}
			if (_x < 0)
			{
				a = a + 27000;
			}
		}
		if (_y < 0)
		{
			if (_x > 0)
			{
				a = a + 9000;
			}
			if (_x < 0)
			{
				a = a + 27000;
			}
		}
	}

	return (a / 100);
}


#endif // !INCLUDED_MY_UTIL

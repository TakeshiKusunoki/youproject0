#include <assert.h>
#include "WindowCleate.h"
#define WINDOWS_CLASS_NAME TEXT("Win.Sample.WindowClass")//ウインドウクラスにつける名前
namespace Lib_Base {




	HWND WindowCleate::GetHWnd()
	{
		return _hWnd;
	}

	size_t WindowCleate::GetWindowWidth()
	{
		return _windowWidth;
	}

	size_t WindowCleate::GetWindowHeight()
	{
		return _windowHeight;
	}

	const char * WindowCleate::GetClassName_()
	{
		return _className;
	}

	POINT WindowCleate::GetCursorPos_()
	{
		return _cursorPos;
	}

	void WindowCleate::InitWindow()
	{
		BOOL err = _InitWindowFunc(_hWnd, _className, _left, _top, _windowWidth, _windowHeight);

		if (!err)
		{
			assert(!"表示するウインドウの初期化ができません。");
			return;
		}
	}

	void WindowCleate::UnInit()
	{
		UnregisterClass(_className, GetModuleHandle(NULL));
	}

	void WindowCleate::UpdateCursorPos()
	{
		GetCursorPos(&_cursorPos);
		ScreenToClient(_hWnd, &_cursorPos);
	}













	//+ 管理クラス------------------------------------------------------------
	void WindowCreateManager::AddWindow(WIN_INIT_TYPE initWindow, char* className, int left, int top, size_t width, size_t height)
	{
		WindowCleate newwin;
		newwin._InitWindowFunc = initWindow;
		newwin._className = className;
		newwin._left = left;
		newwin._top = top;
		newwin._windowHeight = height;
		newwin._windowWidth = width;
		//addしたウインドウを生成する
		newwin.InitWindow();
		_windowList.push_back(newwin);
	}

	void WindowCreateManager::DestoroyAllWindow()
	{
		for (auto& it : _windowList)
		{
			HWND hWnd = it.GetHWnd();
			DestroyWindow(hWnd);
			it.UnInit();
		}
		_windowList.clear();
	}

	void WindowCreateManager::DestoroyAllSubWindow(const char * className)
	{
		auto iterater = _windowList.begin();// イテレーター
		for (size_t i = 0; iterater != _windowList.end(); i++)
		{
			//クラスネームがあっているか？
			if (strcmp(iterater->GetClassName_(), className))
				iterater++;
			continue;
			//クラス名合致していないものは破棄
			HWND hWnd = iterater->GetHWnd();
			DestroyWindow(hWnd);
			iterater->UnInit();//定義を消す
			iterater = _windowList.erase(iterater);//空のノード削除
			break;
		}
	}

	void WindowCreateManager::DestoroyWindowWithClassName(const char* className)
	{
		auto iterater = _windowList.begin();// イテレーター
		for (size_t i = 0; iterater != _windowList.end(); i++)
		{
			//クラスネームがあっているか？
			if (!strcmp(iterater->GetClassName_(), className))
				iterater++;
				continue;
				//クラス名合致するものがあれば破棄
				HWND hWnd = iterater->GetHWnd();
				DestroyWindow(hWnd);
				iterater->UnInit();//定義を消す
				iterater = _windowList.erase(iterater);//空のノード削除
				break;
		}
	}

	void WindowCreateManager::ProcessExectuteEachWindow(WindowEatherProcess * process)
	{
		for (auto& it : _windowList)
		{
			process->WinEatherProcess(&it);
		}
	}

	size_t WindowCreateManager::GetListSize()
	{
		return _windowList.size();
	}

	WindowCleate* WindowCreateManager::GetWindowWithClassName(const char * className)
	{
		for (auto& it: _windowList)
		{
			if (strcmp(it.GetClassName_(), className))
			continue;
			return &it;
		}
		return nullptr;
	}

}

#pragma once
#include "..//Lib_Vassel//BaseObject.h"
#include "..//Lib_Collition/Octree.h"
#include <vector>
#include <variant>
#include "..//Lib_Collition/PrimitivClass.h"
#include "..//Lib_Collition/BaseCollisionObject.h"


#define PRIMITIV_ARRAY(Type) Lib_Collition::PrimitiveVariantIndex::index_of_t<Lib_Collition::Type>::value

//! �Փ˔�����
using PrimitiveCategoly = std::variant<
	std::monostate,
	Lib_Collition::PrimitiveVariantC<PRIMITIV_ARRAY(Plane)>,
	Lib_Collition::PrimitiveVariantC<PRIMITIV_ARRAY(Point)>,
	Lib_Collition::PrimitiveVariantC<PRIMITIV_ARRAY(Line)>,
	Lib_Collition::PrimitiveVariantC<PRIMITIV_ARRAY(Segment)>,
	Lib_Collition::PrimitiveVariantC<PRIMITIV_ARRAY(Triangle)>,
	Lib_Collition::PrimitiveVariantC<PRIMITIV_ARRAY(Square)>,
	Lib_Collition::PrimitiveVariantC<PRIMITIV_ARRAY(AABB)>
>;

//��
void example()
{
	PrimitiveCategoly x;
	x;
}

struct Visitor
{
};

struct ObjectBind
{
	Lib_Vassel::BaseObject* obj;



};


template<int I>
using Componet = Lib_Base::VariantC<I,
	std::monostate,
	Lib_Collition::OctTreeManager,
	Lib_Collition::BaseCollisionObject<Lib_Collition::Plane>,
	Lib_Collition::BaseCollisionObject<Lib_Collition::Point>,
	Lib_Collition::BaseCollisionObject<Lib_Collition::Line>,
	Lib_Collition::BaseCollisionObject<Lib_Collition::Segment>,
	Lib_Collition::BaseCollisionObject<Lib_Collition::Triangle>,
	Lib_Collition::BaseCollisionObject<Lib_Collition::Square>,
	Lib_Collition::BaseCollisionObject<Lib_Collition::AABB>,
	Lib_Collition::LeterForBaseObject
>;

template<int I>
using ComponetT = Lib_Base::VariantTC<I,
	std::monostate,
	Lib_Collition::Plane,
	Lib_Collition::Point,
	Lib_Collition::AABB
>;

using Componet0 = Lib_Base::VariantC<1,
	Lib_Collition::Plane,
	Lib_Collition::Point
>;

template<typename int... I>
class ComCl
{
public:
	std::array<Componet<I>, sizeof...(I)> component;
public:
	constexpr ComCl()
		:component(Componet<I>{})
	{
		constexpr int x = PRIMITIV_ARRAY(Plane);

		component[0] = Componet<3>{};
		Componet<3>::GetThis();
		Componet<3> c;
		c.GetVariant();
	}
	~ComCl() = default;
private:
};

template<typename int... I>
class ComCl_
{
public:
	std::array<ComponetT<I>, sizeof...(I)> component;
public:
	constexpr ComCl_()
		:component(ComponetT<I>{})
	{
		component[0] = ComponetT<3>{};
		ComponetT<3>::GetThis();
		ComponetT<3> c;
		c.GetVariant();
	}
	~ComCl() = default;
private:
};

void kk()
{
	ComponetT<2> lp{1};
	ComponetT<1>::at_t;
	lp.GetThis();
	lp.GetVariant();
}

constexpr ComCl<3,1> com;
constexpr void WorkSpace()
{
	constexpr Componet0 ki{};
	Componet0::at_t;
	ki.GetThis();
	com.component[0].GetThis();
	constexpr auto c = ComCl<3>{};
	com.component[0].GetThis();
	constexpr auto x = com.component[0].size();
	constexpr Componet<2> b{};
	b.GetVariant();
}

/*void x()
{
	component[0] = Lib_Collition::OctTree();
	int x_ = component[0].index();
	auto x___ = std::get<Lib_Collition::OctTree>(component[0]);
	auto y = std::visit(Visitor{}, component[0]);
	auto z = std::visit([](const auto& x){ return x; }, component[0]);

	int n = 0;
	switch (n)
	{
	default:
		break;
	case PRIMITIV_ARRAY(Plane):
		auto x = std::get<Lib_Collition::Plane>();
		break;
	}*/



#pragma once
// @file Cmand.h
// @brief Time operationクラスが記述されている

#include "BaseLoop.h"
#include <list>
#include <vector>

namespace Lib_Vassel {

	//! @brief コマンドクラス
	//! @details コマンドが打たれるまでこのループを飛ばす<br>
	//! コマンドが打たれると戻り値を転生関数の引数に渡す
	class Cmand
	{
	private:
		char* _cmand;
	public:
		Cmand() :_cmand("") {};
		~Cmand() {};

		//! CmandInterfaceからコマンドをゲットして実行させる
		//! コマンドは呼ばれたら消去
		void CallCmand();
		//! コマンドをCmandInterfaceへセット
		void SetCmand(char* cmand);
	private:

	};


	//! @brief コマンドメッセージを受け取る(シングルトン)(callNumを増やさないためこのクラスを使う)
	//! 仕様変更があるかもしれないのでクラスを分けた
	//! なぜこのような仕様なのかはcallNumが増えてから、ループファンクを呼ぶかのDecide()の後、コマンドを取得し、そのあとcallNumが増えてからの再起関数上でコマンドを呼ぶため
	class CmandInterface
	{
		//! 次に呼ばれるループファンク		(次よぶかDecide()するループファンク)
#define NextRoopFunc(x) x[x.size()-1]
		//! //さっき呼び終わったループファンク	(さっきよぶかDecide()したループファンク)
#define CurrentRoopFunc(x) x[x.size()-2]
		//! l//最初に呼ばれたループファンク
#define FirstRoopFunc(x) x.at(0)
	private:
		//std::list<void(CmandInterface::*)()> cmandList;
		int _saveNodeNum;//位置記憶するノード番号
						 //std::vector<BaseLoop*>* _loopFuncList;//RecursionからloopFuncListをもらう
	public:
		/*CmandInterface(std::vector<BaseLoop*>* loopFuncList)
		:_loopFuncList(loopFuncList)
		{};
		~CmandInterface(){}*/
	public:
		// リストに対して実行---------------------------
		//! @brief 処理を逆戻りする
		//! @return 次に再起関数で呼ぶループファンクになるもの
		BaseLoop* Reverce(const std::vector<BaseLoop*>& loopFuncList)
		{
			BaseLoop* func = nullptr;
			for (int i = loopFuncList.size() - 1; i >= 0; i--)
			{
				func = loopFuncList[i]->LoopFunc(loopFuncList[i]);
			}
			return func;
		}

		//! @brief 1つ処理を戻る
		BaseLoop* Buck(const std::vector<BaseLoop*>&  loopFuncList)
		{
			if (loopFuncList.size() < 2)
				return nullptr;//アサート
			return CurrentRoopFunc(loopFuncList)->LoopFunc(CurrentRoopFunc(loopFuncList));
		}

		//! @brief 初めに戻る
		BaseLoop* Rstart(const std::vector<BaseLoop*>&  loopFuncList)
		{
			Recursion(FirstRoopFunc(loopFuncList));
			return NextRoopFunc(loopFuncList);
		}

		//! @brief ループを終える(再起ループから抜ける)
		//! 引数に意味はない
		BaseLoop* End(const std::vector<BaseLoop*>&  loopFuncList)
		{
			return nullptr; loopFuncList;
		}

		//! @brief このループをもう一回する
		BaseLoop* again(const std::vector<BaseLoop*>&  loopFuncList)
		{
			return CurrentRoopFunc(loopFuncList)->LoopFunc(CurrentRoopFunc(loopFuncList));
		}

		// セーブ関係-----------------------
		//! @brief save ループファンクの位置を記憶する
		void Save(const std::vector<BaseLoop*>&  loopFuncList)
		{
			if (loopFuncList.size() < 2)
				return;//アサート
			_saveNodeNum = loopFuncList.size() - 2;
		}

		//! @brief セーブしたクラスの処理をする
		void SaveFuncExecute(const std::vector<BaseLoop*>&  loopFuncList)
		{

		}

	private:
#undef NextRoopFunc
	};

}


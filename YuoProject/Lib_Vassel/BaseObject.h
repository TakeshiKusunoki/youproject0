#pragma once
#include <list>
#include "Union.h"
#include "BaseClass.h"
#include "MsgLocation.h"
#include "LocationManager.h"
#include "BaseLoop.h"
#include "../Lib_Base/Template.h"
#include "../Lib_Render/BaseRender.h"
#include "../Lib_Base/Counter.h"
#include "../Lib_3D/vector.h"
#include "../Lib_Collition/PrimitivClass.h"
#include "../Lib_Collition/CollitonDitection.h"
#include <cstdio>


namespace Lib_Vassel {
	class LocationManager;
	//! @brief 種々オブジェクト間で共通する部分(器)
	//! @par 詳細
	//! _my_instanceのポインタが示すオブジェクトデータを管理する
	//! my_instanceを変えることで別のインスタンスに変化する
	//! このクラスの情報はMsgLocationクラスが参照できる
	//! _my_instance, _my_previnstance,_valueList,_renderer
	//! 位置やロケーションは同じで、違うインスタンスを持てるようにする
	class BaseObject : public BaseLoop, public  BaseTask
	{
		friend class Renderer;
		template <typename T>
		using use_UserValue = std::vector<std::pair<char*, T*>>;//char*　ラベル、T ゆーざーバリュー
	private:
		struct PRIVATE_OBJECT_DATA
		{
			// _my_instanceで参照しない
			BaseObject* _my_instance;//! このクラスが管理するインスタンス（このクラスの実体ではない）(_flagMyInstanceModeで_my_instanceかこのクラスの実体かどちらを実行するか切り替えられる)
			BaseObject* _my_previnstance;//! 過去のインスタンス
			// _my_instanceで参照
			BaseLoop* _pNextLoopFunc;//! 次に実行されるループファンク(thisでagain、 nullptrでループ抜け)
			LoopFuncExecter _executer;//! このループからの派生処理
			Counter _counter;//! 待機カウンター
			Counter _counterLoop;//! ループカウンター
			bool _endFlag;//! 終了フラグ
			bool _flagMyInstanceMode;//! trueで_my_instanceで動く
		};
		struct PUBLIC_OBJECT_DATA
		{
			std::vector<DirectX::XMFLOAT3*> _vertexList;//! 頂点リスト//
			std::pair<char*, DirectX::XMFLOAT3*> _position;//位置
			bool _judgeFlag;//! 当たり判定するか
			char* _cmnd;//! コマンド(１回の処理の終わりごとにnullにする)(LoopFunc関数)
			char* _msg;//! メッセージ(１回の処理の終わりごとにnullにする)(msg処理関数)
			char* _label;//! オブジェクトソートラベル
			BaseObject* _pBaseObjectHucked;//! ハックしたオブジェクト(器)(１回の処理の終わりごとにnullにする)
			LocationManager * _pLom;//! 今存在するロケーション(どのロケーション設計図を参照するか)(letするローケーションマネージャー)

			template <class T>
			Lib_Collition::CollitonDitection<T> _collisoinObj;
		};
		struct PROTECTED_OBJECT_DATA//データーまとめ
		{
			// _my_instanceで参照// (重要メソッド)
			Lib_Render::BaseRender* _pRenderer;//! 描画するオブジェクト(個々でnewしてポインタ参照させる)
			int _actFlame;//! 進行フレーム
			int _actFlamePrev;//! 進行フレームの前位置
		};
	private:
		PRIVATE_OBJECT_DATA _private;//! BaseObjectのメソッド(このクラスのみの共通処理で使う)
	protected:
		PROTECTED_OBJECT_DATA _protected;//! BaseObjectのメソッド(継承クラスでも共通して使う)
	public:
		PUBLIC_OBJECT_DATA _public;//! BaseObjectのメソッド(このクラス外でも共通して使う)
	public:
		BaseObject()
		{
			Init();
		}
		BaseObject(const BaseObject&){}
		virtual ~BaseObject(){}

	public:
		//! @brief 初期化
		virtual void Initialize()
		{
			Init();
		}
		//! @brief 解放処理
		virtual void UnInitialize() {};

		//! @brief オブジェクトの情報をメッセージロケーションに送る
		virtual void LetData()
		{
			LetData_();
		}

		//! @brief メッセージを見せる
		void ShowMsg();

		//! @brief ループファンク
		//! @details この関数がexeされないと_nextLoopFuncは呼ばれない
		BaseLoop* LoopFunc(BaseLoop* loopFunc)override;

		//! @brief オブジェクトのインスタンスをスワップ(_my_instance同士を取り換えあう)
		//! @details *器同士の処理なので_my_instanceをこの関数に引数として渡さない
		//! @param[in] instance器のアドレス
		//! 別の器に管理を委譲する
		void SwapInstance(BaseObject* instance)
		{
			ChangeInstance(instance);
			instance->ChangeInstanceGetVassel(_private._my_previnstance);
		}
		//! あるBaseObjectの器の情報をもらう(_my_instanceからは取れない)
		void HuckBaseObjectVassel(BaseObject* obj)
		{
			_public._pBaseObjectHucked = obj;//器の情報を渡す
		}
		//! あるBaseObjectの情報をもらう
		void HuckBaseObject(BaseObject* obj)
		{
			if (_private._flagMyInstanceMode)
			{
				_private._my_instance->_public._pBaseObjectHucked = obj;
				return;
			}
			_public._pBaseObjectHucked = obj;//器の情報を渡す
		}
		//! ゲッター
		bool GetEndFlag()
		{
			if (_private._flagMyInstanceMode)
				return _private._my_instance->_private._endFlag;
			return _private._endFlag;
		}
		//! @return 頂点リスト
		const std::vector<DirectX::XMFLOAT3*>& GetVertxList()
		{
			if (_private._flagMyInstanceMode)
				return (_private._my_instance->_public._vertexList);
			return _public._vertexList;
		}
		const std::vector<DirectX::XMFLOAT3*>& GetVertxListVassel()
		{
			return _public._vertexList;
		}
		//! セッター
		//! @param[in] labelNaame ラベル名
		void SetLabel(char * labelNaame)
		{
			if (_private._flagMyInstanceMode)
				_private._my_instance->_public._label = labelNaame;
			else
			_public._label = labelNaame;
		}
		//! @param[in] pLom LocationManager
		void SetLocationManager(LocationManager * pLom)
		{
			if (_private._flagMyInstanceMode)
				_private._my_instance->_public._pLom = pLom;
			else
				_public._pLom = pLom;
		}
	protected:
		//! @brief 実行処理(自由に書き換える)
		virtual void MultiTask()override
		{
			_protected._actFlame++;
		}
		//! @brief 初期化(派生クラスで使いまわせる)
		void Init();
		//! @brief オブジェクトの情報をメッセージロケーションに送る
		//! @details 器の情報を渡す
		void LetData_();
	private:

		//! @brief オブジェクトにインスタンスを変える
		void ChangeInstance(BaseObject* instance)
		{
			_private._my_previnstance = _private._my_instance;
			_private._my_instance = instance->_private._my_instance;
			//無限ループを起こさないため、_my_instanceは_my_instance->_my_instanceのインスタンス参照をしない
		/*	if (this != _private._my_instance)
				_private._my_instance->_private._flagMyInstanceMode = false;*/
		}
		//! myinstanceに引数(器のポインタ)を入れる
		void ChangeInstanceGetVassel(BaseObject* instance)
		{
			_private._my_previnstance = _private._my_instance;
			_private._my_instance = instance;
			//無限ループを起こさないため、_my_instanceは_my_instance->_my_instanceのインスタンス参照をしない
			/*if (this != _private._my_instance)
				_private._my_instance->_private._flagMyInstanceMode = false;*/
		}
		//! @brief ループファンク関数呼び出した時の初期化
		void Start()
		{
			_protected._actFlamePrev = _protected._actFlame;
		}
		//! @brief 指定フレーム数処理を待機
		bool Weit(const int n)
		{
			return _private._counter.FlagAdvanced(n);
		}
		//! @brief ループするか？
		bool DoLoopTimes(const int n)
		{
			return _private._counterLoop.FlagAdvanced(n);
		}

		//! @brief 終了
		//! @details アクトフレームが止まったか調べる
		void End()
		{
			//_protected._pBaseObjectHucked = nullptr;//自分で解放させる
			//_public._msg = nullptr;
			if (_protected._actFlamePrev != _protected._actFlame)
				return;
			//_private._endFlag = true;
		}
	};




	using use_pairIDandObject = std::pair<int, class BaseObject*>;

	//! @brief オブジェクトマネージャー
	class ObjectManager : public Singleton<ObjectManager>
	{
	protected:
		std::list<use_pairIDandObject> _list;
		static int count;
	public:
		//! @return 追加したオブジェクトのIDを返す
		int Add(BaseObject* obj, LocationManager* pLom);

		//! id検索して再生成
		/*void ReCreate(int id)
		{
			for (auto it : _list)
			{
				if (it.first != id)
					continue;
				it.second->UnInitialize();
				it.second->Initialize();
			}
		}*/
		//! ocuuringSuccetion実行
		void Execute()
		{
			int timer = 0;
			for(auto& it : _list)
			{
				//printf("BaseObject\t%d\n", ++timer);
				//it.second->ShowMsg();
				it.second->LoopFunc(nullptr);
			}
		}
		//! 情報を送る
		void Let()
		{
			for (auto& it : _list)
			{
				it.second->LetData();
			}
		}
		void InitAll()
		{
			for (auto& it : _list)
			{
				it.second->Initialize();
			}
		}
		void UnInitAll()
		{
			for (auto& it : _list)
			{
				it.second->UnInitialize();
			}
			_list.clear();
		}
	};

}


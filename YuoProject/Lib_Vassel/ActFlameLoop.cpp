#include "ActFlameLoop.h"
namespace Lib_Vassel {


	ActFlameLoop::ActFlameLoop()
	{
		this->actFlame = 0;
	}


	ActFlameLoop::~ActFlameLoop()
	{
	}

	ActFlame* ActFlameLoop::LoopFunc(int argument)
	{
		this->actFlame += argument;
		return this;
	}
}
#pragma once
/**
* @file BaseClass.h
* @brief 基底クラスが記述されている
*/
#include <typeinfo.h>
#include <list>

//! @brief vasselライブラリに入っている
namespace Lib_Vassel {
	/**
	* @brief 基底クラス(BaseTask)
	* @par 詳細
	* このクラスを継承するとvoid型の関数群として扱うことができる<br>
	* ポインタを渡すことができる
	*/
	class BaseTask
	{
	public:
		virtual void MultiTask() = 0;
		virtual ~BaseTask() = 0;
	};


	/**
	* @brief 基底クラス(BaseDecide)
	* @par 詳細
	* このクラスを継承したクラスは主にあるクラスのsubクラス(あるクラスのfriend)として扱う<br>
	* subクラスはmainクラスより先に定義する(mainクラスを前方宣言する)<br>
	* mainクラスとこのクラスを継承したsubクラスは交互に互いの実態を持つ（この時、subクラスはmainクラスのポインタ型の実態を持たせる）<br>
	* 互いのコンストラクタで互いの実態を初期化する<br>
	* main関数で処理を作る<br>
	* subクラスでその関数を呼ぶ<br>
	* @par subクラスのポインタをほかのクラスに渡す。
	*/
	class BaseDecide
	{
	public:
		virtual bool Decide() = 0;
		virtual ~BaseDecide() = 0;
	};



	/*template <typename T>
	class BaseLoop
	{
	public:
		virtual BaseLoop* LoopFunc(BaseLoop* loopFunc) = 0;
	};*/


	////! @brief 管理クラスの設計(基底クラス)
	//template<typename T>
	//class BaseManager
	//{
	//protected:
	//	std::list<T> list;
	//public:
	//	BaseManager()
	//	{
	//		list.clear();
	//	}
	//	virtual ~BaseManager()
	//	{
	//		list.clear()
	//	}
	//	//! @brief 実行
	//	virtual void Update() = 0;
	//	//! @brief 削除
	//	void Clear() { list.clear(); }
	//protected:
	//};

	////! @brief 管理クラスの設計(基底クラス,シングルトン)
	////! @details : public BaseManagerSingleton<リストにしたいオブジェクト名>
	////! @par 設計
	////! （add関数は自作する）<br>
	////! Update() override;<br>
	////! std::list<T> list;<br>
	////! void Clear() { list.clear(); }
	//template<typename T>
	//class BaseManagerSingleton : public Singleton<BaseManagerSingleton<T>>
	//{
	//protected:
	//	std::list<T> list;
	//public:
	//	//! @brief 実行
	//	virtual void Update() = 0;
	//	//! @brief 削除
	//	void Clear() { list.clear(); }
	//protected:

	//};
}





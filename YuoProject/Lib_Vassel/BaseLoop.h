#pragma once
#include "../Lib_Base/Flag.h"

#include <vector>
#include <list>

namespace Lib_Vassel {

	//! @brief 1fループ
	//class ActFrame : public BaseLoop
	//{
	//public:
	//	BaseLoop* LoopFunc(BaseLoop* loopFunc_) override;
	//	//! @brief 転生関数
	//	void ActFrame_(FlagForOneTimeCall flag, int actFlame = 0);
	//	//! @brief セッター
	//	void SetNextLoopFunc(BaseLoop* nextLoopFunc)
	//	{
	//		//_nextLoopFunc = nextLoopFunc;
	//	}
	//};

	//! 基底クラス
	template <typename T>
	class BaseLoop
	{
	public:
		virtual BaseLoop* LoopFunc(T argument) = 0;
	};

	class ActFlameIndirectReference;
	class  ActFlameAsynchronous;

	//! actFlameが共通するクラス
	class ActFlame : public BaseLoop<const int>
	{
		friend class ActFlameIndirectReference;
		friend class ActFlameAsynchronous;
	protected:
		int actFlame;
	public:
		virtual ActFlame* LoopFunc(const int argument)override = 0;
		int GetActFlame()
		{
			return actFlame;
		}
	private:

	};

	//! 1フレームcallループ
	//! @brief 転生関数を呼ぶ<br>
	//!	再起関数によるループの中で呼ばれる
	//! @details LoopFunc(BaseLoop *)通常関数で呼ばれる<br>
	//!	返り値で指定された次のループ関数を呼び出す。<br>
	//! この中で関数内で、クラスのメソッドを何度も呼び出すことができる
	//! @param[in] loopFunc 次に呼ぶループ処理
	//! @return BaseLoop* 次に呼ぶループ関数を指定する。（返り値が引数になる。）<br>nullptrで終了
	class LoopFlame : public BaseLoop<LoopFlame*>
	{
	public:
		virtual LoopFlame* LoopFunc(LoopFlame* argument)override = 0;
	private:

	};


	//! @brief 再起関数
	//! 1つの実体につき１処理
	class Recursion
	{
		friend class CmandInterface;
	private:
		int _callNum;//! 読んだ回数
		std::vector<LoopFlame*> _loopFuncList;//実態を持たせない
		LoopFlame* _startloopFunc;//初めのループファンク
	public:
		Recursion(LoopFlame* loopFunc_)
			: _callNum(-1)
			, _startloopFunc(loopFunc_)
		{
			_loopFuncList.clear();
			OcurringSucssecion(loopFunc_);
		}
		~Recursion() {}
	private:
		//! @brief 連続した関数を呼ぶ
		//! @details BaseLoopクラスが戻り値により連なる
		void OcurringSucssecion(LoopFlame* loopFunc_);

	};
	//! @brief RecursionのリストにプッシュすることでloopFuncを実行する
	class LoopFuncExecter
	{
	private:
		std::vector<LoopFlame*> _loopFuncList;//再帰始め関数りすと
	public:
		LoopFuncExecter() {}
		~LoopFuncExecter() {}
		//Recursionをまとめて実行
		void Executer()
		{
			for (auto& it : _loopFuncList)
			{
				if (!it)
					continue;
				Recursion recursion(it);//まとめて実行
			}
		}
		//再帰始め関数を入れる
		void Add(LoopFlame* loopFunc_)
		{
			_loopFuncList.push_back(loopFunc_);
		}
		void Clear()
		{
			_loopFuncList.clear();
		}
	private:
	};

	//! @brief RecursionのリストにプッシュすることでloopFuncを実行する
	class LoopFuncExecterWhile : public LoopFlame
	{
	private:
		std::vector<LoopFlame*> _loopFuncList;//再帰始め関数りすと
		LoopFlame* _nextLoopFunc;//! 次に実行されるループファンク(thisでagain、 nullptrでループ抜け)
	public:
		LoopFuncExecterWhile() {}
		~LoopFuncExecterWhile() {}
		//Recursionをまとめて実行
		LoopFlame* LoopFunc(LoopFlame* loopFunc_) override
		{
			for (auto& it : _loopFuncList)
			{
				if (!it)
					continue;
				Recursion recursion(it);//まとめて実行
			}
			return _nextLoopFunc;
		}
		//再帰始め関数を入れる
		void Add(LoopFlame* loopFunc_)
		{
			_loopFuncList.push_back(loopFunc_);
		}
		void Clear()
		{
			_loopFuncList.clear();
		}
	private:
	};


	//! BaseLoopの実態を作りたいときや
	//! BaseLoopの戻り値として、BaseLoopの実態を選択したいとき、このリストを参照する
	class BaseLoopManager
	{
	private:
		//std::pair<int , BaseLoop*> IDつき
		std::vector<LoopFlame*> loopFuncList;
	public:
		void Add(LoopFlame* loopFunc_)
		{
			loopFuncList.push_back(loopFunc_);
		}
		//! @brief 関数名からループファンクを設定
		void SetNextLoopFunc(LoopFlame* nextLoopFunc, char* funcname)
		{
			for (size_t i = 0; i < loopFuncList.size(); i++)
			{
				if (funcname != "")
					continue;
				//loopFuncList.at(i).SetNextLoopFunc(nextLoopFunc);
				break;
			}
		}
	};

	//! BaseLoopManagerList(シングルトン)



	//
	//template <typename T>
	//BaseLoop*  LoopFunc_();
	//BaseLoop*  LoopFunc_(BaseLoop * loopFunc_);
}
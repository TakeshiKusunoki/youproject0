#pragma once
#include "MsgLocation.h"
//#include <array>
#include <list>
#include "../Lib_3D/vector.h"
#include "../Lib_Base/Template.h"
#include <cmath>
#include "../Lib_Base\Property.h"
#include "../Lib_Base/Common.h"


//class Lib_Vassel::Location;

namespace Lib_Vassel {

	class Location;
	//! (オブジェクトにどのLocationManagerに属するかの値を渡すので、位置だけからletできる(forループで検索しなくていい))
	//! 長方形を長方形に敷き詰める(架空なので当たり判定がいらない)
	class LocationManager
	{
		_PROPERTY_READABLE_REFERENCE(std::vector<Location>, ArraySpace)//空間配列
		_PROPERTY_WRITEABLE(VECTOR3, PosCenter)//! 配列開始位置(位置差分)
		_PROPERTY_WRITEABLE_PROTECTED(VECTOR3, Ba)
	private:
		USER::LABEL* _label;
	public:
		static const size_t _LEVEL = 3;//! モートン深度レベル 0,1,2・・・
		static const size_t _ARRAY_WIDTH = 4;//! 配列幅の大きさ(SCREEN座標比)
		static size_t _ARRAY_SIZE;//配列長さ
		//std::array<Location, ARRAY_SIZE> _array;


	public:
		LocationManager(const VECTOR3& pos)
			:_PosCenter(_PosCenter)
		{
			_ARRAY_SIZE = ((size_t)pow((double)8, (double)_LEVEL) - 1) / 7;
			this->_ArraySpace.resize(_ARRAY_SIZE);
		}
		~LocationManager() {}

		void Update(const DirectX::XMFLOAT3& pos);
		void Show();

	private:
	};

	/*class LocationManager_ : public Singleton<LocationManager_>
	{
	public:

	private:

	};

#define pLocationManager_ LocationManager_::getInstance();*/
}

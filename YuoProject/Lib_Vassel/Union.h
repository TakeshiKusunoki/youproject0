#pragma once
//#include <tuple>
#include <DirectXMath.h>
#include <stdint.h>
#include <string>
#include <vector>
#include <cwctype>


//union AllType
//{
//public:
//	AllType() {}
//	~AllType() {}
//	int x;
//	float y;
//	ClassExample e;
//	ClassExample1 e1;
//	ClassExample2 e2;
//
//};
//
//
//
//struct TypeIndex
//{
//	//! ID
//	int ID;
//	//! データー
//	AllType OneTypeDate;
//};



/**すべての変数型名(AllTypeとして一つの型のみを保持)
*Sorting machine
*/
union UnionAllValueType
{
public:
	UnionAllValueType() {}
	~UnionAllValueType() {}

	void operator=(const UnionAllValueType& value)
	{
		*this = value;
	}
	//変数をセット
	template <typename ValueType>
	void SetValue(const ValueType& value)
	{
		*this = reinterpret_cast<UnionAllValueType>(value);
	}
	//ほかの変数へセット
	template <typename ValueType>
	void SetToOther(const ValueType& value)
	{
		value = reinterpret_cast<ValueType>(*this);
	}

	char _char;
	signed char	_char_s;
	unsigned char _char_u;
	wchar_t _wchar;
	short _short;
	signed short _short_s;
	unsigned short _short_u;
	int _int;
	signed int _int_s;
	unsigned int _int_u;
	long _long;
	signed long  _long_s;
	unsigned long _long_u;
	long long	_long__long;
	signed long long  _long__long_s;
	unsigned long long _long__long_u;

	float _float;
	double _double;

	int8_t __i08;
	int16_t __i16;
	int32_t __i32;
	int64_t __i64;
	uint8_t __ui08;
	uint16_t __ui16;
	uint32_t __ui32;
	uint64_t __ui64;

	int_least8_t __i_least08;
	int_least16_t __i_least16;
	int_least32_t __i_least32;
	int_least64_t __i_least64;
	uint_least8_t __ui_least08;
	uint_least16_t __ui_least16;
	uint_least32_t __ui_least32;
	uint_least64_t __ui_least64;

	int_fast8_t __i_fast08;
	int_fast16_t __i_fast16;
	int_fast32_t __i_fast32;
	int_fast64_t __i_fast64;
	uint_fast8_t __ui_fast08;
	uint_fast16_t __ui_fast16;
	uint_fast32_t __ui_fast32;
	uint_fast64_t __ui_fast64;

	intmax_t __max_int;
	uintmax_t __max_uint;

	//DirectX::XMVECTOR vector;
	//DirectX::XMMATRIX matrix;

};

////! 一つ分のデータ、識別用の番号など
//struct UnionAllValueType_BindData
//{
//	int num;//! 番号
//	std::string typeName;//! 型名
//	int id;//! 同じ型同じ名前の変数を識別するためのID
//	std::string valueName;//! 変数名
//	UnionAllValueType* pAllType;//! タイプデータのポインタ
//};
//
////! vectorクラスで作った疑似構造体
//struct UserClass
//{
//	int id;//! 同じ名前の構造体を識別するためのID
//	std::string structName;//! 構造体名
//	std::vector<UnionAllValueType_BindData>* pAllValueUnionPillar;//! 変数のvector型集合
//};

//! @brief ユニオン１っ個分
class UserValue
{
private:
	int _id;//! 同じ型同じ名前の変数を識別するためのID
	std::string _valueName;//! 変数名
	std::pair<std::string, UnionAllValueType> _value;
public:
	UserValue(int id, const char* valueName)
	{
		_id = id;
		_valueName = valueName;
	}
	~UserValue(){}
	//変数をセット
	template <typename ValueType>
	void Set(const ValueType& value)
	{
		_value.first = typeid(value).name();
		_value.second.SetValue(value);
	}
	//別の変数へ代入
	template <typename ValueType>
	void TypeSwitch(const std::string& str, ValueType& setedValue);

	std::string GetValueName();
private:

};

using use_UserValue = std::pair<std::string, UnionAllValueType>;

//! これをオブジェクトで定義
class UserValueManager
{
private:
	std::vector<UserValue> _valueList;
public:
	//! @brief 追加
	//! @param[in] id 乱数リストから取得
	//! @param[in] valueName 変数名
	//! @param[in] value 初期化値
	template <typename ValueType>
	void Add(int id, const char* valueName, const ValueType& value = 0)
	{
		UserValue userValue;
		userValue._id = id;
		userValue._valueName = valueName;
		userValue.Set(value);
		_valueList.push_back(userValue);
	}
	//! ユーザー変数の型名を取得
	UserValue* GetUserValue(char* valueName)
	{
		for (size_t i = 0; i < _valueList.size(); i++)
		{
			if (_valueList.at(i).GetValueName() != valueName)
				continue;
			return &_valueList.at(i);
		}
		return nullptr;
	}
};


//class UnionContorol
//{
//private:
//	UnionAllValueType _value;
//public:
//	UnionContorol();
//	~UnionContorol();
//	void operator=(const UnionAllValueType& value)
//	{
//		_value = value;
//	}
//	void compare();
//};
#include "BaseObject.h"
#include	<d3d11.h>
#include "../Common.h"
namespace Lib_Vassel {
	int ObjectManager::count = 0;
	//+ ----------------------------------------------------
	void BaseObject::Init()
	{
		SecureZeroMemory(&_private, sizeof(_private));
		SecureZeroMemory(&_protected, sizeof(_protected));
		SecureZeroMemory(&_public, sizeof(_public));
		_private._my_previnstance = nullptr;
		_private._my_instance = this;
		_private._endFlag = false;
		_private._flagMyInstanceMode = true;
		_private._pNextLoopFunc = nullptr;
		_public._pLom = nullptr;

		_protected._pRenderer = nullptr;
		_protected._actFlame = 0;
		_protected._actFlamePrev = 0;

		_public._vertexList.clear();
		_public._judgeFlag = true;
	};

	void BaseObject::LetData_()
	{
		//if (_flagMyInstanceMode)//_my_instanceで動く
		//{
		//	_my_instance->LetData_();
		//	return;
		//}
		//頂点すべてを参照
		float x, y, z;//配列位置格納用
		float x_ = -1, y_ = -1, z_ = -1;//前の配列位置//ありえない数字

		//for (size_t i=0 ; i < _public._vertexList.size(); i++)
		{
			//ロケーション配列キューブの外にオブジェクトがあればreturn
			x = ((_public._position.second->x - _public._pLom->_pos.x) / _public._pLom->LOCATION_SIZE);
			if (x >= _public._pLom->ARRAY_WIDTH || 0 > x)
				return;
			y = ((_public._position.second->y - _public._pLom->_pos.y) / _public._pLom->LOCATION_SIZE);
			if (y >= _public._pLom->ARRAY_WIDTH || 0 > y)
				return;
			z = ((_public._position.second->z - _public._pLom->_pos.z) / _public._pLom->LOCATION_SIZE);
			if (z >= _public._pLom->ARRAY_WIDTH || 0 > z)
				return;
			if (x == x_&&y == y_&&z == z_)//前の配列位置と同じ位置ならリターン
				return;
			x_ = x;
			y_ = y;
			z_ = z;

			size_t arrNum = static_cast<size_t>((z * _public._pLom->ARRAY_WIDTH * _public._pLom->ARRAY_WIDTH) + (y * _public._pLom->ARRAY_WIDTH) + x);// 配列位置
																														   //配列の最大サイズを超えていたらreturn
			if (arrNum >= _public._pLom->ARRAY_SIZE)
				return;
			//;LOCATIONに情報をadd
			_public._pLom->_array[arrNum].AddObject(this);
		}
	}

	void BaseObject::ShowMsg()
	{
		if (_public._msg == USER::MSG[Enum::HIT_ITEM])
		{
			int x = 0;
		}
		if (_private._flagMyInstanceMode)//_my_instanceで動く
		{
			_private._flagMyInstanceMode = false;
			_private._my_instance->ShowMsg();
			_private._flagMyInstanceMode = true;
			return;
		}
		static int timer = 0;
		printf("ShowMsg\t%d\n", ++timer);
		printf("%p\n", _private._my_instance);
		printf("%s\n", _public._msg);
	}

	BaseLoop * BaseObject::LoopFunc(BaseLoop * loopFunc)
	{
		//スワップ中のインスタンスのアクトフレームが止まっても動く
		if (_private._flagMyInstanceMode)//_my_instanceで動く
		{
			_private._flagMyInstanceMode = false;
			if (!_private._my_instance)
				return nullptr;
			BaseLoop* next = nullptr;
			if(_private._my_instance)
			next = _private._my_instance->LoopFunc(loopFunc);//_my_instance->_flagMyInstanceModeはfalseなので無限ループは起こらない
			_private._flagMyInstanceMode = true;

			return next;
		}

		if (_private._endFlag)
			return nullptr;//転生処理

		BaseLoop* next = _private._pNextLoopFunc = loopFunc;
		//auto if
		//if (Weit(0))//0回ウェイト
		//	return nullptr;
		//if (!DoLoopTimes(1))//1回るーぷ
		//	return nullptr;
		//else
		//	next = this;

		//exe
		Start();
		{
			//printf("_my_instance\t%p\n", _private._my_instance);
			MultiTask();
			_private._executer.Executer();
		}
		End();
		return next;
	}

	int ObjectManager::Add(BaseObject * obj, LocationManager * pLom)
	{
		use_pairIDandObject obj_;
		obj_.first = count++;
		obj_.second = obj;
		obj_.second->SetLocationManager(pLom);
		obj_.second->Initialize();
		_list.push_back(obj_);
		return obj_.first;
	}
}



#pragma once
#include "../Lib_Base/Property.h"
#include "../Lib_Base/Common.h"
#include "../Lib_Collition/PrimitivClass.h"
#include <vector>


namespace Lib_Vassel {

	//! 動かないオブジェクトで初期配置してから、変わらないもの
	class StageObject
	{
		_PROPERTY_READABLE_REFERENCE(const Lib_Collition::AABB, AABB)
	public:
		StageObject(Lib_Collition::AABB aabb):_AABB(aabb){}
		~StageObject(){}

	private:

	};


	//! 変わらない登録空間(８分木)
	class StaticLocation
	{
	private:
		const VECTOR3 _centerPos;
		const size_t _level;	//! ８分木分割レベル
		const std::vector<int> mortonSpace;//! モートン順序線形リスト
	public:
		StaticLocation(size_t Level_, const std::vector<StageObject>& cl, VECTOR3 centerPos)
			: _centerPos(centerPos)
			, _level(Level_)
		{
			cl[0].GetAABB().GetMaxPos();
			mortonSpace.resize(_level);
		}
		~StaticLocation();

		//! @param[in] Level ８分木分割レベル
		bool init()
		{

		}
	};
}

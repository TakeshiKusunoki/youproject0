#pragma once
#include "BaseObject.h"
#include "LocationObjectList.h"

namespace Lib_Vassel {
	class BaseObject;
	//! @brief msgloctionで委譲
	class Comparer
	{
	protected:
		/*std::list<BaseObject*>* _list1;
		std::list<BaseObject*>* _list2;*/
	public:
		Comparer(/*std::list<BaseObject*>* list1, std::list<BaseObject*>* list2*/)
			/*: _list1(nullptr)
			, _list2(nullptr)*/
		{}
		virtual ~Comparer() {}
		//! @breif InfoList1とInfoList２で比較を行う
		virtual void Compare(LocationObjectList* list) = 0;
		// 比較リスト決め　メッセージロケーションにaddするときもらう
		//virtual void SetList(LocationObjectList* list) = 0;
	};
}





#include "LocationManager.h"
#include <iostream>

namespace {
	// ビット分割関数 //3バイトごとに間隔を開ける関数(Level8まで)
	uint64_t BitSeparateFor3D(uint8_t n)
	{
		uint64_t s = n;
		s = (s | s << 8) & 0x0000f00f;
		s = (s | s << 4) & 0x000c30c3;
		s = (s | s << 2) & 0x00249249;
		return s;
	}

	// 3Dモートン空間番号算出関数
	uint64_t Get3DMortonOrder(uint8_t x, uint8_t y, uint8_t z)
	{
		return BitSeparateFor3D(x) | BitSeparateFor3D(y) << 1 | BitSeparateFor3D(z) << 2;
	}


}

namespace Lib_Vassel {
	void LocationManager::Update(const DirectX::XMFLOAT3 & pos)
	{
		//std::cout << "LocationManager" <<_pos.x << "\t" << _pos.y << "\t" << _pos.z << std::endl;
		this->SetPosCenter(pos);
		for (size_t i = 0, n = _ArraySpace.size(); i < n; i++)
		{
			this->_ArraySpace[i].Compare();
			_array[i].Compare();
			_array[i].Clear();
		}
	}

	void LocationManager::Show()
	{
		for (size_t i = 0; i <_array.size(); i++)
		{
			_array[i].Show();
		}
	}

}


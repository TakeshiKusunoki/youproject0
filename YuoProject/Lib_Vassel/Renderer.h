#pragma once
#include "BaseObject.h"
#include "../Lib_Base/Template.h"


namespace Lib_Vassel {
	class BaseObject;

	//! @brief レンダ−処理をする
	//! @details こちらでまとめて描画する先や、条件によってこの関数を呼び出すかを決める(視覚の問題)
	//! つまり情報の操作はウインドウに影響されず、描画はウインドウに反映させることができる
	class Renderer
	{
	private:
		//char* _windowClassName;//ウインドウクラス名(このオブジェクトはどのウインドウに反映する情報か)
	public:
		//BaseobjのmyInstanceを渡す
		Renderer(BaseObject* cl)
			:_cl(cl)
		{
		}
		~Renderer() {}
		BaseObject* _cl;//描画するオブジェクト
						//! @brief 描画
		virtual void Render();

	};

	//! 描画したいウインドウhwnd毎にソートする
	class RendererInterface
	{
	private:
		//std::list<BaseRender> base;
	};

	//! レンダラーを回す
	class RendererManager : public Singleton<RendererManager>
	{
		std::list<Lib_Render::BaseRender*> list;
	public:
		RendererManager() { Clear(); }
		~RendererManager() { Clear(); }
		void Add(Lib_Render::BaseRender* renderer)
		{
			///Renderer renderer(cl);
			list.push_back(renderer);
		}
		void Draw()
		{
			for (auto& it : list)
			{
				it->Render();
			}
		}
		void Clear()
		{
			list.clear();
		}
	private:
	};
#define pRendererManager RendererManager::getInstance()
}



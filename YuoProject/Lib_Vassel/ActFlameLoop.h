#pragma once
#include "BaseLoop.h"
using namespace std;

namespace Lib_Vassel {

	class ActFlameLoop : public ActFlame
	{

	public:
		ActFlameLoop();
		~ActFlameLoop();
		ActFlame* LoopFunc(const int argument)override;

		void Set(int x)
		{
			this->actFlame = x;
		}
	};

	//
	//class LoopFlame_ : public BaseLoop<LoopFlame*>
	//{
	//private:
	//	LoopFlame* nextLoop;//! 次のループ
	//	LoopFlame* const my_instance;//! 管理するインスタンス
	//	LoopFlame* my_instance_prev;//! スワップ用
	//public:
	//	LoopFlame_()
	//		: my_instance(this)
	//		, my_instance_prev(nullptr)
	//		, nextLoop(nullptr)
	//	{
	//	}
	//	virtual LoopFlame* LoopFunc(LoopFlame* argument)override = 0;
	//private:
	//
	//};

	//! actFlameが共通するクラス
	//! 別のアクトフレームを継承して使うクラス
	class ActFlameIndirectReference
	{
	private:

	public:
		ActFlame* actFlameCl;
	public:
		ActFlameIndirectReference(ActFlame* actFlameCl_)
			: actFlameCl(actFlameCl_)
		{
		}
		~ActFlameIndirectReference() {}

		//! actFlameを別のアクトフレームと同期させる
		//! @param  argument 反映させるアクトフレーム
		ActFlameIndirectReference* Task(ActFlame* argument)
		{
			this->actFlameCl->LoopFunc(argument->actFlame);
			argument->actFlame = this->actFlameCl->actFlame;
			return this;
		}
		//! @return アクトフレーム
		ActFlame*const GetActFlameCl()
		{
			return this->actFlameCl;
		}
		//! actFlameClを変える
		void ChangeActFlameCl(ActFlame* actFlameCl_)
		{
			actFlameCl = actFlameCl_;
		}
	private:

	};


	//! actFlameが共通するクラス
	//! 別のアクトフレームを継承して使うクラス
	class ActFlameAsynchronous
	{
	private:
		ActFlame* actFlameCl;
	public:

	public:
		ActFlameAsynchronous(ActFlame* actFlameCl_)
			: actFlameCl(actFlameCl_)
		{
		}
		~ActFlameAsynchronous() {}

		//! actFlameを別のアクトフレームと同期しない
		//! param  argument 反映させるアクトフレーム
		const int Task(ActFlame* argument)
		{
			this->actFlameCl->LoopFunc(argument->actFlame);
			return this->actFlameCl->actFlame;
		}
		ActFlame*const GetActFlameCl()
		{
			return this->actFlameCl;
		}
		//! actFlameClを変える
		void ChangeActFlameCl(ActFlame* actFlameCl_)
		{
			actFlameCl = actFlameCl_;
		}
	private:

	};


	//! 自身のアクトフレームを引数に渡す
	class ActFlameInstant
	{
		ActFlameAsynchronous actFlameAsynchronous;//! 変化させるよう
		ActFlame* actFlameCl;
	public:
		ActFlameInstant(ActFlame* actFlameCl)
			: actFlameAsynchronous(actFlameCl)
			, actFlameCl(actFlameCl)
		{
		}
		~ActFlameInstant() {}

		//! アクトフレームにクラス
		//! @return アクトフレームを返す
		int MultiTusk()
		{
			//actFlameを反映させる
			return actFlameAsynchronous.Task(actFlameCl);
		}
	private:

	};

}
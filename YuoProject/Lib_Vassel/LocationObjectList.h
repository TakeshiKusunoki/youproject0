#pragma once
#include "BaseObject.h"
namespace Lib_Vassel {
	class BaseObject;
	struct LocationObjectList
	{
		std::list<BaseObject*> _playerList;
		std::list<BaseObject*> _enemyList;
		std::list<BaseObject*> _itemList;
	};
}

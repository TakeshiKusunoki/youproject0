struct VS_OUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
};

struct PS_DirLight(
    float2 Tex : TEXCOORD0) : COLOR
{

};

cbuffer CONSTANT_BUFFER : register(b0)
{
	row_major float4x4 WVP;//wvp
	row_major float4x4 WORLD;	//ワールド座標
	float4 MATRIAL_COLOR;	//物体の色
	float4 LIGHT_DIRECTION;	//ライト進行方向
	float4 EYE_POS;            //視点座標
	float4 LIGHT_COLOR;//光源の色
	float4 NYUTORAL_LIGHT;//環境光
};

// 平行光源（開始位置あり）
technique dirLight
{
	pass P0
	{
		//シェーダー
		VertexShader = compile ps_5_0 PS_DirLight();
	}
}
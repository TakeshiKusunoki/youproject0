struct VS_OUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
};

cbuffer CONSTANT_BUFFER : register(b0)
{
	row_major float4x4 WVP;//wvp
	row_major float4x4 WORLD;	//ワールド座標
	float4 MATRIAL_COLOR;	//物体の色
	float4 LIGHT_DIRECTION;	//ライト進行方向
	float4 EYE_POS;            //視点座標
	float4 LIGHT_COLOR;//光源の色
	float4 NYUTORAL_LIGHT;//環境光
};
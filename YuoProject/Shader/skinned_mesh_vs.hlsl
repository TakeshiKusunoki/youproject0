#include "skinned_mesh.hlsli"

VS_OUT main(float4 position : POSITION, float4 normal : NORMAL, float2 texcoord : TEXCOORD,
	float4 bone_weights : WEIGHTS, uint4 bone_indices : BONES)// UNIT.20
{
	VS_OUT vout;

	// UNIT.20--------------------------------------------------
	/*float4 influence = { 0,0,0,1 };
	for (int i2 = 0; i2 < 4; i2++)
	{
		float weight = bone_weights[i2];
		if (weight > 0.0f)
		{
			switch (bone_indices[i2])
			{
			case 0:
				influence.r = weight;
				break;
			case 1:
				influence.g = weight;
				break;
			case 2:
				influence.b = weight;
				break;
			}
		}
	}
	vout.color = influence;*/
	//--------------------------------------------------------


	// UNIT.21-----------
	float3 p = { 0, 0, 0 };
	float3 n = { 0, 0, 0 };
	for (int i2 = 0; i2 < 4; i2++)
	{
		if (bone_weights[i2] == 0)
			continue;
		p += (bone_weights[i2] * mul(position, BONE_TRANSFORM[bone_indices[i2]])).xyz;
		n += (bone_weights[i2] * mul(float4(normal.xyz, 0), BONE_TRANSFORM[bone_indices[i2]])).xyz;
	}
	position = float4(p, 1.0f);
	normal = float4(n, 0.0f);
	//normal.w = 0;
	//-----------------------
	vout.position = mul(position, WVP);
	vout.texcoord = texcoord;

	float4 N = normalize(mul(normal, WORLD));

	float4 L = normalize(-LIGHT_DIRECTION);
	//vout.color = MATRIAL_COLOR;


	//���ʔ���
	/*float3 l = normalize(L.xyz - position.xyz);
	float3 no = normalize(normal.xyz);
	float3 r = 2.0 * no * dot(no, l) - l;
	float3 v = normalize(EYE_POS.xyz - position.xyz);
	float  i = pow(saturate(dot(r, v)), MATRIAL_COLOR.w);*/

	//�n�[�t�x�N�g�����ʔ���
	float3 l = normalize(LIGHT_DIRECTION.xyz - position.xyz);
	float3 no = normalize(normal.xyz);
	float3 v = normalize(EYE_POS.xyz - position.xyz);
	float3 h = normalize(l + v);
	float  i = pow(saturate(dot(h, v)), MATRIAL_COLOR.w);

	vout.color = float4(i * MATRIAL_COLOR.xyz * LIGHT_COLOR.xyz, MATRIAL_COLOR.a) ;


	//�����łڂ���
	//vout.color *= max(0, dot((L.xyz*(EYE_POS.xyz-position.xyz)), N));
	//vout.color *= max(0, dot(L, N));
	//���̉e
	vout.color *= max(0, dot(L, N));
	//����
	vout.color += NYUTORAL_LIGHT;
	//vout.color.a = 1;



	return vout;
}
#include "XVertexMesh.hlsli"

VS_OUT main(float4 pos : POSITION, float4 normal : NORMAL, float2 texcoord : TEXCOORD, float3 at : AT, float2 texDistance : TEX_DISTANCE)
{
	VS_OUT vout;
	//sNZψΙγό
	pos = float4(pos.xyz, 1.0f);
	normal = float4(normal.xyz, 0.0f);

	pos += float4(at, 0.0f); //at½f
	vout.position = mul(pos, WVP);
	vout.position = float4(vout.position.xyz, 1.0f);
	//vout.position = pos;
	texcoord += texDistance;//texΚu½f
	vout.texcoord = texcoord;

	normal.w = 0;
	float4 N = normalize(mul(normal, WORLD));
	float4 L = normalize(-LIGHT_DIRECTION);



	//ΎΚ½Κ
	/*float3 l = normalize(L.xyz - position.xyz);
	float3 no = normalize(normal.xyz);
	float3 r = 2.0 * no * dot(no, l) - l;
	float3 v = normalize(EYE_POS.xyz - position.xyz);
	float  i = pow(saturate(dot(r, v)), MATRIAL_COLOR.w);*/

	//n[txNgΎΚ½Κ
	float3 l = normalize(LIGHT_DIRECTION.xyz - pos.xyz);
	float3 no = normalize(normal.xyz);
	float3 v = normalize(EYE_POS.xyz - pos.xyz);
	float3 h = normalize(l + v);
	float  i = pow(saturate(dot(h, v)), MATRIAL_COLOR.w);

	vout.color = float4(i * MATRIAL_COLOR.xyz * LIGHT_COLOR.xyz, MATRIAL_COLOR.a);

	vout.color *= max(0, dot(L, N));
	////Β«υ
	vout.color += NYUTORAL_LIGHT;

	return vout;
}
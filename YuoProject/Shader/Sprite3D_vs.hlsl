#include "Sprite3D.hlsli"

VS_OUT main(float4 pos : POSITION, float4 normal : NORMAL, float2 texcoord : TEXCOORD)
{
	VS_OUT vout;
	//ピクセル引数に代入
	pos = float4(pos.xyz, 1.0f);
	normal = float4(normal.xyz, 0.0f);
	vout.position = mul(pos,WVP);
	vout.position = float4(vout.position.xyz, 1.0f);
	//vout.position = pos;
	vout.texcoord = texcoord;

	normal.w = 0;
	float4 N = normalize(mul(normal, WORLD));
	float4 L = normalize(-LIGHT_DIRECTION);

	//鏡面反写
	/*float3 l = normalize(L.xyz - position.xyz);
	float3 no = normalize(normal.xyz);
	float3 r = 2.0 * no * dot(no, l) - l;
	float3 v = normalize(EYE_POS.xyz - position.xyz);
	float  i = pow(saturate(dot(r, v)), MATRIAL_COLOR.w);*/

	//ハーフベクトル鏡面反写
	float3 l = normalize(LIGHT_DIRECTION.xyz - pos.xyz);
	float3 no = normalize(normal.xyz);
	float3 v = normalize(EYE_POS.xyz - pos.xyz);
	float3 h = normalize(l + v);
	float  i = pow(saturate(dot(h, v)), MATRIAL_COLOR.w);

	vout.color = float4(i * MATRIAL_COLOR.xyz * LIGHT_COLOR.xyz, MATRIAL_COLOR.a);


	vout.color *= max(0, dot(L, N));
	////環境光
	vout.color += NYUTORAL_LIGHT;

	//normal.w = 0;
	//float4 N = normalize(mul(normal, WORLD));
	//float4 L = normalize(-LIGHT_DIRECTION);
	//vout.color = MATRIAL_COLOR * max(0, dot(L, N)) + NYUTORAL_LIGHT;
	//vout.color.a = MATRIAL_COLOR.a;
	//return vout;

	//vout.color = float4(1,1,1,1);
	//光の影

	return vout;
}
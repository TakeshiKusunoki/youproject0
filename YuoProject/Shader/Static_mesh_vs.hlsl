#include "Static_mesh.hlsli"
#define NYUTORAL_LIGHT 0.2f//����

VS_OUT main(float4 position : POSITION, float4 normal : NORMAL, float2 texcoord : TEXCOORD, float3 at : AT)
{
	VS_OUT vout;
	vout.position = mul(position + at, world_view_projection);
	vout.texcoord = texcoord;

	normal.w = 0;  float4 N = normalize(mul(normal, world));
	float4 L = normalize(-light_direction);
	vout.color = material_color * max(0, dot(L, N)) + NYUTORAL_LIGHT;
	vout.color.a = material_color.a;
	return vout;
}